\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{styleRapport}

\LoadClass[12pt,a4paper,twoside]{report}

\RequirePackage{ae,lmodern}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{cite}
\RequirePackage{pifont} %\ding{}
\RequirePackage{svg}
\RequirePackage{hyperref}
\RequirePackage{enumitem}
\RequirePackage{pgfornament} % rendre le document plus joli
\RequirePackage{latexsym}
\RequirePackage{xcolor}
\RequirePackage{graphicx}
\RequirePackage[explicit]{titlesec}
\RequirePackage{tikz}
\RequirePackage[left=2.5cm,right=1.5cm,top=2cm,bottom=2.5cm,twoside]{geometry}
\RequirePackage{textcomp}
\RequirePackage{pgfgantt}
\RequirePackage{pdflscape}
\RequirePackage[french]{babel}
\RequirePackage{amssymb}
\RequirePackage{xstring}
\RequirePackage{listings}

\usetikzlibrary{positioning,shapes.misc,calc,babel}
\RequirePackage{caption}

% modification du chemin source des images svg
\setsvg{svgpath=./src/}
% ne pas oublier la commande
% "inkscape -z -D --file=my_file.svg --export-pdf=my_file.pdf --export-latex"
% pour transformer svg en latex

% couleurs personnalisées
\definecolor{mainColor}{RGB}{0,100,255}
\definecolor{secondColor}{RGB}{0,255,170}

% customiser les puces
\setlist[itemize]{label=$\bullet$}
\setlist[itemize,1]{label=$\circ$}

%%%%%%%%%%%%%%%% COMMANDES FORT UTILES %%%%%%%%%%%%%%%%
%\texteAnnexe{reference}{titre}{texte}, \ref{reference} \pageref{reference}
\newcommand{\texteAnnexe}[3]{
	\noindent
	\begin{tikzpicture}
		\node [draw=secondColor!50!black,
			line width=.25mm,
			rectangle,
			rounded corners=4mm,
			inner sep=12pt,
			inner ysep=16pt,
			text width={\textwidth - 32pt},
			] (box) {%
			%\begin{minipage}{\textwidth - 10pt}
			#3
			%\end{minipage}
		};
		\node [draw=secondColor!50!black,
			fill=secondColor,
			top color=secondColor!50!white,
			bottom color=secondColor!60!black,
			line width=.25mm,
			rounded corners=2mm,
			right=24pt] at (box.north west) {%
				\large\bfseries\sffamily\color{white}{#2}
				\label{#1}
			};
	\end{tikzpicture}
	\par
}

%%%%%%%%%%%%%%%% STYLE DES CHAPITRAGES %%%%%%%%%%%%%%%%
%%%%%%%% EN TÊTE DE CHAPITRES %%%%%%%%
\makeatletter
\def\@makechapterhead#1{% \chapter
	{\makebox{\reset@font \setlength{\baselineskip}{.5\baselineskip} \Huge \scshape \color{mainColor}{\@chapapp \hspace{3mm} \thechapter}}%
	\hfill \interlinepenalty\@M {\Huge \sffamily \bfseries \color{mainColor}{#1}}%
	\vspace{1mm}
	\begin{center}
		\pgfornament[width=0.7\textwidth,color=mainColor]{89}%
	\end{center}
	\par\vspace{4mm}}
}
\def\@makeschapterhead#1{% \chapter*
	{\reset@font \hfill \interlinepenalty\@M {\Huge \sffamily \bfseries \color{mainColor}{#1}}%
	\vspace{1mm}
	\begin{center}
		\pgfornament[width=0.7\textwidth,color=mainColor]{89}%
	\end{center}
	\par\vspace{4mm}}
}

%%%%%%%% CHAPITRES %%%%%%%%
% paramètres : <texte><taille>
\newcommand{\titleNumberStyle}[2]{
	\begin{tikzpicture}
		\node[fill=mainColor!50!white,
			line width=#2,
			rounded rectangle,
			rounded rectangle east arc=0pt,
			top color=mainColor!50!white,
			bottom color=mainColor!60!black,
			draw] {\color{white}#1};
	\end{tikzpicture}
}
% paramètres : <texte><taille>
\newcommand{\titleLabelStyle}[2]{
	\begin{tikzpicture}
		\node(s)[rectangle,
			fill=white]{#1};
		\draw(s.south west)--(s.south east)[
			line width=#2,
			fill=mainColor!50!white];
	\end{tikzpicture}
}
\titleformat{\section}
	[hang]
	{\reset@font\Large\bfseries\color{mainColor}}
	{%
	%\llap{\colorbox{mainColor}{\color{white}\thesection}}}
	\begin{tikzpicture}
		\node[fill=mainColor!50!white,
			line width=.5mm,
			rounded rectangle,
			rounded rectangle east arc=0pt,
			top color=mainColor!50!white,
			bottom color=mainColor!60!black,
			draw] {\color{white}\thesection};
	\end{tikzpicture}
	}
	{-5.5mm}
	{
	\begin{tikzpicture}
		\node(s)[rectangle,
			fill=white]{#1};
		%\node(s){#1};
		\draw(s.south west)--(s.south east)[
			line width=.5mm,
			fill=mainColor!50!white];
		%\node[rectangle,rounded rectangle,inner sep=10pt,fill=blue]{\llap{\thesection}};
	\end{tikzpicture}
	}
	%[\color{mainColor}{\hrule}]

\titleformat{\subsection}
	[hang]
	{%\reset@font\Large
	\bfseries\color{mainColor}}
	{\hspace{1em}\titleNumberStyle{\thesubsection}{.3mm}}
	{-3.8mm}
	{\titleLabelStyle{#1}{.3mm}}
	%[\color{mainColor}{\hrule}]

\titleformat{\subsubsection}
	[hang]
	{%\reset@font\Large
	\bfseries\color{mainColor}}
	{\hspace{2em}\titleNumberStyle{\thesubsubsection}{.2mm}}
	{-3.8mm}
	{\titleLabelStyle{#1}{.2mm}}
	%[\color{mainColor}{\hrule}]

%%%%%%%%%%%%%%%% COMMANDES POUR LA PAGE DE GARDE %%%%%%%%%%%%%%%%

\newcommand*{\TypeDocument}[1]{\gdef\@TypeDocument{#1}}
\TypeDocument{}

\newcommand*{\LogoUniversite}[1]{\gdef\@LogoUniversite{#1}}
\LogoUniversite{}

\newcommand*{\discipline}[1]{\gdef\@discipline{#1}}
\discipline{}

\newcommand*{\NomProjet}[1]{\gdef\@NomProjet{#1}}
\NomProjet{}

\newcommand*{\SousTitre}[1]{\gdef\@SousTitre{#1}\vspace{6mm}}
\SousTitre{}

\newcommand*{\NomUniversite}[1]{\gdef\@NomUniversite{#1}}
\NomUniversite{}

\newcommand*{\unite}[1]{\gdef\@unite{#1}}
\unite{}

\newcommand*{\Commanditaires}[1]{\gdef\@Commanditaires{#1}}
\Commanditaires{}

\newcommand*{\ChargesTD}[1]{\gdef\@ChargesTD{#1}}
\ChargesTD{}

\makeatletter
\def\maketitle{%
	\thispagestyle{empty}
	\clearpage
	\begin{tikzpicture}[remember picture,overlay,line width=0mm]
		%\draw[draw=white,fill=none](current page.north west) rectangle (\paperwidth,1);
		\node at (current page.center)
		{
			\parbox{14cm}{
				\begin{center}
					\@LogoUniversite
				\end{center}
				\begin{center}
					\sffamily\Large\textsc{\@TypeDocument}\\ \@NomUniversite
				\end{center}
				\begin{center}
					\begin{huge}
						\textcolor[rgb]{0.7,0.7,0.7}{\ding{67}}
						\textcolor[rgb]{0.4,0.4,0.4}{\ding{67}}
						\textcolor[rgb]{0,0,0}{\ding{67}}
						\textcolor[rgb]{0.4,0.4,0.4}{\ding{67}}
						\textcolor[rgb]{0.7,0.7,0.7}{\ding{67}}
					\end{huge}
				\end{center}
				\begin{center}
					\@unite
				\end{center}
				\begin{center}
					\textbf{Discipline : \@discipline}
				\end{center}
				\vspace{1cm}
				%\noindent\rule{14cm}{0.5mm}
				\pgfornament[width=14cm,symmetry=h]{85}
				\begin{center}
					\vspace{8mm}
					\begin{huge}
						\textbf{\@NomProjet}
					\end{huge}\\
					\vspace{8mm}
					\@SousTitre
				\end{center}
				%\noindent\rule{14cm}{0.5mm}
				\pgfornament[width=14cm]{85}
				\vspace{8mm}\\
				\begin{minipage}{45mm}
					\begin{flushleft}\textsc{Auteurs}\end{flushleft}
				\end{minipage}
				\begin{minipage}{40mm}
					\begin{center}\textsc{Chargée de TD}\end{center}
				\end{minipage}
				\begin{minipage}{45mm}
					\begin{flushright}\textsc{Clients}\end{flushright}
				\end{minipage}
				\vspace{3mm}\\
				\begin{minipage}{45mm}
					\begin{flushleft}\@author\end{flushleft}
				\end{minipage}
				\begin{minipage}{40mm}
					\begin{center}\@ChargesTD\end{center}
				\end{minipage}
				\begin{minipage}{45mm}
					\begin{flushright}\@Commanditaires\end{flushright}
				\end{minipage}
				\vspace{12mm}\\
				\begin{minipage}{5cm}
					\includegraphics[width=\textwidth]{src/inria}
				\end{minipage}
				\begin{minipage}{3cm}
					\centering\Large\textsf{HiePACS}
				\end{minipage}
				\begin{minipage}{6cm}
					\begin{flushright}
						{\small\textbf{Dernière modification}\\ \@date}
					\end{flushright}
				\end{minipage}
			}
		};
	\end{tikzpicture}
	\cleardoublepage
}
\makeatother
