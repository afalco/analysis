#-------------------------------------------------
#
# Project created by QtCreator 2017-02-13T21:52:06
#
#-------------------------------------------------

QT       += core sql xml gui widgets

TARGET = tests
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ../

SOURCES += tests.cpp \
	../src/toolbox.cpp \
	../src/modules/model/Model.cpp \
	../src/modules/model/Module.cpp \
	../src/modules/model/parsing/ParseModule.cpp \
	../src/modules/model/GraphModule.cpp \
	../src/modules/model/parsing/XmlContentHandler.cpp \
	../src/modules/model/metricTable/MetricSelector.cpp \
	../src/modules/model/database/DatabaseModule.cpp \
	../src/modules/model/database/Experiment.cpp \
	../src/modules/model/database/ExperimentCampaign.cpp \
	../src/modules/model/database/Matrix.cpp \
	../src/modules/model/database/Solver.cpp \
	../src/modules/model/database/Software.cpp \
	../src/modules/model/database/Hardware.cpp \
	../src/modules/model/database/Output.cpp \
	../src/modules/model/database/ConfigFile.cpp \
	../src/modules/model/database/MetricPattern.cpp \
	../src/modules/model/database/DatabaseTable.cpp \
	../src/modules/model/database/DatabaseTableMetrics.cpp \
	../src/modules/model/parsing/ParsingExperimentCampaign.cpp \
	../src/modules/model/parsing/ParsingExperiment.cpp \
	../src/modules/model/parsing/ParsingConfig.cpp \
	../src/modules/model/database/DatabaseTableException.cpp \
	../src/modules/model/parsing/ParsingMetric.cpp \
	../src/modules/model/parsing/ParsingMetricPattern.cpp \
	../src/modules/model/metricTable/PlotStyleChooser.cpp \
	../src/modules/model/metricTable/MetricTable.cpp \
	../src/modules/model/metricTable/MetricContainer.cpp \
	../src/modules/model/metricTable/MetricPlotStyle.cpp \
	../src/modules/model/metricTable/MetricModifier.cpp

HEADERS += ../src/toolbox.h \
	../src/modules/model/Model.h \
	../src/modules/model/Module.h \
	../src/modules/model/parsing/ParseModule.h \
	../src/modules/model/GraphModule.h \
	../src/modules/model/parsing/XmlContentHandler.h \
	../src/modules/model/metricTable/MetricSelector.h \
	../src/modules/model/database/DatabaseModule.h \
	../src/modules/model/database/Experiment.h \
	../src/modules/model/database/ExperimentCampaign.h \
	../src/modules/model/database/Matrix.h \
	../src/modules/model/database/Solver.h \
	../src/modules/model/database/Software.h \
	../src/modules/model/database/Hardware.h \
	../src/modules/model/database/Output.h \
	../src/modules/model/database/ConfigFile.h \
	../src/modules/model/database/MetricPattern.h \
	../src/modules/model/database/DatabaseTable.h \
	../src/modules/model/database/DatabaseTableMetrics.h \
	../src/modules/model/parsing/ParsingExperimentCampaign.h \
	../src/modules/model/parsing/ParsingExperiment.h \
	../src/modules/model/parsing/ParsingConfig.h \
	../src/modules/model/database/DatabaseTableException.h \
	../src/modules/model/parsing/ParsingMetric.h \
	../src/modules/model/parsing/ParsingMetricPattern.h \
	../src/modules/model/metricTable/PlotStyleChooser.h \
	../src/modules/model/metricTable/MetricTable.h \
	../src/modules/model/metricTable/MetricContainer.h \
	../src/modules/model/metricTable/MetricPlotStyle.h \
	../src/modules/model/metricTable/MetricModifier.h

DEFINES += SRCDIR=\\\"$$PWD/\\\"
