#include <string>
#include <iostream>
#include <strstream>
#include <cstdio>
#include <vector>
#include <map>
#include <sys/time.h>

#include <QGuiApplication>
#include <QVariant>
#include <QDebug>

#include "src/modules/model/parsing/ParseModule.h"
#include "src/modules/model/Model.h"
#include "src/modules/model/database/ExperimentCampaign.h"
#include "src/modules/model/database/Experiment.h"
#include "src/modules/model/GraphModule.h"
#include "src/modules/model/database/DatabaseTableException.h"
#include "src/modules/model/database/ConfigFile.h"
#include "src/modules/model/database/MetricPattern.h"


#define Default		"\033[0m"
#define Tblack		"\033[30m"
#define Tred		"\033[31m"
#define Tgreen		"\033[32m"
#define Tyellow		"\033[33m"
#define Tblue		"\033[34m"
#define Tmagenta	"\033[35m"
#define Tcyan		"\033[36m"
#define Tlgray		"\033[37m"
#define Tdgray		"\033[90m"
#define Tlred		"\033[91m"
#define Tlgreen		"\033[92m"
#define Tlyellow	"\033[93m"
#define Tlblue		"\033[94m"
#define Tlmagenta	"\033[95m"
#define Tlcyan		"\033[96m"
#define Twhite		"\033[97m"
#define Bblack		"\033[40m"
#define Bred		"\033[41m"
#define Bgreen		"\033[42m"
#define Byellow		"\033[43m"
#define Bblue		"\033[44m"
#define Bmagenta	"\033[45m"
#define Bcyan		"\033[46m"
#define Blgray		"\033[47m"
#define Bdgray		"\033[100m"
#define Blred		"\033[101m"
#define Blgreen		"\033[102m"
#define Blyellow	"\033[103m"
#define Blblue		"\033[104m"
#define Blmagenta	"\033[105m"
#define Blcyan		"\033[106m"
#define Bwhite		"\033[107m"
#define Bold		"\033[1m"

using namespace std;

unsigned int nbTests=0, nbSuccess=0;
strstream out;

#define TIME_DIFF(t1, t2) \
	((t2.tv_sec - t1.tv_sec) * 1000000 + (t2.tv_usec - t1.tv_usec))

void printElapsedTime(string label, struct timeval begin, struct timeval end)
{
	out << Tcyan << "\tElapsed time to " << label << " : " << Bold << (double) TIME_DIFF(begin, end) / 1000000.0f << " seconds" << Default << endl;
}

void newSection(string section)
{
	out << endl << Bold Tcyan << "----------------<" Tblack Bcyan " BEGIN SECTION " << section << " " Default Bold Tcyan ">----------------" << Default << endl;
}

#define ROBUST_TEST(count, name, test) { \
	nbTests ++; unsigned long long ok = 0; \
	for (unsigned long long i = 0; i < count; ++i) \
	if (test) ok++; \
	if (count == ok) nbSuccess++; \
	out << "\t" << name << ": " Bold "(" << ok << "/" << count << ") " << (ok==count?Tgreen "successful":ok==0?Tred "failed":Tyellow "to review") << Default << endl; \
	}

void test(string name, bool test)
{
	nbTests ++;
	out << "\t" << name;
	if (test) {
		out << Bold Tgreen ": successful" Default << endl;
		nbSuccess ++;
	} else out << Bold Tred ": failed" Default << endl;
}

void robust_test(unsigned long long count, string name, bool(*f)(int))
{
	nbTests ++;
	unsigned long long ok = 0;
	for (unsigned long long i = 0; i < count; ++i)
		if (f(i))
			++ok;
	if (ok == count)
		nbSuccess++;
	out << "\t" << name << ": " Bold "(" << ok << "/" << count << ") " << (ok==count?Tgreen "successful":ok==0?Tred "failed":Tyellow "to review") << Default << endl;
}

bool parsingResultComparing(std::vector<ParsingMetric *> result1, std::vector<ParsingMetric *> result2)
{
	if (result1.size() != result2.size())
		return false;
	bool isTheSame = true;
	for (std::vector<ParsingMetric *>::iterator iteratorResult1 = result1.begin(); iteratorResult1 != result1.end(); ++iteratorResult1)
	{
		isTheSame = false;
		for (std::vector<ParsingMetric *>::iterator iteratorResult2 = result2.begin(); iteratorResult2 != result2.end(); ++iteratorResult2)
		{
			if ((*iteratorResult1)->getPattern()->getName() == (*iteratorResult2)->getPattern()->getName()
					&& (*iteratorResult1)->getValue() == (*iteratorResult2)->getValue())
			{
				isTheSame = true;
			}
		}
		if (!isTheSame)
		{
			return false;
		}
	}
	return true;
}

bool checkNumberResults(Model * model, int nbFilesExpected, int nbValuesExpected) {
	int nbFiles = 0;
	int nbValues = 0;

	/*
	std::vector<std::vector<ParsingMetric*> > resultsExpected;
	resultsExpected.push_back(result1Expected);
	resultsExpected.push_back(result2Expected);
	*/
	//bool passed = true;
	std::vector<ParsingExperiment *> parsedExperiments = model->getCurrentCampaign()->getExperiments();
	//std::vector<std::vector<ParsingMetric*> >::iterator resultsIterator = resultsExpected.begin();
	for (std::vector<ParsingExperiment *>::iterator itExp = parsedExperiments.begin(); itExp != parsedExperiments.end(); ++itExp)
	{
		std::map<std::string, std::vector<ParsingMetric *>> parsedOutputFiles = (*itExp)->getOutputFiles();
		for (std::map<std::string, std::vector<ParsingMetric *>>::iterator it = parsedOutputFiles.begin(); it != parsedOutputFiles.end(); ++it)
		{
			nbFiles++;
			for (std::vector<ParsingMetric *>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
			{
				nbValues++;
			}
		}
		/*
		if (resultsIterator != resultsExpected.end())
		{
			++resultsIterator;
		}
		*/
	}
	return (nbFiles == nbFilesExpected && nbValues == nbValuesExpected);
}

bool insertExperimentCampaign(int seed)
{
	ExperimentCampaign *ec = new ExperimentCampaign;
	ec->setName(QVariant::fromValue(seed).toString());
	ec->setDate(QDateTime::fromMSecsSinceEpoch(seed+1));
	ec->setDescription(QVariant::fromValue(seed+2).toString());
	Experiment *e = new Experiment;
	ec->addExperiment(e);
	e->setName(QVariant::fromValue(seed+3).toString().toStdString());
	for (int i = 0, end = (seed/10)+1; i < end; ++i)
	{
		Matrix *m = new Matrix;
		e->setMatrix(m);
		m->setName(QVariant::fromValue(seed).toString().toStdString());
		Software *sof = new Software;
		e->setSoftware(sof);
		sof->setCompiler(QVariant::fromValue(seed).toString().toStdString() + "compiler");
		Solver *sol = new Solver;
		e->setSolver(sol);
	}
	ec->save();
	return ec->getId() != 0;
}

int main(int argc, char *argv[])
{
	out.flush();
	QGuiApplication app(argc, argv); // required to use components like QPixmap
	out << Tcyan << "args (" << argc << "):" << Tblue;
	for (int i = 1; i < argc; ++i)
		out << " " << argv[i];
	out << Default << endl;

	struct timeval tBegin, tEnd;


	// -------- BEGIN TESTS --------
	/****************************** test db  *******************************/
	remove("test.db"); // delete database file if exists
	newSection("Database");

	// setup database informations
	test("Can open sqlite connection in file test.db",
		DatabaseModule::connect("test.db", DatabaseModule::SQLITE));

	// create database scheme, cause we have a new clean database for tests
	DatabaseModule::createDatabaseScheme();

	/*
	 * Tests for config files
	 */
	QString
		confFileName = "ConfigTest.xml",
		confFilePath = "/user/foo/bar/",
		mpName = "test",
		mpPattern = "test\\d+",
		mpPrint = "Test",
		mpDefault = "42",
		mpPreOp = "azerty",
		mpPostOp = "querty";
	DatabaseModule::Tables mpTable = DatabaseModule::MATRIX;
	QVariant::Type mpType = QVariant::Double;
	bool
		mpIsParameter = true,
		mpIsFraction = false;
	ConfigFile *configFile = new ConfigFile;
	configFile->setName(confFileName);
	configFile->setPath(confFilePath);
	MetricPattern *metricPattern = new MetricPattern;
	configFile->addMetricPattern(metricPattern);
	metricPattern->setTable(mpTable);
	metricPattern->setName(mpName);
	metricPattern->setPattern(mpPattern);
	metricPattern->setPrint(mpPrint);
	metricPattern->setDefault(mpDefault);
	metricPattern->setPreOperation(mpPreOp);
	metricPattern->setPostOperation(mpPostOp);
	metricPattern->setType(mpType);
	metricPattern->isParameter(mpIsParameter);
	metricPattern->isFraction(mpIsFraction);
	configFile->save();

	delete configFile; configFile = NULL;
	delete metricPattern; metricPattern = NULL;

	try {
		configFile = new ConfigFile(1);
	} catch (DatabaseTableException& dte) { qInfo("%s", dte.what()); }
	test("Can select a config file", configFile != NULL);
	test("Config file got data is correct", configFile != NULL &&
		configFile->getName() == confFileName &&
		configFile->getPath() == confFilePath &&
		configFile->getMetricPattern().size() == 1);
	try {
		metricPattern = configFile->getMetricPattern().at(0);
	} catch (std::out_of_range& oor) { qInfo("%s", oor.what()); }
	test("Can select a metric pattern", metricPattern != NULL);
	test("Metric pattern got data is correct", metricPattern != NULL &&
		metricPattern->getTable() == mpTable &&
		metricPattern->getName() == mpName &&
		metricPattern->getPattern() == mpPattern &&
		metricPattern->getPrint() == mpPrint &&
		metricPattern->getDefault() == mpDefault &&
		metricPattern->getPreOperation() == mpPreOp &&
		metricPattern->getPostOperation() == mpPostOp &&
		metricPattern->getType() == mpType &&
		metricPattern->isParameter() == mpIsParameter &&
		metricPattern->isFraction() == mpIsFraction);

        // values to insert in database and check if are corrcctly insert ed
	QString
		expCampName = "experiment campaign test",
		expCampDescr = "A test experiment campaign.",
		expName = "Experiment test",
		matName = "My matrix",
		softCompiler = "g++5",
		softLib = "-lm -lopenmp",
		solvName = "PaStiX";
	QDateTime expCampDate = QDateTime::fromString("2017−02−16T19:20:30+01:00");
	std::pair<QString, QVariant>
		matMetric(mpName, 55.22),
		hardMetric("platform", "Debian 8.5");
	/*
	 * Create an empty experiment campaign and save it.
	 * Adding all associated content, as experiment,
	 * matrix, software, hardware, solver and others.
	 */
	ExperimentCampaign *experimentCampaign = new ExperimentCampaign;
	experimentCampaign->setName(expCampName);
	experimentCampaign->setDate(expCampDate);
	experimentCampaign->setDescription(expCampDescr);
	Experiment *experiment = new Experiment;
	experimentCampaign->addExperiment(experiment);
	experiment->setName(expName.toStdString());
	Matrix *matrix = experiment->getMatrix();
	matrix->setName(matName.toStdString());
	matrix->addMetric(matMetric.first.toStdString(), matMetric.second);
	Software *software = experiment->getSoftware();
	software->setCompiler(softCompiler.toStdString());
	software->setLibraries(softLib.toStdString());
	Hardware *hardware = experiment->getHardware();
	hardware->addMetric(hardMetric.first.toStdString(), hardMetric.second);
	Solver *solver = experiment->getSolver();
	solver->setName(solvName.toStdString());
	experimentCampaign->save();
	// delete data in memory after database insertion
	delete experimentCampaign; experimentCampaign = NULL;
	delete experiment; experiment = NULL;
	delete matrix; matrix = NULL;
	delete software; software = NULL;
	delete hardware; hardware = NULL;
	delete solver; solver = NULL;

	// test if inserted data can be found from database
	// 1 because first inserted ID is ont and currect database was empty at the begin of test file
	try {
		experimentCampaign = new ExperimentCampaign(1);
	} catch (DatabaseTableException& dte) { qInfo("%s", dte.what()); }
	test("Can select an experiment campaign", experimentCampaign != NULL);
	test("Experiment campain got data is correct", experimentCampaign != NULL &&
		experimentCampaign->getName() == expCampName &&
		experimentCampaign->getDate() == expCampDate &&
		experimentCampaign->getDescription() == expCampDescr);

	try {
		experiment = new Experiment(1);
	} catch (DatabaseTableException& dte) { qInfo("%s", dte.what()); }
	test("Can select an experiment", experiment != NULL);
	test("Experiment got data is correct", experiment != NULL &&
		experiment->getName() == expName.toStdString());
	try {
		matrix = new Matrix(1);
	} catch (DatabaseTableException& dte) { qInfo("%s", dte.what()); }
	test("Can select an matrix", matrix != NULL);
	test("Matrix got data is correct", matrix != NULL &&
		matrix->getName() == matName.toStdString());
	try {
		software = new Software(1);
	} catch (DatabaseTableException& dte) { qInfo("%s", dte.what()); }
	test("Can select an software", software != NULL);
	test("Software got data is correct", software != NULL &&
		software->getCompiler() == softCompiler.toStdString() &&
		software->getLibraries() == softLib.toStdString());
	try {
		hardware = new Hardware(1);
	} catch (DatabaseTableException& dte) { qInfo("%s", dte.what()); }
	test("Can select an hardware", hardware != NULL);
	test("hardware got data is correct", hardware != NULL &&
		hardware->getMetric(hardMetric.first.toStdString()) == hardMetric.second);
	try {
		solver = new Solver(1);
	} catch (DatabaseTableException& dte) { qInfo("%s", dte.what()); }
	test("Can select an solver", solver != NULL);
	test("Solver got data is correct", solver != NULL &&
		solver->getName() == solvName.toStdString());

	gettimeofday(&tBegin, NULL);
	robust_test(16, "Insert huge data in database", &insertExperimentCampaign);
	gettimeofday(&tEnd, NULL);
	printElapsedTime("huge database insertion", tBegin, tEnd);

	gettimeofday(&tBegin, NULL);
	robust_test(16, "Select huge data in database", [](int seed) -> bool {
		ExperimentCampaign *ec = NULL;
		try {
			ec = new ExperimentCampaign(seed);
		} catch (DatabaseTableException dte) {
			return false;
		}
		if (ec != NULL)
		{
			try {
				Experiment e(seed);
			} catch (DatabaseTableException& dte) {
				return false;
			}
			try {
				Matrix m(seed);
			} catch (DatabaseTableException& dte) { return false; }
			try {
				Software s(seed);
			} catch (DatabaseTableException& dte) { return false; }
			try {
				Solver s(seed);
			} catch (DatabaseTableException& dte) { return false; }
		}
		ec->setName(QVariant::fromValue(seed).toString());
		ec->setDate(QDateTime::fromMSecsSinceEpoch(seed+1));
		ec->setDescription(QVariant::fromValue(seed+2).toString());
		Experiment *e = new Experiment;
		ec->addExperiment(e);
		e->setName(QVariant::fromValue(seed+3).toString().toStdString());
		for (int i = 0, end = (seed/10)+1; i < end; ++i)
		{
			Matrix *m = new Matrix;
			e->setMatrix(m);
			m->setName(QVariant::fromValue(seed).toString().toStdString());
			Software *sof = new Software;
			e->setSoftware(sof);
			sof->setCompiler(QVariant::fromValue(seed).toString().toStdString() + "compiler");
			Solver *sol = new Solver;
			e->setSolver(sol);
		}
		ec->save();
		return ec->getId() != 0;
	});
	gettimeofday(&tEnd, NULL);
	printElapsedTime("huge database selection", tBegin, tEnd);

	// close connection
	DatabaseModule::disconnect();
	test("Database correctly disconnected", DatabaseModule::isConnected() == false);
	//remove("test.db"); // delete database after test

	/************************************************************************/

	//test("database is connected", DatabaseModule::isConnected());
	//DatabaseModule::disconnect();
	//test("database is not connected", !DatabaseModule::isConnected());

	newSection("Graph"); // --------

	/* Scatter Plot */
	std::vector<QVariant> absSP;
	absSP.push_back(QVariant(2));
	absSP.push_back(QVariant(5));
	absSP.push_back(QVariant(7));
	absSP.push_back(QVariant(4));
	std::map<MetricPlotStyle*,std::vector<QVariant> >  ordSP;
	std::vector<QVariant> nbSP;
	nbSP.push_back(QVariant(52));
	nbSP.push_back(QVariant(36));
	nbSP.push_back(QVariant(89));
	nbSP.push_back(QVariant(1));
	MetricPlotStyle *mpsSP=new MetricPlotStyle(nbSP[0].type(),DatabaseModule::SOLVER,"id");
	ordSP.insert(std::pair<MetricPlotStyle*,std::vector<QVariant>  >(mpsSP, nbSP));

	gettimeofday(&tBegin, NULL);
	QPixmap graphSP = GraphModule::generateRImage(640, 480, absSP, ordSP, GraphModule::SCATTER_PLOT);
	test("Generate Scatter Plot", !graphSP.isNull());
	gettimeofday(&tEnd, NULL);
	printElapsedTime("generate scatter plot", tBegin, tEnd);

	graphSP.save("scatter_plot_test.png");
	//qInfo("%s|||\n%s\n|||%s",Bcyan Tmagenta, GraphModule::generateRScript("MonFichier", 640, 480, absSP, ordSP, GraphModule::SCATTER_PLOT).toStdString().data(), Default);

	/* Line Graph */
	std::vector<QVariant> absLG;
	absLG.push_back(QVariant(2));
	absLG.push_back(QVariant(3));
	absLG.push_back(QVariant(4));
	absLG.push_back(QVariant(6));
	std::map<MetricPlotStyle*,std::vector<QVariant> > ordLG;
	std::vector<QVariant> nbLG;
	nbLG.push_back(QVariant(52));
	nbLG.push_back(QVariant(36));
	nbLG.push_back(QVariant(89));
	nbLG.push_back(QVariant(1));
	MetricPlotStyle *mpsLG=new MetricPlotStyle(nbLG[0].type(),DatabaseModule::SOLVER,"id");
	ordLG.insert(std::pair<MetricPlotStyle*,std::vector<QVariant> >(mpsLG, nbLG));

	gettimeofday(&tBegin, NULL);
	QPixmap graphLG = GraphModule::generateRImage(640, 480, absLG, ordLG, GraphModule::LINE_GRAPH);
	test("Generate Line Graph", !graphLG.isNull());
	gettimeofday(&tEnd, NULL);
	printElapsedTime("generate line graph", tBegin, tEnd);

	graphLG.save("simple_line_graph_test.png");
	//qInfo("%s|||\n%s\n|||%s",Bcyan Tmagenta, GraphModule::generateRScript("MonFichier", 640, 480, absLG, ordLG, GraphModule::LINE_GRAPH).toStdString().data(), Default);

	/* Multiple Line Graph */
	std::vector<QVariant> absLGM;
	absLGM.push_back(QVariant(2));
	absLGM.push_back(QVariant(3));
	absLGM.push_back(QVariant(4));
	absLGM.push_back(QVariant(6));
	std::map<MetricPlotStyle*,std::vector<QVariant> >  ordLGM;
	std::vector<QVariant> nbLGM;
	nbLGM.push_back(QVariant(52));
	nbLGM.push_back(QVariant(36));
	nbLGM.push_back(QVariant(89));
	nbLGM.push_back(QVariant(1));
	MetricPlotStyle *mpsLGM=new MetricPlotStyle(nbLGM[0].type(),DatabaseModule::SOLVER,"id");
	mpsLGM->setName("id");
	ordLGM.insert(std::pair<MetricPlotStyle*,std::vector<QVariant> >(mpsLGM, nbLGM));
	std::vector<QVariant> nb2LGM;
	nb2LGM.push_back(QVariant(4));
	nb2LGM.push_back(QVariant(1500));
	nb2LGM.push_back(QVariant(15));
	nb2LGM.push_back(QVariant(6));
	MetricPlotStyle *mps2LGM=new MetricPlotStyle(nb2LGM[0].type(),DatabaseModule::SOLVER,"id2");
	mps2LGM->setName("id2");
	ordLGM.insert(std::pair<MetricPlotStyle*,std::vector<QVariant> >(mps2LGM, nb2LGM));

	gettimeofday(&tBegin, NULL);
	QPixmap graphLGM = GraphModule::generateRImage(640, 480, absLGM, ordLGM, GraphModule::LINE_GRAPH);
	test("Generate Multiple Line Graph", !graphLGM.isNull());
	gettimeofday(&tEnd, NULL);
	printElapsedTime("generate multiple line graph", tBegin, tEnd);

	graphLGM.save("multiple_line_graph_test.png");

	/* Histogram */
	std::vector<QVariant> absH;
	absH.push_back(QVariant(5.9));
	absH.push_back(QVariant(0));
	absH.push_back(QVariant(9.3));
	absH.push_back(QVariant(1.9));
	absH.push_back(QVariant(1.6));
	absH.push_back(QVariant(2.6));
	absH.push_back(QVariant(5.4));
	absH.push_back(QVariant(4));
	absH.push_back(QVariant(9.2));
	absH.push_back(QVariant(1.2));
	absH.push_back(QVariant(7.7));
	absH.push_back(QVariant(8.3));
	std::map<MetricPlotStyle*,std::vector<QVariant> >  ordH;

	gettimeofday(&tBegin, NULL);
	QPixmap graphH = GraphModule::generateRImage(640, 480, absH, ordH, GraphModule::HISTOGRAM);
	test("Generate Histogram", !graphH.isNull());
	gettimeofday(&tEnd, NULL);
	printElapsedTime("generate histogram", tBegin, tEnd);

	graphH.save("histogram_test.png");
	//qInfo("%s|||\n%s\n|||%s",Bcyan Tmagenta, GraphModule::generateRScript("MonFichier", 640, 480, absH, ordH, GraphModule::HISTOGRAM).toStdString().data(), Default);


	newSection("Parsing"); // --------

	std::vector<ParsingMetricPattern *> configExample;
	ParsingMetricPattern * patternMemory = new ParsingMetricPattern("memory", "Memory", "(Memory)\\s*(.*)", "double", true, "output");
	ParsingMetricPattern * patternFactorize = new ParsingMetricPattern("factorize_time", "Factorize Time", "(Time to factorize  )\\s*(.*)", "double", false, "output");
	patternFactorize->setPostOperation("[^\\s]+\\s");

	configExample.push_back(patternMemory);
	configExample.push_back(patternFactorize);

	//Function to test : ParseModule::parseFile
	std::vector<ParsingMetric *> result1Expected;
	//All the result we expect for "lap100_tol_1e-2_method_1_blocksize_128-256"
	result1Expected.push_back(new ParsingMetric("56.434109", patternMemory));
	result1Expected.push_back(new ParsingMetric("68.5", patternFactorize));
	std::vector<ParsingMetric *> result2Expected;
	//All the result we expect for "lap100_tol_1e-4_method_1_blocksize_128-256
	result2Expected.push_back(new ParsingMetric("29.612403", patternMemory));
	result2Expected.push_back(new ParsingMetric("67.3", patternFactorize));

	std::vector<ParsingMetric *> oneFileResult;
	std::map<std::string, std::vector<ParsingMetric *>> multipleFilesResult;

	std::string folder = "examples/data/BLR-pastix/results_laplacians/";

	oneFileResult = ParseModule::parseFile(configExample, folder + "lap100_tol_1e-2_method_1_blocksize_128-256");

	test("One file parsing number results : ParseModule::parseFile", oneFileResult.size() == result1Expected.size());
	test("One file parsing exact results : ParseModule::parseFile", parsingResultComparing(result1Expected, oneFileResult));


	//Function to test : ParseModule::parseFiles
	std::vector<std::string> filenames;
	filenames.push_back(folder + "lap100_tol_1e-2_method_1_blocksize_128-256");
	filenames.push_back(folder + "lap100_tol_1e-4_method_1_blocksize_128-256");
	multipleFilesResult = ParseModule::parseFiles(configExample, filenames);
	std::map<std::string, std::vector<ParsingMetric *>>::iterator itMultipleResults = multipleFilesResult.begin();

	gettimeofday(&tBegin, NULL);
	unsigned int sizeToTest = itMultipleResults->second.size();
	bool firstOnePassed = parsingResultComparing(itMultipleResults->second, result1Expected);
	++itMultipleResults;
	sizeToTest += itMultipleResults->second.size();
	bool secondOnePassed = parsingResultComparing(itMultipleResults->second, result2Expected);
	//Do the test associated
	test("Multiple files parsing number Results : ParseModule::parseFiles",sizeToTest == result1Expected.size() + result2Expected.size());
	test("Multiple files parsing exact Results : ParseModule::parseFiles", firstOnePassed && secondOnePassed);

	//Function to test : ParseModule::parseFolder
	multipleFilesResult = ParseModule::parseFolder(configExample, folder);
	sizeToTest = 0;
	for (std::map<std::string, std::vector<ParsingMetric *>>::iterator it = multipleFilesResult.begin(); it != multipleFilesResult.end(); ++it)
	{
		sizeToTest += it->second.size();
	}

	//Do the test associated
	//We will not test ALL the files...
	//44 files with between 1 and 6 metrics, I HAVE COUNTED there is 213 results.
	test("Full folder parsing number Results : ParseModule::parseFolder",sizeToTest == 84);
	gettimeofday(&tEnd, NULL);
	printElapsedTime("full folder parsing", tBegin, tEnd);

	std::vector<ParsingMetricPattern *> configParsed = ParseModule::parseConfig("examples/config/pastix-lapla.xml");

	test("Config file parsing, number of patterns results : ParseModule::parseConfig", configParsed.size() == 2);
	//For the moment, we will not compare the config we get with the config we expect, because it will be 29 strings to write, compare, and it's a little boring.
	//Do a ParseModule::parseFiles and compare with the correct values.

	//Make a model example
	Model * model = new Model();

	QStringList * qConfigFilename = new QStringList();
	qConfigFilename->push_back("examples/config/pastix-lapla.xml");
	model->parseConfig(qConfigFilename);


	//Function to test : Model::parseFiles
	QStringList * filenamesFromController = new QStringList();
	string filename1 = folder + "lap100_tol_1e-2_method_1_blocksize_128-256";
	string filename2 = folder + "lap100_tol_1e-4_method_1_blocksize_128-256";

	filenamesFromController->push_back(QString(filename1.c_str()));
	filenamesFromController->push_back(QString(filename2.c_str()));

	gettimeofday(&tBegin, NULL);
	model->parseFiles(filenamesFromController);

	bool exactNumber = checkNumberResults(model, 2, 4);

	test("Multiple files parsing after config file initialize, checking for number of results and validity of results", exactNumber);
	gettimeofday(&tEnd, NULL);
	printElapsedTime("multiple files parsing", tBegin, tEnd);
	//test("Multiple files parsing after config file initialize, checking for exact results", passed);

	model->clearExperimentCampaign();

	test("Clear Experiment, No config", model->getCurrentCampaign()->getParsingConfig() == NULL);
	test("Clear Experiment, No experiments", model->getCurrentCampaign()->getExperiments().empty());
	test("Clear Experiment, No current experiments", model->getCurrentCampaign()->getSelectedExperiments().empty());
	qConfigFilename = new QStringList();
	qConfigFilename->push_back("examples/config/pastix-lapla.xml");

	model->parseConfig(qConfigFilename);
	//Function to test : Model::parseFolder
	QStringList * foldernameFromController = new QStringList();
	QString foldername = QString(folder.c_str());

	foldernameFromController->push_back(foldername);
	model->parseFolder(foldernameFromController);

	exactNumber = checkNumberResults(model, 44, 84);

	test("Folder parsing after config file initialize, exact number of experiments", model->getCurrentCampaign()->getExperiments().size() == 44);
	test("Folder parsing after config file initialize, checking for number of results and validity of results", exactNumber);

	string fraction = "20 MO / 40 MO";
	string expectedFractionResult = "50.000000"; // 6 number after dot
	string fractionResult = ParseModule::parseFraction(fraction);
	test("Fraction parsing right expected value : ", expectedFractionResult == fractionResult);

	newSection("Parsing into Database");

	remove("test.db");
	DatabaseModule::connect("test.db", DatabaseModule::SQLITE);
	// create database scheme, cause we have a new clean database for tests
	DatabaseModule::createDatabaseScheme();

	model->clearExperimentCampaign();
	qConfigFilename = new QStringList();
	qConfigFilename->push_back("examples/config/pastix-lapla.xml");
	model->parseConfig(qConfigFilename);
	QString qConfigFilenameQ = QString("examples/config/pastix-lapla.xml");
	model->saveConfigFile("Test_Config");
	model->parseConfigFile(qConfigFilenameQ);
	std::vector<ParsingMetricPattern *> patterns = model->getCurrentCampaign()->getParsingConfig()->getConfig();
	filenamesFromController = new QStringList();
	filename1 = folder + "lap100_tol_1e-2_method_1_blocksize_128-256";
	filename2 = folder + "lap100_tol_1e-4_method_1_blocksize_128-256";

	filenamesFromController->push_back(QString(filename1.c_str()));
	filenamesFromController->push_back(QString(filename2.c_str()));
	model->onInsertionWidgetParserOptionSelected(1);
	model->parseFiles(filenamesFromController);



	std::string experimentCampaignNameString = "Test_Experiment_Campaign";
	std::string experimentCampaignDescString = "This is an experiment campaign.";
	QString * experimentCampaignName = new QString(experimentCampaignNameString.c_str());
	QString * experimentCampaignDesc = new QString(experimentCampaignDescString.c_str());
	model->insertExperimentCampaign(experimentCampaignName, experimentCampaignDesc);

	ExperimentCampaign experimentCampaignFetched(1); // Get this one



	std::vector<Experiment*> experimentsFetched = experimentCampaignFetched.getExperiments();
	for (std::vector<Experiment*>::iterator it = experimentsFetched.begin() ; it != experimentsFetched.end(); ++it)
	{
		std::cout << " /!\ : " << (*it)->getName() << std::endl;
		std::string experimentName = (*it)->getName();
		std::string testName = "Experiment fetched after Insertion ";
		test(testName, experimentName == "lap100_tol_1e-2_method_1_blocksize_128-256" || "lap100_tol_1e-4_method_1_blocksize_128-256");
	}
	//All the result we expect for "lap100_tol_1e-2_method_1_blocksize_128-256"
	//Experiment 1
	//	Memory  : 56.434109
	//	Factorize  : 68.5
	//Experiment 2
	//	Memory : 29.612403
	//	Factorize : 67.3


	DatabaseModule::disconnect();

	// -------- TESTS SUMMARY --------
	out << endl << Bold Tcyan <<  "----------------<" Tblack Bcyan " SUMMARY OF TEST CAMPAIGN " Default Bold Tcyan ">----------------" << endl;
	out << "\t" << (nbSuccess==nbTests?Tgreen:nbSuccess==0?Tred:Tyellow);
	out << "(" << nbSuccess << "/" << nbTests << ") Tests successed" Default << endl;
	// -------- DO SOMETHING WITH RESULT STRING --------
	cout << out.str();
	return 0;
}
