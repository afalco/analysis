# PDP project from university of Bordeaux & Inria

## Build the application
* Go to `./` directory.
* Execute `qmake && make`
* An `PDP` executable must be created

**Is important to notice that app and test needs some files in src (src/sql/). If the application doesn't works fine, it can be because doesn't have access ti it's files.**
*If make doesn't work, make sure that Qt-5 is installed and run `qmake -qt=5` instead of `qmake`*
If the project is compiled on Debian, try using `qmake -qt=5 -spec linux-g++-64 CONFIG+=debug PDP.pro` instead of `qmake`.

Use `-w` or `--windowed` argument to run app in graphical mode
Use `-h` or `--help` argument to get a help on how to use the app

### Build unary tests
* Go to `tests/` directory.
* Execute `qmake && make`
* An `test` executable must be created

Run `(cd ../ && exec tests/tests)` to make tests.

#### Directories ####
```sh
doc/ # documentation in the app
examples/ # example file to run application
	config/ # Solver's configuration files
	data/ # Solvers' outputs
rapports/ # Tous documents confondus (rapports, slides, ...)
src/ # source code of the app
	modules/ # modules composing app kernel
		controller/ # contains controller class
		model/
			database/
			metricTable/
			parsing/
		view/
			viewGUI/
				metricTable/
				tab/
			viewSH/
	sql/ # all SQL files
tests/ # all tests for the app
```
