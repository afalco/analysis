SELECT :metric
	FROM :table
	WHERE :metric BETWEEN :begin AND :end;
