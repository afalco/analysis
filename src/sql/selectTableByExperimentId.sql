SELECT :table.id
	FROM :table
		INNER JOIN Experiment
		ON Experiment.id:table = :table.id
	WHERE Experiment.id = :id;
