PRAGMA foreign_keys = ON;

CREATE TABLE matrix (
	id						INTEGER PRIMARY KEY AUTOINCREMENT,
	name					TEXT
);

CREATE TABLE software (
	id						INTEGER PRIMARY KEY AUTOINCREMENT,
	compiler				TEXT,
	libraries				TEXT
);

CREATE TABLE hardware (
	id						INTEGER PRIMARY KEY AUTOINCREMENT
);

CREATE TABLE solver (
	id						INTEGER PRIMARY KEY AUTOINCREMENT,
	name					TEXT,
	version					TEXT,
	options					TEXT
);

CREATE TABLE experimentCampaign (
	id						INTEGER PRIMARY KEY AUTOINCREMENT,
	name					TEXT,
	date					TEXT,
	description 			TEXT
);

CREATE TABLE experiment (
	id						INTEGER PRIMARY KEY AUTOINCREMENT,
	name					TEXT,
	idSolver				INTEGER,
	idMatrix				INTEGER,
	idHardware				INTEGER,
	idSoftware				INTEGER,
	idExperimentCampaign	INTEGER,
	FOREIGN KEY (idSolver) REFERENCES solver(id),
	FOREIGN KEY (idMatrix) REFERENCES matrix(id),
	FOREIGN KEY (idHardware) REFERENCES hardware(id),
	FOREIGN KEY (idSoftware) REFERENCES software(id),
	FOREIGN KEY (idExperimentCampaign) REFERENCES experimentCampaign(id)
);

CREATE TABLE output (
	id						INTEGER PRIMARY KEY AUTOINCREMENT,
	name					TEXT,
	date					TEXT,
	path					TEXT,
	idExperiment			INTEGER,
	FOREIGN KEY (idExperiment) REFERENCES experiment(id)
);

CREATE TABLE configFile (
	id						INTEGER PRIMARY KEY AUTOINCREMENT,
	name					TEXT,
	path					TEXT
);

CREATE TABLE metricPattern (
	id						INTEGER PRIMARY KEY AUTOINCREMENT,
	destinationTable		TEXT,
	name					TEXT,
	pattern					TEXT,
	print					TEXT,
	defaultVal				TEXT,
	preOperation			TEXT,
	postOperation			TEXT,
	type					TEXT,
	isParameter				INTEGER,
	isFraction				INTEGER,
	idConfigFile			INTEGER,
	FOREIGN KEY (idConfigFile) REFERENCES configFile(id)
);

CREATE TABLE solver_configFile (
	idSolver        		INTEGER,
	idConfigFile			INTEGER,
	FOREIGN KEY (idSolver) REFERENCES solver(id),
	FOREIGN KEY (idConfigFile) REFERENCES configFile(id),
	PRIMARY KEY (idSolver, idConfigFile)
);
