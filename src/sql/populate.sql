INSERT INTO configFile (name) VALUES ('config file 1');
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Matrix', 'name', 'name\\s*(.+)', 'QString', 1);
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Matrix', 'nnz', 'nnz\\s*(\\d+)', 'int', 1);
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Matrix', 'precision', 'precision\\s*(\\d+)', 'double', 1);
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Solver', 'name', 'name\\s*(.*)', 'QString', 1);
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Solver', 'options', 'options\\s*(.*)', 'QString', 1);
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Software', 'compiler', 'compiler\\s*(.*)', 'QString', 1);
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Software', 'libraries', 'libraries\\s*(.*)', 'QString', 1);
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Hardware', 'platform', 'platform\\s*(.*)', 'QString', 1);
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Hardware', 'cluster', 'cluster\\s*(.*)', 'QString', 1);
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Experiment', 'nbCore', 'nbCore\\s*(.*)', 'int', 1);
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Experiment', 'nbNode', 'nbNode\\s*(.*)', 'int', 1);
INSERT INTO metricPattern (destinationTable, name, pattern, type, idConfigFile) VALUES ('Experiment', 'nbGpu', 'nbGpu\\s*(.*)', 'int', 1);

INSERT INTO matrix (name, nnz) VALUES ('small', 55);
INSERT INTO matrix (name, nnz) VALUES ('medium', 232);
INSERT INTO matrix (name, nnz) VALUES ('large', 5589);

INSERT INTO solver (name, options) VALUES ('MaPHyS', '-O3');
INSERT INTO solver (name, options) VALUES ('paStiX', '-O3');

INSERT INTO software (compiler, libraries) VALUES ('gcc', '-lm');
INSERT INTO software (compiler, libraries) VALUES ('g++', '-lm');

INSERT INTO hardware (platform, cluster) VALUES ('debian', '');
INSERT INTO hardware (platform, cluster) VALUES ('fedora', '');

INSERT INTO experimentCampaign (name, description) VALUES ('testing', 'A small campaign');
INSERT INTO experimentCampaign (name, description) VALUES ('production', 'Big experiment campaign');

INSERT INTO experiment (idSolver, idMatrix, idHardware, idSoftware, idExperimentCampaign, name, date, nbCore, nbNode, nbGpu) VALUES (1, 1, 2, 1, 1, 'experiment 1', '', 5, 1, 0);
INSERT INTO experiment (idSolver, idMatrix, idHardware, idSoftware, idExperimentCampaign, name, date, nbCore, nbNode, nbGpu) VALUES (2, 2, 1, 1, 1, 'experiment 2', '', 0, 1, 5);
INSERT INTO experiment (idSolver, idMatrix, idHardware, idSoftware, idExperimentCampaign, name, date, nbCore, nbNode, nbGpu) VALUES (1, 3, 1, 2, 2, 'experiment 3', '', 96, 8, 256);

INSERT INTO output (idExperiment, name, date) VALUES (1, 'output 1', '');
INSERT INTO output (idExperiment, name, date) VALUES (2, 'output 2', '');
INSERT INTO output (idExperiment, name, date) VALUES (3, 'output 3', '');
