SELECT :table.id, :table.*
	FROM :table
		INNER JOIN experiment
		ON experiment.id:table = :table.id
	WHERE experiment.idExperimentCampaign = :id;
