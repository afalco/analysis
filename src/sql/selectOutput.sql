SELECT output.*
	FROM output
		JOIN experiment
		ON experiment.idOutput = output.id
	WHERE experiment.id = :id;
