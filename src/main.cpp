#include "src/modules/model/Model.h"
#include "src/modules/view/viewGUI/ViewGUI.h"
#include "src/modules/view/viewSH/ViewSH.h"
#include "src/modules/controller/Controller.h"
#include <QApplication>

#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QThread>
#include <QMetaType>

#include <iostream>
#include <sstream>

/**
 * return true if -w or --windowed parameters are used
 * else return false
 */
bool applicationType(int &argc, char *argv[])
{
	for (int i = 1; i < argc; ++i)
		if (!(strcmp(argv[i], "-w") && strcmp(argv[i], "--windowed")))
			return true;
	return false;
}

int main(int argc, char *argv[])
{
	/// variables declaration
	int returnCode;
	Model model;
	Controller controller;
	QThread modelThread;

	// Define own types, used in application events and/or QVariant
	qRegisterMetaType<std::map<unsigned long int, QString> >("std::map<unsigned long int, QString>");
	qRegisterMetaType<std::vector<unsigned long int> >("std::vector<unsigned long int>");
	qRegisterMetaType<unsigned long int>("unsigned long int");

	// threading of the model, in order to improve user experience
	model.moveToThread(&modelThread);
	modelThread.start();

	if (applicationType(argc, argv)) {
		/* GUI application is required. */
		QApplication app(argc, argv);

		/*MainWindow w;
		w.show();*/
		ViewGUI view;
		controller.bindEvents(view, model);
		view.show();

		// start event loop
		returnCode = app.exec();
	} else {
		/* command line application is required */
		QCoreApplication app(argc, argv);

		// create parser for command line arguments
		QCommandLineParser parser;
		parser.setApplicationDescription("Exemple");
		parser.addHelpOption();
		parser.addVersionOption();

		// add arguments to parser
		QCommandLineOption testOption(
			QStringList() << "t" << "test",
			QCoreApplication::translate("main", "Teste les arguments")
		);
		parser.addOption(testOption);

		// parse application
		parser.process(app);

		//get parsed results
		std::stringstream str;

		str << "App arguments :\n";
		for (int i = 0; i < argc; ++i)
			str << "\t" << i << " : " << argv[i] << "\n";
		str << "\n\n";

		str << "Postionnal arguments :\n";
		const QStringList args = parser.positionalArguments();
		for (QStringList::const_iterator cit = args.constBegin();
			 cit != args.constEnd();
			 ++cit) {
			str << "\t" << cit->toLocal8Bit().constData() << "\n";
		}
		str << "\n\n";

		str << "Parsed options :\n";
		str << "\t";
		foreach (QString s, testOption.names()) { str << "{" << s.toStdString() << "} "; }
		str << ": " << (parser.isSet(testOption) ? "SET" : "UNSET") << "\n";

		std::cout << str.rdbuf() << std::endl;
		returnCode = 0;//app.exec();
	}

	// properly stop the thread
	modelThread.quit();
	modelThread.wait();

	return returnCode;
}
