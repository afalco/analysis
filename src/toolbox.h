#ifndef TOOLBOX_H
#define TOOLBOX_H

#include <string>
#include <algorithm>
#include <cctype>
#include <functional>
#include <locale>

#include <QString>
#include <QVariant>

/**
 * @brief ltrim
 * Delete all white-space characters from the beginning of the string
 * @param s
 * std::tring to modify
 */
inline void ltrim(std::string &s)
{
	s.erase(s.begin(),
		std::find_if(
			s.begin(),
			s.end(),
			std::not1(std::ptr_fun<int, int>(std::isspace))
		)
	);
}

/**
 * @brief rtrim
 * Delete all white-space characters from end beginning of the string
 * @param s
 * std::tring to modify
 */
inline void rtrim(std::string &s)
{
	s.erase(std::find_if(
			s.rbegin(),
			s.rend(),
			std::not1(std::ptr_fun<int, int>(std::isspace))
		).base(),
		s.end()
	);
}

/**
 * @brief trim
 * Delete all white-space characters from the beginning and the end of the string
 * @param s
 * std::tring to modify
 */
inline void trim(std::string &s)
{
	ltrim(s);
	rtrim(s);
}

/**
 * @brief replace
 * Replace the oldText in the subject by the text in newText.
 * @param subject
 * The string where replace value
 * @param oldText
 * A regex pattern to find text to replace
 * @param newText
 * The text to insert
 * @param all
 * Set to true to replace all instances of oldText in subjet or false to replace
 * only the first instance found.
 */
void replace(std::string &, const std::string &, const std::string &, bool all = false);

struct cmpQStringInsensitive {
	bool operator()(const QString& x, const QString& y) const {
		return x.compare(y, Qt::CaseInsensitive) > 0;
	}
};

typedef std::pair<QString, QVariant::Type> qStringTypePair_t;
const qStringTypePair_t multimapStartValues[] = {
	qStringTypePair_t("BOOL", QVariant::Bool),
	qStringTypePair_t("B", QVariant::Bool),
	qStringTypePair_t("BOOLEAN", QVariant::Bool),

	qStringTypePair_t("INT", QVariant::Int),
	qStringTypePair_t("I", QVariant::Int),
	qStringTypePair_t("INTEGER", QVariant::Int),
	qStringTypePair_t("LONG", QVariant::LongLong),
	qStringTypePair_t("L", QVariant::LongLong),
	qStringTypePair_t("LONGINT", QVariant::LongLong),
	qStringTypePair_t("LONGINTEGER", QVariant::LongLong),
	qStringTypePair_t("LONGLONG", QVariant::LongLong),
	qStringTypePair_t("LL", QVariant::LongLong),
	qStringTypePair_t("LONGLONGINT", QVariant::LongLong),
	qStringTypePair_t("LONGLONGINTEGER", QVariant::LongLong),

	qStringTypePair_t("UINT", QVariant::UInt),
	qStringTypePair_t("UI", QVariant::UInt),
	qStringTypePair_t("UINTEGER", QVariant::UInt),
	qStringTypePair_t("ULONG", QVariant::ULongLong),
	qStringTypePair_t("UL", QVariant::ULongLong),
	qStringTypePair_t("ULONGINT", QVariant::ULongLong),
	qStringTypePair_t("ULONGINTEGER", QVariant::ULongLong),
	qStringTypePair_t("ULONGLONG", QVariant::ULongLong),
	qStringTypePair_t("ULL", QVariant::ULongLong),
	qStringTypePair_t("ULONGLONGINT", QVariant::ULongLong),
	qStringTypePair_t("ULONGLONGINTEGER", QVariant::ULongLong),

	qStringTypePair_t("DOUBLE", QVariant::Double),
	qStringTypePair_t("D", QVariant::Double),
	qStringTypePair_t("F", QVariant::Double),
	qStringTypePair_t("FLOAT", QVariant::Double),

	qStringTypePair_t("DATE", QVariant::Date),
	qStringTypePair_t("TIME", QVariant::Time),
	qStringTypePair_t("DATETIME", QVariant::DateTime),

	qStringTypePair_t("STRING", QVariant::String),
	qStringTypePair_t("STR", QVariant::String),
	qStringTypePair_t("S", QVariant::String)
};
const int multimapStartValuesSize = sizeof(multimapStartValues) / sizeof(multimapStartValues[0]);
extern std::map<QString, QVariant::Type, cmpQStringInsensitive> nameTypeSurjection;

QString qVariantTypeToTypename(QVariant::Type);
QVariant::Type typenameToQVariantType(const QString &);

#endif // TOOLBOX_H
