#ifndef VIEW_H
#define VIEW_H

#include <QObject>
#include <QStringList>
#include <QWidget>

// abstract view
class View : public QObject
{
	Q_OBJECT

public:
	explicit View();
	/**
	 * @brief bindEvents binds all events required by the view
	 */
	virtual void bindEvents() = 0;

private slots:
	// slots required in all views
	/**
	 * @brief openFilesToParse ask user what file to parse. If files can be opened,
	 */
	virtual void openFilesToParse() = 0;

signals:
	// signals required in all views
	/**
	 * @brief databaseCreated throws when database must be created
	 * @param name the name of the database to create
	 */
	virtual void databaseCreated(QString*) = 0;
	/**
	 * @brief databaseOpened throws when database must be opened
	 * @param name the name of the database to open
	 */
	virtual void databaseOpened(QString*) = 0;
	/**
	 * @brief filesParsed throws when files must be parsed
	 * @param files contains a list of filename thet have to be parsed
	 */
	virtual void filesParsed(QStringList*) = 0;
	/**
	 * @brief experimentCampaignFinded throws when experiment campaign must be searched
	 * @param name the name or part name of the experiment campaign to find
	 */
	virtual void experimentCampaignCleared() = 0;
};

#endif // VIEW_H
