#include "src/modules/view/viewGUI/metricTable/MetricSelectorWidget.h"

/*QString MetricSelectorWidget::_timeFormat = QString("HH:mm:ss");
QString MetricSelectorWidget::_dateFormat = QString("dd.MM.yyyy");
QString MetricSelectorWidget::_dateTimeFormat = MetricSelectorWidget::_dateFormat + " " + MetricSelectorWidget::_timeFormat;*/

/*std::map<int, std::function<void()> > MetricSelectorWidget::_buildWidgetView_userTypeSwitchCase = std::map<int, std::function<void()> >();
std::map<int, std::function<void()> > MetricSelectorWidget::_updateValue_userTypeSwitchCase = std::map<int, std::function<void()> >();
std::map<int, std::function<void()> > MetricSelectorWidget::_onContainerValueChanged_userTypeSwitchCase = std::map<int, std::function<void()> >();*/

MetricSelectorWidget::MetricSelectorWidget(MetricSelector *metricSelector, QWidget *parent):
	MetricWidget(metricSelector, parent),
	_first(NULL), _second(NULL)
{
	addLambdaFunctions();
	setMetricContainer(metricSelector);

	_layout = new QHBoxLayout(this);
	setLayout(_layout);
	_layout->setContentsMargins(10, 0, 10, 0);

	buildWidgetView();
	if (_first != NULL) _first->blockSignals(true);
	if (_second != NULL) _second->blockSignals(true);
	onContainerValueChanged();
	if (_first != NULL) _first->blockSignals(false);
	if (_second != NULL) _second->blockSignals(false);

	setContentsMargins(0, 0, 0, 0);
}

MetricSelectorWidget::~MetricSelectorWidget()
{}

void MetricSelectorWidget::addLambdaFunctions()
{
	_buildWidgetView_userTypeSwitchCase[PlotStyleChooser::METATYPE_POINTER_ID] =
		[this](){
			PlotStyleChooserWidget *pscw = new PlotStyleChooserWidget(
				static_cast<MetricSelector *>(_metricContainer)->getBegin().value<PlotStyleChooser_p>().data(), this);
			_layout->addWidget(pscw);
			connect(pscw, &PlotStyleChooserWidget::valueChanged, this, &MetricSelectorWidget::widgetValueChanged);
			_first = pscw;
		};
	_updateValue_userTypeSwitchCase[PlotStyleChooser::METATYPE_POINTER_ID] =
		[this](){ emit valueChanged(); };
	_onContainerValueChanged_userTypeSwitchCase[PlotStyleChooser::METATYPE_POINTER_ID] =
		[this](){};
}

void MetricSelectorWidget::setMetricContainer(MetricSelector *metricSelector)
{
	if (_metricContainer != NULL)
	{
		// SIGSEGV to correct
		//disconnect(_metricSelector, &MetricSelector::containerValueChanged, this, &MetricSelectorWidget::onContainerValueChanged);
		disconnect(this, &MetricSelectorWidget::valueChanged, static_cast<MetricSelector *>(_metricContainer), &MetricSelector::onValueChange);
	}
	_metricContainer = metricSelector;
	connect(static_cast<MetricSelector *>(_metricContainer), &MetricSelector::containerValueChanged, this, &MetricSelectorWidget::onContainerValueChanged);
	connect(this, &MetricSelectorWidget::valueChanged, static_cast<MetricSelector *>(_metricContainer), &MetricSelector::onValueChange);
}

void MetricSelectorWidget::buildWidgetView()
{
	// create custom interface depending to value
	switch (_metricContainer->getType())
	{
	case QVariant::Bool:		// boolean value
	{
		QCheckBox *cb = new QCheckBox(this);
		_layout->addWidget(cb);
		connect(cb, &QCheckBox::stateChanged, this, &MetricSelectorWidget::widgetValueChanged);
		_first = cb;
		_layout->setAlignment(cb, Qt::AlignCenter);

		_second = NULL;
		break;
	}
	case QVariant::Char:		// integer value
	case QVariant::Int:
	case QVariant::UInt:
	case QVariant::LongLong:
	case QVariant::ULongLong:
	{
		QSpinBox *sb = new QSpinBox(this);
		_layout->addWidget(sb);
		connect(sb, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
			this, &MetricSelectorWidget::widgetValueChanged);
		_first = sb;

		sb = new QSpinBox(this);
		_layout->addWidget(sb);
		connect(sb, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
			this, &MetricSelectorWidget::widgetValueChanged);
		_second = sb;
		break;
	}
	case QVariant::Double:		// floating point value
	{
		QDoubleSpinBox *dsb = new QDoubleSpinBox(this);
		_layout->addWidget(dsb);
		connect(dsb, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
			this, &MetricSelectorWidget::widgetValueChanged);
		_first = dsb;

		dsb = new QDoubleSpinBox(this);
		_layout->addWidget(dsb);
		connect(dsb, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
			this, &MetricSelectorWidget::widgetValueChanged);
		_second = dsb;
		break;
	}
	case QVariant::String:		// text value
	{
		QLineEdit *le = new QLineEdit(this);
		_layout->addWidget(le);
		connect(le, &QLineEdit::editingFinished, this, &MetricSelectorWidget::widgetValueChanged);
		_first = le;

		_second = NULL;
		break;
	}
	case QVariant::Time:		// time value
	{
		QTimeEdit *te = new QTimeEdit(this);
		te->setDisplayFormat(_timeFormat);
		_layout->addWidget(te);
		connect(te, &QTimeEdit::timeChanged, this, &MetricSelectorWidget::widgetValueChanged);
		_first = te;

		te = new QTimeEdit(this);
		te->setDisplayFormat(_timeFormat);
		_layout->addWidget(te);
		connect(te, &QTimeEdit::timeChanged, this, &MetricSelectorWidget::widgetValueChanged);
		_second = te;
		break;
	}
	case QVariant::Date:		// date value
	{
		QDateEdit *de = new QDateEdit(this);
		de->setDisplayFormat(_dateFormat);
		_layout->addWidget(de);
		connect(de, &QTimeEdit::dateChanged, this, &MetricSelectorWidget::widgetValueChanged);
		_first = de;

		de = new QDateEdit(this);
		de->setDisplayFormat(_dateFormat);
		_layout->addWidget(de);
		connect(de, &QTimeEdit::dateChanged, this, &MetricSelectorWidget::widgetValueChanged);
		_second = de;
		break;
	}
	case QVariant::DateTime:	// datetime value
	{
		QDateTimeEdit *dte = new QDateTimeEdit(this);
		dte->setDisplayFormat(_dateTimeFormat);
		_layout->addWidget(dte);
		connect(dte, &QTimeEdit::dateTimeChanged, this, &MetricSelectorWidget::widgetValueChanged);
		_first = dte;

		dte = new QDateTimeEdit(this);
		dte->setDisplayFormat(_dateTimeFormat);
		_layout->addWidget(dte);
		connect(dte, &QTimeEdit::dateTimeChanged, this, &MetricSelectorWidget::widgetValueChanged);
		_second = dte;
		break;
	}
	case QVariant::UserType:	// user defined type
	{
		std::map<int, std::function<void()> >::iterator function = _buildWidgetView_userTypeSwitchCase.find(_metricContainer->getUserType());
		if (function != _buildWidgetView_userTypeSwitchCase.end())
			function->second();
		break;
	}
	default:					// other value type
	{
		QLabel *label = new QLabel(
			QString(QVariant::typeToName(_metricContainer->getType())) + " unknown", this);
		label->setStyleSheet("background: #FFD200;");
		label->setAlignment(Qt::AlignCenter);
		_layout->addWidget(label);
		_first = label;
		_second = NULL;
	}
	}
}

void MetricSelectorWidget::deleteWidgetView()
{}

void MetricSelectorWidget::updateValue()
{
	_metricContainer->blockSignals(true);
	MetricSelector *metricSelector = static_cast<MetricSelector *>(_metricContainer);
	// update value depening type
	switch (_metricContainer->getType())
	{
	case QVariant::Bool:		// boolean value
	{
		QCheckBox *cb = static_cast<QCheckBox*>(_first);
		metricSelector->setBegin(cb->isChecked());
		emit valueChanged();
		break;
	}
	case QVariant::Char:		// integer value
	case QVariant::Int:
	case QVariant::UInt:
	case QVariant::LongLong:
	case QVariant::ULongLong:
	{
		QSpinBox *sb = static_cast<QSpinBox*>(_first);
		metricSelector->setBegin(sb->value());
		sb = static_cast<QSpinBox*>(_second);
		metricSelector->setEnd(sb->value());
		emit valueChanged();
		break;
	}
	case QVariant::Double:		// floating point value
	{
		QDoubleSpinBox *dsb = static_cast<QDoubleSpinBox*>(_first);
		metricSelector->setBegin(dsb->value());
		dsb = static_cast<QDoubleSpinBox*>(_second);
		metricSelector->setEnd(dsb->value());
		emit valueChanged();
		break;
	}
	case QVariant::String:		// text value
	{
		QLineEdit *le = static_cast<QLineEdit*>(_first);
		metricSelector->setBegin(le->text());
		emit valueChanged();
		break;
	}
	case QVariant::Time:		// time value
	{
		QTimeEdit *te = static_cast<QTimeEdit*>(_first);
		metricSelector->setBegin(te->time());
		te = static_cast<QTimeEdit*>(_second);
		metricSelector->setEnd(te->time());
		emit valueChanged();
		break;
	}
	case QVariant::Date:		// date value
	{
		QDateEdit *de = static_cast<QDateEdit*>(_first);
		metricSelector->setBegin(de->date());
		de = static_cast<QDateEdit*>(_second);
		metricSelector->setEnd(de->date());
		emit valueChanged();
		break;
	}
	case QVariant::DateTime:	// datetime value
	{
		QDateTimeEdit *dte = static_cast<QDateTimeEdit*>(_first);
		metricSelector->setBegin(dte->dateTime());
		dte = static_cast<QDateTimeEdit*>(_second);
		metricSelector->setEnd(dte->dateTime());
		emit valueChanged();
		break;
	}
	case QVariant::UserType:	// user defined type
	{
		std::map<int, std::function<void()> >::iterator function = _updateValue_userTypeSwitchCase.find(_metricContainer->getUserType());
		if (function != _updateValue_userTypeSwitchCase.end())
			function->second();
		break;
	}
	default:					// other value type
	{
		// nothing
	}
	}
	_metricContainer->blockSignals(false);
}

// -------- SLOTS --------
void MetricSelectorWidget::onContainerValueChanged()
{
	MetricSelector *metricSelector = static_cast<MetricSelector *>(_metricContainer);
	// update value depening type
	switch (_metricContainer->getType())
	{
	case QVariant::Bool:		// boolean value
	{
		QCheckBox *cb = static_cast<QCheckBox*>(_first);
		cb->setChecked(metricSelector->getBegin().toBool());
		break;
	}
	case QVariant::Char:		// integer value
	case QVariant::Int:
	case QVariant::UInt:
	case QVariant::LongLong:
	case QVariant::ULongLong:
	{
		QSpinBox *sb = static_cast<QSpinBox*>(_first);
		sb->setMinimum(metricSelector->getMin().toInt());
		sb->setMaximum(metricSelector->getMax().toInt());
		sb->setValue(metricSelector->getBegin().toInt());
		sb = static_cast<QSpinBox*>(_second);
		sb->setMinimum(metricSelector->getMin().toInt());
		sb->setMaximum(metricSelector->getMax().toInt());
		sb->setValue(metricSelector->getEnd().toInt());
		break;
	}
	case QVariant::Double:		// floating point value
	{
		QDoubleSpinBox *dsb = static_cast<QDoubleSpinBox*>(_first);
		dsb->setMinimum(metricSelector->getMin().toDouble());
		dsb->setMaximum(metricSelector->getMax().toDouble());
		dsb->setValue(metricSelector->getBegin().toDouble());
		dsb = static_cast<QDoubleSpinBox*>(_second);
		dsb->setMinimum(metricSelector->getMin().toDouble());
		dsb->setMaximum(metricSelector->getMax().toDouble());
		dsb->setValue(metricSelector->getEnd().toDouble());
		break;
	}
	case QVariant::String:		// text value
	{
		QLineEdit *le = static_cast<QLineEdit*>(_first);
		le->setText(metricSelector->getBegin().toString());
		break;
	}
	case QVariant::Time:		// time value
	{
		QTimeEdit *te = static_cast<QTimeEdit*>(_first);
		te->setMinimumTime(metricSelector->getMin().toTime());
		te->setMaximumTime(metricSelector->getMax().toTime());
		te->setTime(metricSelector->getBegin().toTime());
		te = static_cast<QTimeEdit*>(_second);
		te->setMinimumTime(metricSelector->getMin().toTime());
		te->setMaximumTime(metricSelector->getMax().toTime());
		te->setTime(metricSelector->getEnd().toTime());
		break;
	}
	case QVariant::Date:		// date value
	{
		QDateEdit *de = static_cast<QDateEdit*>(_first);
		de->setMinimumDate(metricSelector->getMin().toDate());
		de->setMaximumDate(metricSelector->getMax().toDate());
		de->setDate(metricSelector->getBegin().toDate());
		de = static_cast<QDateEdit*>(_second);
		de->setMinimumDate(metricSelector->getMin().toDate());
		de->setMaximumDate(metricSelector->getMax().toDate());
		de->setDate(metricSelector->getEnd().toDate());
		break;
	}
	case QVariant::DateTime:	// datetime value
	{
		QDateTimeEdit *dte = static_cast<QDateTimeEdit*>(_first);
		dte->setMinimumDateTime(metricSelector->getMin().toDateTime());
		dte->setMinimumDateTime(metricSelector->getMax().toDateTime());
		dte->setDateTime(metricSelector->getBegin().toDateTime());
		dte = static_cast<QDateTimeEdit*>(_second);
		dte->setMinimumDateTime(metricSelector->getMin().toDateTime());
		dte->setMinimumDateTime(metricSelector->getMax().toDateTime());
		dte->setDateTime(metricSelector->getEnd().toDateTime());
		break;
	}
	case QVariant::UserType:	// user defined type
	{
		std::map<int, std::function<void()> >::iterator function = _onContainerValueChanged_userTypeSwitchCase.find(_metricContainer->getUserType());
		if (function != _onContainerValueChanged_userTypeSwitchCase.end())
			function->second();
		break;
	}
	default:					// other value type
	{
		// nothing
	}
	}
}

void MetricSelectorWidget::widgetValueChanged()
{
	updateValue();
}
