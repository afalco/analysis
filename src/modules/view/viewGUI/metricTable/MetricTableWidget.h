#ifndef SELECTTABLEWIDGET_H
#define SELECTTABLEWIDGET_H

#include "src/modules/view/viewGUI/metricTable/MetricSelectorWidget.h"
#include "src/modules/view/viewGUI/metricTable/MetricPlotStyleWidget.h"
#include "src/modules/view/viewGUI/metricTable/MetricModifierWidget.h"
#include "src/modules/model/metricTable/MetricTable.h"

#include <QTableWidget>
#include <QHeaderView>
#include <QVariant>

class MetricTableWidget : public QTableWidget
{
	Q_OBJECT

private:
	MetricTable *_selectTable;

	void updateView();

protected:
	void addLine(const QString, MetricContainer *);

public:
	explicit MetricTableWidget(QWidget *parent = 0);

	void setSelectTable(MetricTable *);
	MetricTable *getSelectTable();
	/*void insertItem(const QString, DatabaseTableMetrics*);
	void insertItems(SelectTable*);*/

signals:
	/**
	 * @brief valueChanged
	 * Emitted on each user modification on view components
	 */
	void valueChanged();

private slots:
	/**
	 * @brief onContainerElementChange
	 * Once connected to SelectTable container, is called on each
	 * insertion or deletion of element in container, but not on
	 * modification.
	 */
	void onContainerElementChange();

private slots:
};

#endif // SELECTTABLEWIDGET_H
