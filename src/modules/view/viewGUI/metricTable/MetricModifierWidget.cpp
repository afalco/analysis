#include "src/modules/view/viewGUI/metricTable/MetricModifierWidget.h"

MetricModifierWidget::MetricModifierWidget(MetricModifier *metricModifier, QWidget *parent):
	MetricWidget(metricModifier, parent),
	_selector(NULL)
{
	setMetricContainer(metricModifier);

	_layout = new QHBoxLayout(this);
	setLayout(_layout);
	_layout->setContentsMargins(10, 0, 10, 0);

	buildWidgetView();
	if (_selector != NULL) _selector->blockSignals(true);
	onContainerValueChanged();
	if (_selector != NULL) _selector->blockSignals(false);

	setContentsMargins(0, 0, 0, 0);
}

void MetricModifierWidget::buildWidgetView()
{
	// create custom interface depending to value
	switch (_metricContainer->getType())
	{
	case QVariant::Bool:		// boolean value
	{
		QCheckBox *cb = new QCheckBox(this);
		_layout->addWidget(cb);
		connect(cb, &QCheckBox::stateChanged, this, &MetricModifierWidget::widgetValueChanged);
		_selector = cb;
		_layout->setAlignment(cb, Qt::AlignCenter);
		break;
	}
	case QVariant::Char:		// integer value
	case QVariant::Int:
	case QVariant::UInt:
	case QVariant::LongLong:
	case QVariant::ULongLong:
	{
		QSpinBox *sb = new QSpinBox(this);
		_layout->addWidget(sb);
		connect(sb, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
			this, &MetricModifierWidget::widgetValueChanged);
		_selector = sb;
		break;
	}
	case QVariant::Double:		// floating point value
	{
		QDoubleSpinBox *dsb = new QDoubleSpinBox(this);
		_layout->addWidget(dsb);
		connect(dsb, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
			this, &MetricModifierWidget::widgetValueChanged);
		_selector = dsb;
		break;
	}
	case QVariant::String:		// text value
	{
		QLineEdit *le = new QLineEdit(this);
		_layout->addWidget(le);
		connect(le, &QLineEdit::editingFinished, this, &MetricModifierWidget::widgetValueChanged);
		_selector = le;
		break;
	}
	case QVariant::Time:		// time value
	{
		QTimeEdit *te = new QTimeEdit(this);
		te->setDisplayFormat(_timeFormat);
		_layout->addWidget(te);
		connect(te, &QTimeEdit::timeChanged, this, &MetricModifierWidget::widgetValueChanged);
		_selector = te;
		break;
	}
	case QVariant::Date:		// date value
	{
		QDateEdit *de = new QDateEdit(this);
		de->setDisplayFormat(_dateFormat);
		_layout->addWidget(de);
		connect(de, &QTimeEdit::dateChanged, this, &MetricModifierWidget::widgetValueChanged);
		_selector = de;
		break;
	}
	case QVariant::DateTime:	// datetime value
	{
		QDateTimeEdit *dte = new QDateTimeEdit(this);
		dte->setDisplayFormat(_dateTimeFormat);
		_layout->addWidget(dte);
		connect(dte, &QTimeEdit::dateTimeChanged, this, &MetricModifierWidget::widgetValueChanged);
		_selector = dte;
		break;
	}
	case QVariant::UserType:	// user defined type
	{
		std::map<int, std::function<void()> >::iterator function = _buildWidgetView_userTypeSwitchCase.find(_metricContainer->getUserType());
		if (function != _buildWidgetView_userTypeSwitchCase.end())
			function->second();
		break;
	}
	default:					// other value type
	{
		QLabel *label = new QLabel(
			QString(QVariant::typeToName(_metricContainer->getType())) + " unknown", this);
		label->setStyleSheet("background: #FFD200;");
		label->setAlignment(Qt::AlignCenter);
		_layout->addWidget(label);
		_selector = label;
	}
	}
}

void MetricModifierWidget::deleteWidgetView()
{}

void MetricModifierWidget::updateValue()
{
	_metricContainer->blockSignals(true);
	MetricModifier *metricModifier = static_cast<MetricModifier *>(_metricContainer);
	// update value depening type
	switch (_metricContainer->getType())
	{
	case QVariant::Bool:		// boolean value
	{
		QCheckBox *cb = static_cast<QCheckBox*>(_selector);
		metricModifier->setValue(cb->isChecked());
		emit valueChanged();
		break;
	}
	case QVariant::Char:		// integer value
	case QVariant::Int:
	case QVariant::UInt:
	case QVariant::LongLong:
	case QVariant::ULongLong:
	{
		QSpinBox *sb = static_cast<QSpinBox*>(_selector);
		metricModifier->setValue(sb->value());
		emit valueChanged();
		break;
	}
	case QVariant::Double:		// floating point value
	{
		QDoubleSpinBox *dsb = static_cast<QDoubleSpinBox*>(_selector);
		metricModifier->setValue(dsb->value());
		emit valueChanged();
		break;
	}
	case QVariant::String:		// text value
	{
		QLineEdit *le = static_cast<QLineEdit*>(_selector);
		metricModifier->setValue(le->text());
		emit valueChanged();
		break;
	}
	case QVariant::Time:		// time value
	{
		QTimeEdit *te = static_cast<QTimeEdit*>(_selector);
		metricModifier->setValue(te->time());
		emit valueChanged();
		break;
	}
	case QVariant::Date:		// date value
	{
		QDateEdit *de = static_cast<QDateEdit*>(_selector);
		metricModifier->setValue(de->date());
		emit valueChanged();
		break;
	}
	case QVariant::DateTime:	// datetime value
	{
		QDateTimeEdit *dte = static_cast<QDateTimeEdit*>(_selector);
		metricModifier->setValue(dte->dateTime());
		emit valueChanged();
		break;
	}
	case QVariant::UserType:	// user defined type
	{
		std::map<int, std::function<void()> >::iterator function = _updateValue_userTypeSwitchCase.find(_metricContainer->getUserType());
		if (function != _updateValue_userTypeSwitchCase.end())
			function->second();
		break;
	}
	default:					// other value type
	{
		// nothing
	}
	}
	_metricContainer->blockSignals(false);
}

void MetricModifierWidget::setMetricContainer(MetricModifier *metricModifier)
{
	if (_metricContainer != NULL)
	{
		// SIGSEGV to correct
		//disconnect(_metricSelector, &MetricModifier::containerValueChanged, this, &MetricModifierWidget::onContainerValueChanged);
		disconnect(this, &MetricModifierWidget::valueChanged, static_cast<MetricModifier *>(_metricContainer), &MetricModifier::onValueChange);
	}
	_metricContainer = metricModifier;
	connect(static_cast<MetricModifier *>(_metricContainer), &MetricModifier::containerValueChanged, this, &MetricModifierWidget::onContainerValueChanged);
	connect(this, &MetricModifierWidget::valueChanged, static_cast<MetricModifier *>(_metricContainer), &MetricModifier::onValueChange);
}

// -------- SLOTS --------
void MetricModifierWidget::onContainerValueChanged()
{
	MetricModifier *metricModifier = static_cast<MetricModifier *>(_metricContainer);
	// update value depening type
	switch (_metricContainer->getType())
	{
	case QVariant::Bool:		// boolean value
	{
		QCheckBox *cb = static_cast<QCheckBox*>(_selector);
		cb->setChecked(metricModifier->getValue().toBool());
		break;
	}
	case QVariant::Char:		// integer value
	case QVariant::Int:
	case QVariant::UInt:
	case QVariant::LongLong:
	case QVariant::ULongLong:
	{
		QSpinBox *sb = static_cast<QSpinBox*>(_selector);
		sb->setValue(metricModifier->getValue().toInt());
		break;
	}
	case QVariant::Double:		// floating point value
	{
		QDoubleSpinBox *dsb = static_cast<QDoubleSpinBox*>(_selector);
		dsb->setValue(metricModifier->getValue().toDouble());
		break;
	}
	case QVariant::String:		// text value
	{
		QLineEdit *le = static_cast<QLineEdit*>(_selector);
		le->setText(metricModifier->getValue().toString());
		break;
	}
	case QVariant::Time:		// time value
	{
		QTimeEdit *te = static_cast<QTimeEdit*>(_selector);
		te->setTime(metricModifier->getValue().toTime());
		break;
	}
	case QVariant::Date:		// date value
	{
		QDateEdit *de = static_cast<QDateEdit*>(_selector);
		de->setDate(metricModifier->getValue().toDate());
		break;
	}
	case QVariant::DateTime:	// datetime value
	{
		QDateTimeEdit *dte = static_cast<QDateTimeEdit*>(_selector);
		dte->setDateTime(metricModifier->getValue().toDateTime());
		break;
	}
	case QVariant::UserType:	// user defined type
	{
		std::map<int, std::function<void()> >::iterator function = _onContainerValueChanged_userTypeSwitchCase.find(_metricContainer->getUserType());
		if (function != _onContainerValueChanged_userTypeSwitchCase.end())
			function->second();
		break;
	}
	default:					// other value type
	{
		// nothing
	}
	}
}

void MetricModifierWidget::widgetValueChanged()
{
	updateValue();
}
