#include "src/modules/view/viewGUI/metricTable/MetricWidget.h"

QString MetricWidget::_timeFormat = QString("HH:mm:ss");
QString MetricWidget::_dateFormat = QString("dd.MM.yyyy");
QString MetricWidget::_dateTimeFormat = MetricWidget::_dateFormat + " " + MetricWidget::_timeFormat;

MetricWidget::MetricWidget(MetricContainer *metricContainer, QWidget *parent):
	QWidget(parent),
	_buildWidgetView_userTypeSwitchCase(),
	_updateValue_userTypeSwitchCase(),
	_onContainerValueChanged_userTypeSwitchCase()
{
	setMetricContainer(metricContainer);
}

void MetricWidget::setMetricContainer(MetricContainer *metricContainer)
{
	if (_metricContainer != NULL)
	{
		// SIGSEGV to correct
		//disconnect(_metricSelector, &MetricContainer::containerValueChanged, this, &MetricWidget::onContainerValueChanged);
		disconnect(this, &MetricWidget::valueChanged, _metricContainer, &MetricContainer::onValueChange);
	}
	_metricContainer = metricContainer;
	connect(_metricContainer, &MetricContainer::containerValueChanged, this, &MetricWidget::onContainerValueChanged);
	connect(this, &MetricWidget::valueChanged, _metricContainer, &MetricContainer::onValueChange);
}

MetricContainer *MetricWidget::getMetricContainer()
{
	return _metricContainer;
}

QVariant::Type MetricWidget::getType()
{
	return _metricContainer->getType();
}

void MetricWidget::addLambdaFunctions()
{}
