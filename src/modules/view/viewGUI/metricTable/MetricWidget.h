#ifndef METRICWIDGET_H
#define METRICWIDGET_H

#include "src/modules/model/metricTable/MetricContainer.h"

#include <QWidget>

#include <map>
#include <functional>

class MetricWidget : public QWidget
{
	Q_OBJECT

protected:
	static QString _timeFormat,	// format to time values
		_dateFormat,			// format to date values
		_dateTimeFormat;		// format to datetime values

	/*
	 * Used because of metatype registered with Qt have an ID to reference
	 * them, but this id cannot be known at compile time.
	 */
	std::map<int, std::function<void()> > _buildWidgetView_userTypeSwitchCase,
		_updateValue_userTypeSwitchCase,
		_onContainerValueChanged_userTypeSwitchCase;

	MetricContainer *_metricContainer;	// associated container

	virtual void addLambdaFunctions();

	/**
	 * @brief buildWidgetView
	 * Build the econtent of the widget and bind required events
	 */
	virtual void buildWidgetView() = 0;

	/**
	 * @brief deleteWidgetView
	 * Correctly delete widget contents in order to replace it's widgets, in case of type changed for exampple
	 */
	virtual void deleteWidgetView() = 0;

	/**
	 * @brief updateValue
	 * Update values of this in relation to it's widget's elements
	 */
	virtual void updateValue() = 0;

public:
	explicit MetricWidget(MetricContainer *, QWidget *parent = 0);

	virtual void setMetricContainer(MetricContainer *);

	/**
	 * @brief getMetricContainer
	 * Update values with void updateValues() method and return a MetricSelector
	 * @return A MetricContainer containing actual widget values
	 */
	MetricContainer *getMetricContainer();

	QVariant::Type getType();

signals:
	/**
	 * @brief valueChanged
	 * Emitted to notify the associated MetricSelector of values changes
	 */
	void valueChanged();

public slots:
	/**
	 * @brief containerValueChanged
	 * Once connected to model component, is used by them to notify a value update
	 */
	virtual void onContainerValueChanged() = 0;

	virtual void widgetValueChanged() = 0;

	// slot bind to all MetricSelectors in the widget. It calls them when they're updated
	//void updateValue(MetricSelector *);
};

#endif // METRICWIDGET_H
