#include "src/modules/view/viewGUI/metricTable/MetricPlotStyleWidget.h"

// Part related to PlotStyle namespace ----------------------------------------
namespace PlotStyle {

MyColorDialog::MyColorDialog(QWidget *parent):
	QColorDialog(parent)
{
	setOption(QColorDialog::DontUseNativeDialog);
	setOption(QColorDialog::NoButtons);
	setWindowFlags(Qt::Widget);
}

void MyColorDialog::keyPressEvent(QKeyEvent *e) {
	if(e->key() != Qt::Key_Escape)
		QColorDialog::keyPressEvent(e);
}

ModalSelector::ModalSelector(MetricPlotStyleWidget *sender, QWidget *parent):
	QDialog(parent),
	_sender(sender)
{
	buildWidgetView();
	fillInterface();
	updateValues();
}

void ModalSelector::closeEvent(QCloseEvent *ce)
{
	updateValues();
	QDialog::closeEvent(ce);
}

void ModalSelector::buildWidgetView()
{
	// -- main layout
	_layout = new QHBoxLayout(this);
	setLayout(_layout);

	// ---- line group box
	_line_group = new QGroupBox("Line style", this);
	_layout->addWidget(_line_group);
	_line_layout = new QGridLayout(_line_group);
	_line_group->setLayout(_line_layout);

	// ------ color chooser
	_line_color_dialog = new MyColorDialog(_line_group);
	_line_layout->addWidget(_line_color_dialog, 0, 0, 1, 4);

	// ------ style chooser
	_line_layout->addWidget(new QLabel("Style",_line_group), 1, 0);
	_line_style = new QComboBox(_line_group);
	_line_layout->addWidget(_line_style, 1, 1);

	// ------ size chooser
	_line_layout->addWidget(new QLabel("Width",_line_group), 1, 2);
	_line_width = new QDoubleSpinBox(_line_group);
	_line_layout->addWidget(_line_width, 1, 3);

	// ---- dot group box
	_dot_group = new QGroupBox("Dot style", this);
	_layout->addWidget(_dot_group);
	_dot_layout = new QGridLayout(_dot_group);
	_dot_group->setLayout(_dot_layout);

	// ------ color chooser
	_dot_color_dialog = new MyColorDialog(_dot_group);
	_dot_layout->addWidget(_dot_color_dialog, 0, 0, 1, 4);

	// ------ style chooser
	_dot_layout->addWidget(new QLabel("Style",_line_group), 1, 0);
	_dot_style = new QComboBox(_dot_group);
	_dot_layout->addWidget(_dot_style, 1, 1);

	// ------ size chooser
	_dot_layout->addWidget(new QLabel("Size",_line_group), 1, 2);
	_dot_size = new QDoubleSpinBox(_dot_group);
	_dot_layout->addWidget(_dot_size, 1, 3);
}

void ModalSelector::fillInterface()
{
	// insert static data
	_line_style->addItem(
		QString(MetricPlotStyle::lineTypeToName(MetricPlotStyle::NoLine)),
		QVariant::fromValue(MetricPlotStyle::NoLine));
	_line_style->addItem(
		QString(MetricPlotStyle::lineTypeToName(MetricPlotStyle::Continuous)),
		QVariant::fromValue(MetricPlotStyle::Continuous));
	_line_style->addItem(
		QString(MetricPlotStyle::lineTypeToName(MetricPlotStyle::Dashed)),
		QVariant::fromValue(MetricPlotStyle::Dashed));
	_line_style->addItem(
		QString(MetricPlotStyle::lineTypeToName(MetricPlotStyle::Dotted)),
		QVariant::fromValue(MetricPlotStyle::Dotted));
	_line_style->addItem(
		QString(MetricPlotStyle::lineTypeToName(MetricPlotStyle::DashedDotted)),
		QVariant::fromValue(MetricPlotStyle::DashedDotted));
	_line_style->addItem(
		QString(MetricPlotStyle::lineTypeToName(MetricPlotStyle::DashedLarge)),
		QVariant::fromValue(MetricPlotStyle::DashedLarge));
	_line_style->addItem(
		QString(MetricPlotStyle::lineTypeToName(MetricPlotStyle::DashedDottedLarge)),
		QVariant::fromValue(MetricPlotStyle::DashedDottedLarge));

	addDotStyleChoice(MetricPlotStyle::EmptySquare);
	addDotStyleChoice(MetricPlotStyle::NoDot);
	addDotStyleChoice(MetricPlotStyle::EmptyCircle);
	addDotStyleChoice(MetricPlotStyle::EmptyTriangle);
	addDotStyleChoice(MetricPlotStyle::Plus);
	addDotStyleChoice(MetricPlotStyle::Cross);
	addDotStyleChoice(MetricPlotStyle::EmptyLosange);
	addDotStyleChoice(MetricPlotStyle::EmptyInvertedTriangle);
	addDotStyleChoice(MetricPlotStyle::SquareWithCross);
	addDotStyleChoice(MetricPlotStyle::Star);
	addDotStyleChoice(MetricPlotStyle::LosangeWithPlus);
	addDotStyleChoice(MetricPlotStyle::CircleWithPlus);
	addDotStyleChoice(MetricPlotStyle::TrianglesTopBottom);
	addDotStyleChoice(MetricPlotStyle::SquareWithPlus);
	addDotStyleChoice(MetricPlotStyle::CircleWithCross);
	addDotStyleChoice(MetricPlotStyle::SquareWithTriangle);
	addDotStyleChoice(MetricPlotStyle::Square);
	addDotStyleChoice(MetricPlotStyle::Circle);
	addDotStyleChoice(MetricPlotStyle::Triangle);
	addDotStyleChoice(MetricPlotStyle::Losange);
	addDotStyleChoice(MetricPlotStyle::BigCircle);
	addDotStyleChoice(MetricPlotStyle::SmallCircle);
	addDotStyleChoice(MetricPlotStyle::FilledCircle);
	addDotStyleChoice(MetricPlotStyle::FilledSquare);
	addDotStyleChoice(MetricPlotStyle::FilledLosange);
	addDotStyleChoice(MetricPlotStyle::FilledTriangle);
	addDotStyleChoice(MetricPlotStyle::FiledReversedTriangle);

	// set current state
	MetricPlotStyle *mpl = static_cast<MetricPlotStyle *>(_sender->getMetricContainer());
	_line_color_dialog->setCurrentColor(mpl->getLineColor());
	_dot_color_dialog->setCurrentColor(mpl->getDotColor());
	_line_style->setCurrentIndex(
		_line_style->findData(
			QVariant(mpl->getLineType())));
	_dot_style->setCurrentIndex(
		_dot_style->findData(
			QVariant(mpl->getDotType())));
	_line_width->setValue(mpl->getLineWidth());
	_dot_size->setValue(mpl->getDotSize());
}

void ModalSelector::updateValues()
{
	MetricPlotStyle *mpl = static_cast<MetricPlotStyle *>(_sender->getMetricContainer());
	mpl->setLineColor(_line_color_dialog->currentColor());
	mpl->setDotColor(_dot_color_dialog->currentColor());
	mpl->setLineType(_line_style->currentData().value<MetricPlotStyle::LineType>());
	mpl->setDotType(_dot_style->currentData().value<MetricPlotStyle::DotType>());
	mpl->setLineWidth(_line_width->value());
	mpl->setDotSize(_dot_size->value());
}

void ModalSelector::addDotStyleChoice(MetricPlotStyle::DotType type)
{
	_dot_style->addItem(
		QString(MetricPlotStyle::dotTypeToName(type)),
		QVariant::fromValue(type));
}

}

// Part related to MetricPlotStyleWidget --------------------------------------
MetricPlotStyleWidget::MetricPlotStyleWidget(MetricPlotStyle *metricPlotStyle, QWidget *parent):
	MetricWidget(metricPlotStyle, parent)
{
	_layout = new QHBoxLayout(this);
	setLayout(_layout);
	_layout->setContentsMargins(10, 0, 10, 0);

	buildWidgetView();

	setContentsMargins(0, 0, 0, 0);
}

MetricPlotStyleWidget::~MetricPlotStyleWidget()
{}

void MetricPlotStyleWidget::buildWidgetView()
{
	_change_style = new QPushButton("Select style", this);
	connect(_change_style, &QPushButton::released, this, &MetricPlotStyleWidget::onStyleChange);
	_layout->addWidget(_change_style);
}

void MetricPlotStyleWidget::deleteWidgetView()
{}

void MetricPlotStyleWidget::updateValue()
{}

// -------- SLOTS --------
void MetricPlotStyleWidget::onStyleChange()
{
	PlotStyle::ModalSelector ms(this);
	ms.exec();
}

void MetricPlotStyleWidget::onContainerValueChanged()
{}

void MetricPlotStyleWidget::widgetValueChanged()
{
	updateValue();
}
