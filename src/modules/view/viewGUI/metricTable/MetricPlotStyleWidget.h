#ifndef METRICPLOTSTYLEWIDGET_H
#define METRICPLOTSTYLEWIDGET_H

#include "src/modules/view/viewGUI/metricTable/MetricWidget.h"
#include "src/modules/model/metricTable/MetricPlotStyle.h"

#include <QHBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include <QDialog>
#include <QColorDialog>
#include <QGroupBox>
#include <QKeyEvent>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QLabel>

class MetricPlotStyleWidget;

namespace PlotStyle {

class MyColorDialog : public QColorDialog {
	Q_OBJECT
private:
	void keyPressEvent(QKeyEvent *);
public:
	explicit MyColorDialog(QWidget *parent = 0);
};

class ModalSelector : public QDialog {
	Q_OBJECT

private:
	MetricPlotStyleWidget *_sender;

	QHBoxLayout *_layout;
	QGridLayout *_line_layout,
		*_dot_layout;
	QGroupBox *_line_group,
		*_dot_group;
	MyColorDialog *_line_color_dialog,
		*_dot_color_dialog;
	QComboBox *_line_style,
		*_dot_style;
	QDoubleSpinBox *_line_width,
		*_dot_size;

	void buildWidgetView();
	void fillInterface();
	void updateValues();
	void addDotStyleChoice(MetricPlotStyle::DotType);

protected:
	void closeEvent(QCloseEvent *);

public:
	explicit ModalSelector(MetricPlotStyleWidget *, QWidget *parent = 0);
};

}

class MetricPlotStyleWidget : public MetricWidget
{
	Q_OBJECT

private:
	QHBoxLayout *_layout;
	QPushButton *_change_style;

	void buildWidgetView();
	void deleteWidgetView();
	void updateValue();

public:
	explicit MetricPlotStyleWidget(MetricPlotStyle *, QWidget *parent = 0);
	~MetricPlotStyleWidget();

signals:
	void valueChanged();

private slots:
	void onStyleChange();

public slots:
	void onContainerValueChanged();
	void widgetValueChanged();
};

#endif // METRICPLOTSTYLEWIDGET_H
