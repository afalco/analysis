#include "src/modules/view/viewGUI/metricTable/MetricTableWidget.h"

MetricTableWidget::MetricTableWidget(QWidget *parent):
	QTableWidget(parent)
{
	// data initialisation
	_selectTable = NULL;

	// widget initialisation
	setColumnCount(3);
	setHorizontalHeaderLabels(QStringList() << "Metric" << "Interval" << "Type");
	horizontalHeader()->setStretchLastSection(true);	// last column always stretch right border

}

void MetricTableWidget::updateView()
{
	// reset view clearing table contents
	setRowCount(0);

	// set updated table contents
	for (std::multimap<QString, MetricContainer*>::const_iterator it = _selectTable->firstItemIterator(),
			last = _selectTable->lastItemIterator();
		it != last;
		++it) {
		addLine(it->first, it->second);
	}
}

void MetricTableWidget::addLine(const QString name, MetricContainer *metricContainer)
{

	int row = rowCount();
	setRowCount(row + 1);
	//setRowHeight(row, 20);

	QTableWidgetItem *item = new QTableWidgetItem(name);
	item->setFlags(item->flags() ^ Qt::ItemIsEditable);
	setItem(row, 0, item);

	//setCellWidget(row, 1, createSelector(value));
	//setCellWidget(row, 1, new MetricSelectorWidget(metricSelector->getType(), metricSelector->getDatabaseTableMetrics()));
	if (MetricSelector *ms = dynamic_cast<MetricSelector*>(metricContainer))
		setCellWidget(row, 1, new MetricSelectorWidget(ms));
	else if (MetricPlotStyle *mps = dynamic_cast<MetricPlotStyle*>(metricContainer))
		setCellWidget(row, 1, new MetricPlotStyleWidget(mps));
	else if (MetricModifier *mm = dynamic_cast<MetricModifier*>(metricContainer))
		setCellWidget(row, 1, new MetricModifierWidget(mm));
	else
		setCellWidget(row, 1, new QLabel("Unknown type"));

	item = new QTableWidgetItem(QVariant::typeToName(metricContainer->getType()));
	item->setFlags(item->flags() ^ Qt::ItemIsEditable);
	setItem(row, 2, item);

	resizeRowToContents(row);
	resizeColumnsToContents();
}

void MetricTableWidget::setSelectTable(MetricTable *selectTable)
{
	if (_selectTable != NULL) { // disconnect events from actual SelectTable
		disconnect(_selectTable, &MetricTable::elementChange, this,	&MetricTableWidget::onContainerElementChange);
	}

	// change SelectTable object
	_selectTable = selectTable;

	updateView();

	// connect events for new SelectTable
	connect(_selectTable, &MetricTable::elementChange, this, &MetricTableWidget::onContainerElementChange);
}

MetricTable *MetricTableWidget::getSelectTable()
{
	return _selectTable;
}

/*void SelectTableWidget::insertItem(const QString name, DatabaseTableMetrics *table)
{
	SelectTable::insertItem(name, table);
	addLine(name, table->getMetric(name.toStdString()));
}

void SelectTableWidget::insertItems(SelectTable *selectTable)
{
	QMultiMap<QString, DatabaseTableMetrics*> items = selectTable->getItems();
	for (QMultiMap<QString, DatabaseTableMetrics*>::iterator it = items.begin();
		it != items.end(); it++)
	{
		insertItem(static_cast<QString>(it.key()), static_cast<DatabaseTableMetrics*>(it.value()));
	}
}*/

// -------- SLOTS --------
void MetricTableWidget::onContainerElementChange()
{
	updateView();
}
