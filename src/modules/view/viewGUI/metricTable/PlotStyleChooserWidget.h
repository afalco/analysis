#ifndef PLOTSTYLECHOOSERWIDGET_H
#define PLOTSTYLECHOOSERWIDGET_H

#include "src/modules/view/viewGUI/PreviewWidget.h"
#include "src/modules/model/metricTable/PlotStyleChooser.h"

#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QDoubleSpinBox>
#include <QColorDialog>

class PlotStyleChooserWidget : public QWidget
{
	Q_OBJECT

private:
	QHBoxLayout *_layout;
	QPushButton *_color_choose;
	PreviewWidget *_preview;
	QDoubleSpinBox *_line_width;

	PlotStyleChooser *_plotStyleChooser;

	void buildInterface();

public:
	explicit PlotStyleChooserWidget(PlotStyleChooser*, QWidget *parent = 0);

	PlotStyleChooser *getPlotStyleChooser() const;

private slots:
	/**
	 * @brief queryColor
	 * Show a modal color dialog to choose a new color
	 */
	void queryColor();

signals:
	/**
	 * @brief valueChanged
	 * Send when an update is made
	 */
	void valueChanged();
};

#endif // PLOTSTYLECHOOSERWIDGET_H
