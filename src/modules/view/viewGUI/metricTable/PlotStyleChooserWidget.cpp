#include "src/modules/view/viewGUI/metricTable/PlotStyleChooserWidget.h"

PlotStyleChooserWidget::PlotStyleChooserWidget(PlotStyleChooser *plotStyleChooser, QWidget *parent):
	QWidget(parent),
	_plotStyleChooser(plotStyleChooser)
{
	buildInterface();
	connect(_color_choose, &QPushButton::released, this, &PlotStyleChooserWidget::queryColor);
}

void PlotStyleChooserWidget::buildInterface()
{
	// -- main layout
	_layout = new QHBoxLayout(this);
	setLayout(_layout);
	_layout->setContentsMargins(0, 0, 0, 0);

	// ---- color chooser button
	_color_choose = new QPushButton("Choose color", this);
	_layout->addWidget(_color_choose);

	// ---- previewer
	_preview = new PreviewWidget(this);
	_layout->addWidget(_preview);

	// ---- line width selector
	_line_width = new QDoubleSpinBox(this);
	_layout->addWidget(_line_width);
}

PlotStyleChooser *PlotStyleChooserWidget::getPlotStyleChooser() const
{
	return _plotStyleChooser;
}

// -------- SLOTS --------
void PlotStyleChooserWidget::queryColor()
{
	QColor newColor = QColorDialog::getColor(
		_plotStyleChooser->getColor(), this, "Chose plot color", QColorDialog::DontUseNativeDialog);
	if (newColor.isValid())
	{
		_plotStyleChooser->setColor(newColor);
		emit valueChanged();
	}
}
