#ifndef METRICMODIFIERWIDGET_H
#define METRICMODIFIERWIDGET_H

#include "src/modules/view/viewGUI/metricTable/MetricWidget.h"
#include "src/modules/model/metricTable/MetricModifier.h"

#include <QHBoxLayout>
#include <QWidget>
#include <QVariant>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QTimeEdit>
#include <QDateEdit>
#include <QDateTimeEdit>
#include <QLabel>
#include <QCheckBox>

class MetricModifierWidget : public MetricWidget
{
	Q_OBJECT

private:
	QHBoxLayout *_layout;
	QWidget *_selector;

	void buildWidgetView();
	void deleteWidgetView();
	void updateValue();

public:
	explicit MetricModifierWidget(MetricModifier *, QWidget *parent = 0);

	void setMetricContainer(MetricModifier *);

public slots:
	void onContainerValueChanged();
	void widgetValueChanged();
};

#endif // METRICMODIFIERWIDGET_H
