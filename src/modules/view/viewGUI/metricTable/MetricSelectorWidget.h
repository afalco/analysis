#ifndef METRICSELECTORWIDGET_H
#define METRICSELECTORWIDGET_H

#include "src/modules/model/database/DatabaseModule.h"
#include "src/modules/view/viewGUI/metricTable/MetricWidget.h"
#include "src/modules/model/metricTable/MetricSelector.h"
#include "src/modules/model/metricTable/PlotStyleChooser.h"
#include "src/modules/view/viewGUI/metricTable/PlotStyleChooserWidget.h"

#include <QWidget>
#include <QVariant>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QTimeEdit>
#include <QDateEdit>
#include <QDateTimeEdit>
#include <QLabel>
#include <QCheckBox>

#include <map>
#include <functional>

class MetricSelectorWidget : public MetricWidget
{
	Q_OBJECT

private:

	/*MetricSelector *_metricSelector;	// associated container

	static QString _timeFormat,	// format to time values
		_dateFormat,			// format to date values
		_dateTimeFormat;		// format to datetime values*/
	QHBoxLayout *_layout;		// main layout
	QWidget *_first,			// first selector widget
		*_second;				// second selector widget or NULL in case of string or boolean

	void addLambdaFunctions();

	/**
	 * @brief buildWidgetView
	 * Build the econtent of the widget and bind required events
	 */
	void buildWidgetView();

	/**
	 * @brief deleteWidgetView
	 * Correctly delete widget contents in order to replace it's widgets, in case of type changed for exampple
	 */
	void deleteWidgetView();

	/**
	 * @brief updateValue
	 * Update values of this in relation to it's widget's elements
	 */
	void updateValue();

public:
	explicit MetricSelectorWidget(MetricSelector *, QWidget *parent = 0);
	~MetricSelectorWidget();

	void setMetricContainer(MetricSelector *);

signals:
	/**
	 * @brief valueChanged
	 * Emitted to notify the associated MetricSelector of values changes
	 */
	void valueChanged();

public slots:
	/**
	 * @brief containerValueChanged
	 * Once connected to model component, is used by them to notify a value update
	 */
	void onContainerValueChanged();

	void widgetValueChanged();

	// slot bind to all MetricSelectors in the widget. It calls them when they're updated
	//void updateValue(MetricSelector *);

signals:
	// signal emitted when a value is updated, contains the MetricSelector associated to the value
	//void valueUpdated(MetricSelector *);
};

#endif // METRICSELECTORWIDGET_H
