#include "src/modules/view/viewGUI/tab/ManagementWidget.h"

ManagementWidget::ManagementWidget(QWidget *parent) :		QWidget(parent)
{
	buildInterface();
	bindEvents();
	fillInterface();
}

ManagementWidget::~ManagementWidget()
{}

void ManagementWidget::buildInterface()
{
	// -- main layout
	_main_layout = new QGridLayout(this);
	setLayout(_main_layout);

	// ---- horizontal splitter for finder and data parts
	_find_data_splitter = new QSplitter(Qt::Horizontal, this);
	_main_layout->addWidget(_find_data_splitter, 0, 0, 1, 2);

	// ------ vertical splitter for selecting data and create experiment
	_finder_experiment_splitter = new QSplitter(Qt::Vertical, _find_data_splitter);
	_find_data_splitter->addWidget(_finder_experiment_splitter);

	// -------- data finder table widget
	_find_experiment_campaign_list = new QListWidget(_finder_experiment_splitter);
	_finder_experiment_splitter->addWidget(_find_experiment_campaign_list);

	// -------- groupe of experiment modification
	_manage_experiment_group = new QGroupBox("Experiment campaign", _finder_experiment_splitter);
	_finder_experiment_splitter->addWidget(_manage_experiment_group);
	_manage_experiment_layout = new QGridLayout(_manage_experiment_group);
	_manage_experiment_group->setLayout(_manage_experiment_layout);

	// ---------- find an experiment
	_experiment_name_label = new QLabel("Name", _manage_experiment_group);
	_manage_experiment_layout->addWidget(_experiment_name_label, 0, 0);
	_experiment_campaign_name = new QLineEdit(_manage_experiment_group);
	_manage_experiment_layout->addWidget(_experiment_campaign_name, 0, 1);

	// ---------- experiment description
	_experiment_description_label = new QLabel("Description", _manage_experiment_group);
	_manage_experiment_layout->addWidget(_experiment_description_label, 1, 0, 1, 2);
	_experiment_campaign_description = new QTextEdit(_manage_experiment_group);
	_manage_experiment_layout->addWidget(_experiment_campaign_description, 2, 0, 1, 2);

	// ---------- experiments lists
	_experiment_list_label = new QLabel("Experiments", _manage_experiment_group);
	_manage_experiment_layout->addWidget(_experiment_list_label, 3, 0, 1, 2);
	_experiment_list = new QListWidget(_manage_experiment_group);
	_manage_experiment_layout->addWidget(_experiment_list, 4, 0, 1, 2);
	_experiment_list->setSelectionMode(QAbstractItemView::ExtendedSelection);

	// ------ data content
	_data_widget = new QWidget(_find_data_splitter);
	_find_data_splitter->addWidget(_data_widget);
	_data_layout = new QVBoxLayout(_data_widget);
	_data_widget->setLayout(_data_layout);

	// -------- data label
	_data_label = new QLabel("Content", _data_widget);
	_data_layout->addWidget(_data_label);

	// -------- data table
	_metric_table = new MetricTableWidget(_data_widget);
	_data_layout->addWidget(_metric_table);
	_metric_table->setColumnCount(3);

	// ---- import button
	_import_button = new QPushButton("Import", this);
	_main_layout->addWidget(_import_button, 1, 0);

	_find_data_splitter->setStretchFactor(0, 1);
	_find_data_splitter->setStretchFactor(1, 2);
}

void ManagementWidget::bindEvents()
{
	connect(_find_experiment_campaign_list, &QListWidget::currentItemChanged, this, &ManagementWidget::onExperimentCampaignSelected);
	connect(_experiment_list, &QListWidget::itemSelectionChanged, this, &ManagementWidget::onExperimentsSelected);
	connect(_import_button, &QPushButton::released, this, &ManagementWidget::updateCurrentValues);
}

void ManagementWidget::fillInterface()
{}

// -------- SLOTS --------
void ManagementWidget::onExperimentCampaignSelected(QListWidgetItem *item)
{
	if (item != NULL)
		emit experimentCampaignSelected(item->data(Qt::UserRole).toULongLong());
}

void ManagementWidget::onExperimentsSelected()
{
	QList<QListWidgetItem *> selectedItems = _experiment_list->selectedItems();
	std::vector<unsigned long int> selectedExperiments;
	for (QList<QListWidgetItem *>::iterator
		it = selectedItems.begin(),
		end = selectedItems.end();
		it != end; ++it)
	{
		selectedExperiments.push_back(
			const_cast<QListWidgetItem*>(*it)->data(Qt::UserRole).toULongLong());
	}
	emit experimentsSelected(selectedExperiments);
}

//void ManagementWidget::onExperimentCampaignsLoaded(QStringList experimentCampaigns)
void ManagementWidget::onExperimentCampaignsLoaded(std::map<unsigned long int, QString> experimentCampaigns)
{
	_find_experiment_campaign_list->clear();
	for (std::map<unsigned long int, QString>::iterator
		it = experimentCampaigns.begin(),
		end = experimentCampaigns.end();
		it != end; ++it)
	{
		QListWidgetItem *item = new QListWidgetItem(_find_experiment_campaign_list);
		item->setText(it->second);
		item->setData(Qt::UserRole, QVariant::fromValue(it->first));
		_find_experiment_campaign_list->addItem(item);
	}
}

void ManagementWidget::onExperimentCampaignLoaded(std::map<unsigned long int, QString> experiments, QString experimentCampaignName, QString experimentCampaignDescription)
{
	_experiment_list->clear();
	for (std::map<unsigned long int, QString>::iterator
		it = experiments.begin(),
		end = experiments.end();
		it != end; ++it)
	{
		QListWidgetItem *item = new QListWidgetItem(_experiment_list);
		item->setText(it->second);
		item->setData(Qt::UserRole, QVariant::fromValue(it->first));
		_experiment_list->addItem(item);
	}
	_experiment_campaign_name->setText(experimentCampaignName);
	_experiment_campaign_description->setText(experimentCampaignDescription);
}

void ManagementWidget::onMetricsLoaded(MetricTable *selectTable)
{
	_metric_table->setSelectTable(selectTable);
}

void ManagementWidget::modifyExperimentName(bool enabled)
{
	_experiment_campaign_name->setEnabled(enabled);
	_experiment_campaign_description->setEnabled(enabled);
}
