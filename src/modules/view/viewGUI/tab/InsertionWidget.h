#ifndef INSERTIONWIDGET_H
#define INSERTIONWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QSplitter>
#include <QGroupBox>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QPlainTextEdit>
#include <QListWidget>
#include <QTableWidget>
#include <QTableView>
#include <QCheckBox>
#include <QComboBox>

#include "src/modules/model/parsing/ParsingExperimentCampaign.h"

class InsertionWidget : public QWidget
{
	Q_OBJECT

private:
	QGridLayout *_main_layout,					// main layout of the widget
		*_experiment_content_layout,			// layout of experiment
		*_file_zone_layout;						// layout of select file and folder zone
	QVBoxLayout *_files_group_layout,			// layout of file list
		*_content_group_layout;					// layout of file content
	QSplitter *_creation_content_widget,		// splitter between experiments select and experiment contents
		*_files_content_widget;					// splitter between files list and experiment contents
	QGroupBox *_experiment_campaign_group_widget,	// group of experiment infos
		*_files_group_widget,					// group of open files info
		*_content_group_widget;					// group of file content
	QWidget *_file_zone_widget;					// widget of file import options
	QPushButton *_open_file_button,				// button to open files
		*_open_dir_button,						// button to open directories
		*_insert_button,						// button to insert new experiment campaign or extend them
		*_clear_button;							// button to abort insertion and delete all experiment campaign
		//*_parser_options_button;				// button to change parser options
	QComboBox *_parser_config_list;					// list of availables parser config
	QLineEdit *_pattern_text,					// files filter recognized by parser
		*_experiment_campaign_name_text;		// name of the experiment campaign
	QLabel *_pattern_label,
		*_experiment_name_label,
		*_experiment_description_label,
		*_experiment_list_label;
	QCheckBox *_recursive_check_box;			// to know if folder research must be recursive or not
	QPlainTextEdit *_experiment_campaign_description_text;	// description of the experiment campaign
	QListWidget *_experiment_list_items,		// list of experiments for actual experiment campaign
		*_files_list_items;						// list of files for actual experiment
	QTableWidget *_content_table_item;			// content of actual selected file
	std::vector<std::string> _content_table_ids; // ids linked to the rows of thcontent tbale
	bool _isLoadingContentTable;
	/**
	 * @brief buildInterface creates all interface components and assembles them
	 */
	void buildInterface();

	/**
	 * @brief relaySignals link each used signals from QWidgets to InsertionWidget signals in order to simplify signal connections
	 */
	void relaySignals();
	void fillInterface();

public:
	explicit InsertionWidget(QWidget *parent = 0);
	~InsertionWidget();

    void updateExperiments(ParsingExperimentCampaign* campaignExperiment);
	void updateFileList(QList<ParsingExperiment *> * experiments, std::vector<ParsingMetricPattern *> config);
	void updateFiles(QList<ParsingExperiment*> * experiments, std::vector<ParsingMetricPattern *> config);
	void sendExperimentCampaign();
	void configCheckResult(QVariant * configResult);
signals:
	/**
	 * @brief filesToParseOpened trigger when user request to open files
	 */
	void filesToParseOpened();
	/**
	 * @brief dirToParseOpened trigger when user request to open folder to parse
	 */
	void dirToParseOpened();

	/**
	 * @brief experimentCampaignFinded trigger when user want to find experiment campaign
	 * @param name of the experiment campaign
	 */
	void experimentCampaignCleared();
	/**
	 * @brief experimentSelected trigger when user select an experiment
	 * @param
	 */
	void experimentsSelected(QList<QListWidgetItem*>*);
	/**
	 * @brief fileSelected trigger when user select a file
	 * @param current new selected item
	 * @param previous previous selected item
	 */
	void fileSelected(QListWidgetItem*, QListWidgetItem*);
	/**
	 * @brief experimentCampaignInserted trigger when changes on current experiment campaign data must be saved
	 */
	void experimentCampaignInserted(QString*, QString*);
	/**
	 * @brief experimentCampaignAborted trigger when current changes on current experiment campaign data must be aborted
	 */
	void experimentCampaignAborted();

	void metricValueChanged(QString*, QString*, QString*);

	void parserOptionsSelected(unsigned long int);

private slots:
	/**
	 * @brief findExperimentCampaign is called by pressing _experiment_find_button or return key is pressed in _experiment_campaign_name_text
	 */
	void clearExperimentCampaign();
	/**
	 * @brief changeMetricValue trigger when changes on a cell on the content table
	 */
	void changeMetricValue(int row, int cell);
	/**
	 * @brief experimentsSelected trigger when user select an experiment
	 * @param
	 */
	void experimentSelected();
	/**
	 * @brief experimentCampaignInserted trigger when changes on current experiment campaign data must be saved
	 */
	void insertExperimentCampaign();

	void onParserOptionChanged();

public slots:
	void setParserOptions(std::map<unsigned long int, QString>);
};

#endif // INSERTIONWIDGET_H
