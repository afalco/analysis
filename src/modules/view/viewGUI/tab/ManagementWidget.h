#ifndef MANAGEMENTWIDGET_H
#define MANAGEMENTWIDGET_H

#include "src/modules/view/viewGUI/metricTable/MetricTableWidget.h"

#include <QWidget>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QSplitter>
#include <QPushButton>
#include <QLabel>
#include <QGroupBox>
#include <QLineEdit>
#include <QTextEdit>
#include <QListWidget>

class ManagementWidget : public QWidget
{
	Q_OBJECT

private:
	QGridLayout *_main_layout,
		*_manage_experiment_layout;
	QSplitter *_find_data_splitter,
		*_finder_experiment_splitter;
	QPushButton *_import_button;
	QWidget *_data_widget;
	QVBoxLayout *_data_layout;
	QLabel *_data_label,
		*_experiment_name_label,
		*_experiment_description_label,
		*_experiment_list_label;
	MetricTableWidget *_metric_table;
	QGroupBox *_manage_experiment_group;
	QLineEdit *_experiment_campaign_name;
	QTextEdit *_experiment_campaign_description;
	QListWidget *_experiment_list,
		*_find_experiment_campaign_list;

	void buildInterface();
	void bindEvents();
	void fillInterface();
public:
	explicit ManagementWidget(QWidget *parent = 0);
	~ManagementWidget();

signals:
	// user choose an experiment campaign
	void experimentCampaignSelected(unsigned long int);
	// user choose an experiment
	void experimentsSelected(std::vector<unsigned long int>);
	// user update current values in database
	void updateCurrentValues();

private slots:
	// when experiment campaign was selected
	void onExperimentCampaignSelected(QListWidgetItem *);
	// when experiment selection (one or most) has changed
	void onExperimentsSelected();

public slots:
	// load experiments campaign
	void onExperimentCampaignsLoaded(std::map<unsigned long int, QString>);
	// load experiments and experiment campaign description
	// experiments, experimentCampaignName, experimentCampaignDescription
	void onExperimentCampaignLoaded(std::map<unsigned long int, QString>, QString, QString);
	// load metrics
	void onMetricsLoaded(MetricTable *);
	// switch experiment name enabled or disabled (in case of multiple experiments selected)
	void modifyExperimentName(bool);
};

#endif // MANAGEMENTWIDGET_H
