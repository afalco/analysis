#include "src/modules/view/viewGUI/tab/GenerationWidget.h"
#include <QPixmap>
#include <QLabel>

GenerationWidget::GenerationWidget(QWidget *parent) : QWidget(parent)
{
	buildInterface();
	fillInterface();
	relaySignals();
}

GenerationWidget::~GenerationWidget()
{}

void GenerationWidget::buildInterface()
{
	// -- main layout
	_main_layout = new QGridLayout(this);
	setLayout(_main_layout);

	// ---- vertical splitter
	_preview_data_splitter = new QSplitter(Qt::Vertical, this);
	_main_layout->addWidget(_preview_data_splitter, 0, 0);

	// ---- horizontal splitter
	_find_preview_splitter = new QSplitter(Qt::Horizontal, _preview_data_splitter);
	_preview_data_splitter->addWidget(_find_preview_splitter);

	// ------ find data
	_find_widget = new MetricTableWidget(_find_preview_splitter);
	_find_preview_splitter->addWidget(_find_widget);

	// ------ image and options widget
	_image_options_widget = new QWidget(_find_preview_splitter);
	_find_preview_splitter->addWidget(_image_options_widget);
	_image_options_layout = new QGridLayout(_image_options_widget);
	_image_options_widget->setLayout(_image_options_layout);

	// -------- preview image
	_preview_widget = new PreviewWidget(_image_options_widget);
	_image_options_layout->addWidget(_preview_widget, 0, 0, 1, 2);

	// -------- generate file button
	_generate_button_widget = new QPushButton("Generate file", _image_options_widget);
	_image_options_layout->addWidget(_generate_button_widget, 1, 0);

	// -------- graphic type chooser
	_generate_type_widget = new QComboBox(_image_options_widget);
	_image_options_layout->addWidget(_generate_type_widget, 1, 1);

	// ---- data chooser widget & layout
	_data_chooser_widget = new QWidget(_preview_data_splitter);
	_preview_data_splitter->addWidget(_data_chooser_widget);
	_data_chooser_layout = new QHBoxLayout(_data_chooser_widget);
	_data_chooser_widget->setLayout(_data_chooser_layout);

	// ------ ordinate groupbox
	_ordinate_widget = new QGroupBox(QString::fromUtf8("Ordinate"), _data_chooser_widget);
	_data_chooser_layout->addWidget(_ordinate_widget);
	_ordinate_widget_layout = new QVBoxLayout(_ordinate_widget);
	_ordinate_widget->setLayout(_ordinate_widget_layout);

	// -------- ordinate table widget
	_ordinate_table_widget = new MetricTableWidget(_ordinate_widget);
	_ordinate_widget_layout->addWidget(_ordinate_table_widget);

	// ------ abscisse groupbox
	_abscisse_widget = new QGroupBox("Abscissa", _data_chooser_widget);
	_data_chooser_layout->addWidget(_abscisse_widget);
	_abscisse_widget_layout = new QVBoxLayout(_abscisse_widget);
	_abscisse_widget->setLayout(_abscisse_widget_layout);

	// -------- abscisse list chooser
	_abscisse_list_widget = new QListWidget(_abscisse_widget);
	_abscisse_widget_layout->addWidget(_abscisse_list_widget);
}

void GenerationWidget::fillInterface()
{
	_abscisse_list_widget->setSelectionMode(QAbstractItemView::SingleSelection);
	_generate_type_widget->addItem(QString::fromStdString(
		GraphModule::getGraphType(GraphModule::SCATTER_PLOT)),
			QVariant::fromValue(GraphModule::SCATTER_PLOT));
	_generate_type_widget->addItem(QString::fromStdString(
		GraphModule::getGraphType(GraphModule::LINE_GRAPH)),
			QVariant::fromValue(GraphModule::LINE_GRAPH));
	_generate_type_widget->addItem(QString::fromStdString(
		GraphModule::getGraphType(GraphModule::HISTOGRAM)),
			QVariant::fromValue(GraphModule::HISTOGRAM));
}

void GenerationWidget::relaySignals()
{
	connect(_preview_widget, &PreviewWidget::resized, this, &GenerationWidget::previewImageResized);
	connect(_generate_type_widget,
		static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
		this, &GenerationWidget::onGraphTypeChanged);
	connect(_generate_button_widget, &QPushButton::released, this, &GenerationWidget::graphFileGenerationRequired);
	connect(_abscisse_list_widget, &QListWidget::itemSelectionChanged, this, &GenerationWidget::onAbscissaMetricChanged);
}

// -------- SLOTS --------
void GenerationWidget::onGraphTypeChanged(int index)
{
	if (index != -1)
		emit graphTypeChanged(
			_generate_type_widget->itemData(index).
				value<GraphModule::GraphType>());
}

void GenerationWidget::setPreviewImage(QPixmap *image)
{
	_preview_widget->setPixmap(QPixmap(*image));
	delete image;
}

void GenerationWidget::setFindSelectorValues(MetricTable *selectTable)
{
	_find_widget->setSelectTable(selectTable);
}

void GenerationWidget::setOrdinateSelectorValues(MetricTable *selectTable)
{
	_ordinate_table_widget->setSelectTable(selectTable);
}

void GenerationWidget::onAbscissaMetricChanged()
{
	emit selectesAbscissaMetric(
		_abscisse_list_widget->selectedItems()
			.first()->data(Qt::UserRole).toULongLong());
}

void GenerationWidget::setAbscissaSelectorValues(QStringList values)
{
	_abscisse_list_widget->clear();
	_abscisse_list_widget->addItems(values);
	//_abscisse_list_widget->blockSignals(true);
	/*for (std::map<unsigned long int, QString>
		it = values.begin(),
		end = values.end();
		it != end; it++)
	{
		std::pair<unsigned long int, QString> value =
			static_cast<std::pair<unsigned long int, QString> >(*it);
		QListWidgetItem *item = new QListWidgetItem(value.second);
		item->setData(Qt::UserRole,
			QVariant::fromValue<unsigned long int>(value.first));
		_abscisse_list_widget->addItem(item);
	}*/
	//_abscisse_list_widget->blockSignals(false);
}
