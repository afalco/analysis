#include "src/modules/view/viewGUI/tab/ConfigFileWidget.h"

ConfigFileWidget::ConfigFileWidget(QWidget *parent):
	QWidget(parent)
{
	buildInterface();
	relaySignals();
}

void ConfigFileWidget::buildInterface()
{
	// - main layout
	_main_layout = new QGridLayout(this);
	setLayout(_main_layout);
	_main_layout->setColumnStretch(0, 1);
	_main_layout->setColumnStretch(1, 3);

	// -- parsing button
	_parsing_button = new QPushButton("Open and parse file", this);
	_main_layout->addWidget(_parsing_button, 0, 0);

	// -- file name
	_file_name_label = new QLabel("Config file name", this);
	//_file_name_label->setSizePolicy(QSizePolicy::Line);
	_main_layout->addWidget(_file_name_label, 1, 0);
	_file_name = new QLineEdit(this);
	_main_layout->addWidget(_file_name, 2, 0);

	// -- import button
	_save_button = new QPushButton("Save configuration", this);
	_main_layout->addWidget(_save_button, 3, 0);

	// -- file metrics
	_file_metrics_table = new QTableWidget(this);
	_main_layout->addWidget(_file_metrics_table, 0, 1, 5, 1);
	_file_metrics_table->setColumnCount(1);
	_file_metrics_table->horizontalHeader()->setStretchLastSection(true);
}

void ConfigFileWidget::relaySignals()
{
	connect(_parsing_button, &QPushButton::released, this, &ConfigFileWidget::parseConfigFile);
	connect(_save_button, &QPushButton::released, this, &ConfigFileWidget::onSaveMetrics);
}

// -------- SLOTS --------
void ConfigFileWidget::onSaveMetrics()
{
	QString name = _file_name->text();
	name = name.trimmed();
	if (name.isEmpty())
		QMessageBox::critical(
			this,
			"No config file name choosed",
			"Please choose a file name.");
	else
		emit saveMetrics(name);
}

void ConfigFileWidget::setConfigFilePatterns(QStringList patterns)
{
	_file_metrics_table->clear();
	_file_metrics_table->setRowCount(patterns.count());
	for (int i = 0; i < patterns.size(); ++i)
	{
		_file_metrics_table->setItem(
			i, 0, new QTableWidgetItem(patterns.at(i)));
	}
}
