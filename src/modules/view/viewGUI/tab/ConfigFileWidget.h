#ifndef CONFIGFILEWIDGET_H
#define CONFIGFILEWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QTableWidget>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QHeaderView>

class ConfigFileWidget : public QWidget
{
	Q_OBJECT

private:
	QGridLayout *_main_layout;
	QPushButton *_parsing_button,
		*_save_button;
	QTableWidget *_file_metrics_table;
	QLabel *_file_name_label;
	QLineEdit *_file_name;

	void buildInterface();
	void relaySignals();

public:
	explicit ConfigFileWidget(QWidget *parent = 0);

signals:
	void parseConfigFile();
	void saveMetrics(QString);

private slots:
	void onSaveMetrics();

public slots:
	void setConfigFilePatterns(QStringList);
};

#endif // CONFIGFILEWIDGET_H
