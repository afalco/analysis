#ifndef GENERATIONWIDGET_H
#define GENERATIONWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QSplitter>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListWidget>
#include <QPushButton>
#include <QComboBox>

#include "src/modules/view/viewGUI/PreviewWidget.h"
#include "src/modules/view/viewGUI/metricTable/MetricTableWidget.h"

#include "src/modules/model/GraphModule.h"

class GenerationWidget : public QWidget
{
	Q_OBJECT

private:
	QGridLayout *_main_layout,
		*_image_options_layout;
	QSplitter *_find_preview_splitter,
		*_preview_data_splitter;
	MetricTableWidget *_find_widget,
		*_ordinate_table_widget;
	QGroupBox *_abscisse_widget,
		*_ordinate_widget;
	QVBoxLayout *_abscisse_widget_layout,
		*_ordinate_widget_layout;
	QWidget *_data_chooser_widget,
		*_image_options_widget;
	QHBoxLayout *_data_chooser_layout;
	QListWidget *_abscisse_list_widget;
	QPushButton *_generate_button_widget;
	QComboBox *_generate_type_widget;
	PreviewWidget *_preview_widget;

	void buildInterface();
	void fillInterface();
	/**
	 * @brief relaySignals link each used signals from QWidgets to GenerationWidget signals in order to simplify signal connections
	 */
	void relaySignals();

public:
	explicit GenerationWidget(QWidget *parent = 0);
	~GenerationWidget();

signals:
	/**
	 * @brief previewImageResized
	 * throws when preview widget is resized
	 * @param size the new size of the preview widget after resizing
	 */
	void previewImageResized(QSize *);

	void graphTypeChanged(GraphModule::GraphType);
	void graphFileGenerationRequired();
	void selectesAbscissaMetric(unsigned long int);

private slots:
	void onGraphTypeChanged(int);
	void onAbscissaMetricChanged();

public slots:
	/**
	 * @brief setPreviewImage
	 * Change the preview image
	 * @param image the new image
	 */
	void setPreviewImage(QPixmap *);

	/**
	 * @brief setFindSelectorValues
	 * Change the content of _find_widget to add new SelectTable values
	 * @param selectTables the new select table to put in widget
	 */
	void setFindSelectorValues(MetricTable *);

	/**
	 * @brief setOrdinateSelectorValues
	 * Change the content of _ordinate_table_widget to add new SelectTable values
	 * @param selectTables the new select table to put in widget
	 */
	void setOrdinateSelectorValues(MetricTable *);

	void setAbscissaSelectorValues(QStringList);
};

#endif // GENERATIONWIDGET_H
