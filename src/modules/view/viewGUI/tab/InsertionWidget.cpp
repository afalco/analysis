#include "src/modules/view/viewGUI/tab/InsertionWidget.h"
#include <QFileDialog>
#include <QStandardItemModel>
#include <iostream>
#include <QMessageBox>

InsertionWidget::InsertionWidget(QWidget *parent) : QWidget(parent)
{
	buildInterface();
	relaySignals();
	fillInterface();
	_isLoadingContentTable = false;
}

InsertionWidget::~InsertionWidget()
{}

void InsertionWidget::buildInterface()
{
	// - main layout
	_main_layout = new QGridLayout(this);
	setLayout(_main_layout);

	// -- creation-content widget
	_creation_content_widget = new QSplitter(Qt::Horizontal, this);
	_main_layout->addWidget(_creation_content_widget, 0, 0, 1, 3);

	// ---- creation label/group
	_experiment_campaign_group_widget = new QGroupBox("Experiment campaign creation", _creation_content_widget);
	_creation_content_widget->addWidget(_experiment_campaign_group_widget);
	_experiment_content_layout = new QGridLayout(_experiment_campaign_group_widget);
	_experiment_campaign_group_widget->setLayout(_experiment_content_layout);

	// ------ find experiment
	_experiment_name_label = new QLabel("Name", _experiment_campaign_group_widget);
	_experiment_content_layout->addWidget(_experiment_name_label, 0, 0);
	_experiment_campaign_name_text = new QLineEdit(_experiment_campaign_group_widget);
	_experiment_content_layout->addWidget(_experiment_campaign_name_text, 0, 1);

	// ------ experiment description
	_experiment_description_label = new QLabel("Description", _experiment_campaign_group_widget);
	_experiment_content_layout->addWidget(_experiment_description_label, 1, 0, 1, 2);
	_experiment_campaign_description_text = new QPlainTextEdit(_experiment_campaign_group_widget);
	_experiment_content_layout->addWidget(_experiment_campaign_description_text, 2, 0, 1, 2);

	// ------ experiences list
	_experiment_list_label = new QLabel("Experiment(s)", _experiment_campaign_group_widget);
	_experiment_content_layout->addWidget(_experiment_list_label, 3, 0, 1, 2);
	_experiment_list_items = new QListWidget(_experiment_campaign_group_widget);
	_experiment_content_layout->addWidget(_experiment_list_items, 4, 0, 1, 2);
	_experiment_list_items->setSelectionMode(QAbstractItemView::ExtendedSelection);

	// ---- file content widget
	_file_zone_widget = new QWidget(_creation_content_widget);
	_creation_content_widget->addWidget(_file_zone_widget);
	_file_zone_layout = new QGridLayout(_file_zone_widget);
	_file_zone_widget->setLayout(_file_zone_layout);

	// ------ load file
	_open_file_button = new QPushButton("File", _file_zone_widget);
	_file_zone_layout->addWidget(_open_file_button, 0, 0);

	// ------ load directory
	_open_dir_button = new QPushButton("Folder", _file_zone_widget);
	_file_zone_layout->addWidget(_open_dir_button, 1, 0);

	// ------ pattern
	_pattern_text = new QLineEdit(_file_zone_widget);
	_file_zone_layout->addWidget(_pattern_text, 0, 2, 1, 2);
	_pattern_label = new QLabel("Pattern :", _file_zone_widget);
	_file_zone_layout->addWidget(_pattern_label, 0, 1);

	// ------ recursive
	_recursive_check_box = new QCheckBox("Recursive", _file_zone_widget);
	_file_zone_layout->addWidget(_recursive_check_box, 1, 1);

	// ------ parser options
	/*_parser_options_button = new QPushButton("Parser options", _file_zone_widget);
	_file_zone_layout->addWidget(_parser_options_button, 1, 2);*/
	_parser_config_list = new QComboBox(_file_zone_widget);
	_file_zone_layout->addWidget(_parser_config_list, 1, 2);

	// ------ files-content widget
	_files_content_widget = new QSplitter(Qt::Horizontal, _file_zone_widget);
	_file_zone_layout->addWidget(_files_content_widget, 2, 0, 1, 4);

	// -------- files
	_files_group_widget = new QGroupBox("Files", _files_content_widget);
	_files_content_widget->addWidget(_files_group_widget);
	_files_group_layout = new QVBoxLayout(_files_group_widget);
	_files_group_widget->setLayout(_files_group_layout);
	_files_list_items = new QListWidget(_files_group_widget);
	_files_group_layout->addWidget(_files_list_items);

	// -------- content
	_content_group_widget = new QGroupBox("Content", _files_content_widget);
	_files_content_widget->addWidget(_content_group_widget);
	_content_group_layout = new QVBoxLayout(_content_group_widget);
	_content_group_widget->setLayout(_content_group_layout);
	_content_table_item = new QTableWidget(_content_group_widget);
	_content_group_layout->addWidget(_content_table_item);

	// -- insert and abort buttons
	_insert_button = new QPushButton("Insert", this);
	_main_layout->addWidget(_insert_button, 1, 0);
	_clear_button = new QPushButton("Clear", this);
	_main_layout->addWidget(_clear_button, 1, 1);

}

void InsertionWidget::relaySignals()
{
	connect(_open_file_button, &QPushButton::released, this, &InsertionWidget::filesToParseOpened);
	connect(_open_dir_button, &QPushButton::released, this, &InsertionWidget::dirToParseOpened); // tmp
	connect(_experiment_list_items, &QListWidget::itemSelectionChanged, this, &InsertionWidget::experimentSelected);
	connect(_files_list_items, &QListWidget::currentItemChanged, this, &InsertionWidget::fileSelected);
	connect(_insert_button, &QPushButton::released, this, &InsertionWidget::insertExperimentCampaign);
	connect(_clear_button, &QPushButton::released, this, &InsertionWidget::experimentCampaignAborted);
	connect(_clear_button, &QPushButton::released, this, &InsertionWidget::clearExperimentCampaign);
	connect(_experiment_campaign_name_text, &QLineEdit::returnPressed, this, &InsertionWidget::clearExperimentCampaign);
	connect(_content_table_item, &QTableWidget::cellChanged, this, &InsertionWidget::changeMetricValue);
	connect(_parser_config_list, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &InsertionWidget::onParserOptionChanged);
}

void InsertionWidget::updateExperiments(ParsingExperimentCampaign* campaignExperiment)
{
    //Maybe factorize it in the class.

    _experiment_list_items->clear();
    //Update the Experiments List
    std::vector<ParsingExperiment *> experiments = campaignExperiment->getExperiments();
    QStringList * experimentsName = new QStringList();
    for (std::vector<ParsingExperiment *>::iterator it = experiments.begin(); it != experiments.end(); ++it)
    {
        experimentsName->push_back(QString((*it)->getName().c_str()));
    }
    _experiment_list_items->addItems(*experimentsName);

    //Fetch the selected experiment for the linked files
    int selectedIndex;
    if (_experiment_list_items->selectedItems().isEmpty())
    {
        _experiment_list_items->setCurrentIndex(_experiment_list_items->rootIndex());
        selectedIndex = 0;
    }
    else
    {
        selectedIndex = _experiment_list_items->selectionModel()->selectedIndexes().front().row();
    }
	QList<ParsingExperiment *> * newList = new QList<ParsingExperiment*>();
	newList->push_back(experiments.at(selectedIndex));
    std::vector<ParsingMetricPattern *> config = campaignExperiment->getParsingConfig()->getConfig();
	updateFileList(newList, config);

}

void InsertionWidget::updateFileList(QList<ParsingExperiment *> * experiments, std::vector<ParsingMetricPattern *> config)
{
	//Update the Files List
	_isLoadingContentTable = true;
	_files_list_items->clear();
	//Update the files list
	if (experiments->size() == 1)
	{
		QStringList * filesName = new QStringList();
		std::map<std::string, std::vector<ParsingMetric*> > outputFiles = experiments->first()->getOutputFiles();
		for (std::map<std::string, std::vector<ParsingMetric *>>::iterator it = outputFiles.begin(); it != outputFiles.end(); ++it)
		{
			filesName->push_back(QString(it->first.c_str()));
		}
		_files_list_items->addItems(*filesName);
	}
	//Update the Content table

	_content_table_item->clear();
	_content_table_item->setRowCount(0);
	_content_table_item->setColumnCount(3);
	_content_table_item->setHorizontalHeaderItem(0,  new QTableWidgetItem(QString("Variable")));
	_content_table_item->setHorizontalHeaderItem(1,  new QTableWidgetItem(QString("Value")));
	_content_table_item->setHorizontalHeaderItem(2,  new QTableWidgetItem(QString("Type")));
	_content_table_ids.clear();
	if (!experiments->isEmpty())
	{
		for (std::vector<ParsingMetricPattern *>::iterator it = config.begin(); it != config.end(); ++it)
		{
			int row = 0;
			_content_table_ids.push_back((*it)->getId());
			_content_table_item->insertRow(row);
			QTableWidgetItem * variable;
			QTableWidgetItem * value;
			QTableWidgetItem * type;
			bool isParameter = (*it)->isParameter();
			QString variableString = QString((*it)->getName().c_str());
			QString valueString;
			if ((*it)->getDefault() == "")
				valueString = QString("???");
			else
				valueString = QString((*it)->getDefault().c_str());
			QString typeString = QString((*it)->getType().c_str());
			//Too complex, must be optimized
			bool found = false;
			std::string valueFound = "";
			QList<ParsingExperiment *>::iterator experimentIterator;
			for (experimentIterator = experiments->begin(); experimentIterator != experiments->end(); ++ experimentIterator)
			{

				std::map<std::string, std::vector<ParsingMetric*> > outputFiles = (*experimentIterator)->getOutputFiles();
				std::vector<ParsingMetric *> fileRows = outputFiles.begin()->second;
				for (std::vector<ParsingMetric *>::iterator it2 = fileRows.begin(); it2 != fileRows.end(); ++it2)
				{
					if ((*it2)->getPattern()->getId() == (*it)->getId())
					{
						if (found != true)
						{
							found = true;
							valueString = QString((*it2)->getValue().c_str());
							valueFound = (*it2)->getValue();
						}
						else if (valueFound == (*it2)->getValue())
						{
							valueString = QString((*it2)->getValue().c_str());
						}
						else
						{
							valueString = QString("<<=different=>>");
						}
					}
				}
			}
			variable =  new QTableWidgetItem(variableString);
			variable->setFlags(variable->flags() ^ Qt::ItemIsEditable);
			value = new QTableWidgetItem(valueString);
			type = new QTableWidgetItem(typeString);
			type->setFlags(type->flags() ^ Qt::ItemIsEditable);
			if (!found)
			{
				if (valueString != QString("???"))
				{
					variable->setBackground(QBrush(QColor(255, 255, 0)));
					value->setBackground(QBrush(QColor(255, 255, 0)));
					type->setBackground(QBrush(QColor(255, 255, 0)));
				}
				else
				{
					variable->setBackground(QBrush(QColor(255, 0, 0)));
					value->setBackground(QBrush(QColor(255, 0, 0)));
					type->setBackground(QBrush(QColor(255, 0, 0)));
				}
			}
			//Distinction between parameters and results. Bold for the moment.
			if (isParameter)
			{
				QFont font = QFont();
				font.setBold(true);
				variable->setFont(font);
				value->setFont(font);
				type->setFont(font);
			}
			_content_table_item->setItem(row, 0, variable);
			_content_table_item->setItem(row, 1, value);
			_content_table_item->setItem(row, 2, type);
			row++;
		}
	}
	_isLoadingContentTable = false;
}

void InsertionWidget::updateFiles(QList<ParsingExperiment *> * experiments, std::vector<ParsingMetricPattern *> config)
{
	updateFileList(experiments, config);
	delete experiments;
}

void InsertionWidget::fillInterface()
{
	// actually unimplemented functionalities
	_recursive_check_box->setEnabled(false);
	_pattern_label->setEnabled(false);
	_pattern_text->setEnabled(false);
}

// -------- SLOTS --------
void InsertionWidget::clearExperimentCampaign()
{
	_experiment_list_items->clear();
	_files_list_items->clear();
	_content_table_item->clear();
	emit experimentCampaignCleared();
}

void InsertionWidget::changeMetricValue(int row, int cell)
{
	if (!_isLoadingContentTable)
	{
		if (cell == 1)
		{
			QString * id = new QString(_content_table_ids.at(row).c_str());
			QString * value = new QString(_content_table_item->item(row, cell)->text().toStdString().c_str());
			_content_table_item->item(row, 0)->setBackground(QBrush(QColor(255, 255, 255)));
			_content_table_item->item(row, 1)->setBackground(QBrush(QColor(255, 255, 255)));
			_content_table_item->item(row, 2)->setBackground(QBrush(QColor(255, 255, 255)));
			QString * fileName;
			if (!_files_list_items->selectedItems().isEmpty())
			{
				fileName = new QString(_files_list_items->selectedItems().first()->text().toStdString().c_str());
			}
			else if (_files_list_items->count() > 0)
			{
				fileName = new QString(_files_list_items->item(0)->text().toStdString().c_str());
			}
			else
			{
				fileName =new QString("");
			}
			emit metricValueChanged(id, value, fileName);
		}
	}
}

void InsertionWidget::experimentSelected()
{
	QList<QListWidgetItem*> * newList = new QList<QListWidgetItem*>();
	QList<QListWidgetItem*> oldList = _experiment_list_items->selectedItems();


	QList<QListWidgetItem*>::iterator i;
	for (i = oldList.begin(); i != oldList.end(); ++i)
	{
		newList->push_back(*i);
	}
	emit experimentsSelected(newList);
}

void InsertionWidget::sendExperimentCampaign()
{
	QString * experimentCampaignName = new QString(_experiment_campaign_name_text->text().toStdString().c_str());
	QString * experimentCampaignDescription = new QString(_experiment_campaign_description_text->toPlainText().toStdString().c_str());
	emit experimentCampaignInserted(experimentCampaignName, experimentCampaignDescription);
}

void InsertionWidget::insertExperimentCampaign()
{
	if (_experiment_campaign_name_text->text().toStdString() != "")
	{
		if (_content_table_item->findItems(QString("???"), Qt::MatchExactly).size() == 0)
		{
			this->sendExperimentCampaign();
		}
		else
		{
			QMessageBox missingPatternAlert;
			missingPatternAlert.setText("Warning, some patterns don't have matching values.");
			missingPatternAlert.setInformativeText("Do you want to insert it anyway ?");
			missingPatternAlert.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
			missingPatternAlert.setDefaultButton(QMessageBox::Yes);
			int ret = missingPatternAlert.exec();
			if (ret == QMessageBox::Yes)
			{
				this->sendExperimentCampaign();
			}
		}
	}
	else
	{
		QMessageBox campaignNameAlert;
		campaignNameAlert.setText("Please, give a name to the experiment campaign to insert it.");
		campaignNameAlert.exec();
	}
}

void InsertionWidget::setParserOptions(std::map<unsigned long int, QString> options)
{
	_parser_config_list->clear();
	for (std::map<unsigned long int, QString>::const_iterator
		it = options.begin(),
		end = options.end();
		it != end; ++it)
	{
		const std::pair<unsigned long int, QString> option
			= static_cast<std::pair<unsigned long int, QString> >(*it);
		_parser_config_list->addItem(
			option.second,
			QVariant::fromValue<unsigned long long>(option.first));
	}
}

void InsertionWidget::onParserOptionChanged()
{
	emit parserOptionsSelected(
		_parser_config_list->currentData().toULongLong());
}
