#include "src/modules/view/viewGUI/PreviewWidget.h"

PreviewWidget::PreviewWidget(QWidget *parent) : QWidget(parent)
{
	buildInterface();
	fillInterface();
}

void PreviewWidget::buildInterface()
{
	_alphaBackground = QPixmap(20, 20);
	QPainter alphaFillPainter(&_alphaBackground);
	alphaFillPainter.fillRect(0, 0, 10, 10, Qt::lightGray);
	alphaFillPainter.fillRect(10, 10, 10, 10, Qt::lightGray);
	alphaFillPainter.fillRect(0, 10, 10, 10, Qt::darkGray);
	alphaFillPainter.fillRect(10, 0, 10, 10, Qt::darkGray);
	alphaFillPainter.end();
	_backgroundBrush = QBrush(_alphaBackground);
}

void PreviewWidget::fillInterface()
{
	/*QImage image("test.png");
	_pixmap = QPixmap::fromImage(image);*/
}

void PreviewWidget::paintEvent(QPaintEvent *event)
{
	QWidget::paintEvent(event);

	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);

	painter.setBrush(_backgroundBrush);
	painter.drawRect(event->rect());
	if (_pixmap.isNull())
	{
		painter.drawText(
			event->rect(),
			QString("No graphic loaded")
		);
	}
	else
	{
		QSize pixSize = _pixmap.size();
		pixSize.scale(event->rect().size(), Qt::KeepAspectRatio);

		QPixmap scaledPix = _pixmap.scaled(
			pixSize,
			Qt::KeepAspectRatio,
			Qt::SmoothTransformation
		);

		painter.drawPixmap(
			QPoint(
				(event->rect().size().width() - pixSize.width()) / 2,
				(event->rect().size().height() - pixSize.height()) / 2),
			scaledPix
		);
	}
}

void PreviewWidget::resizeEvent(QResizeEvent *event)
{
	emit resized(new QSize(event->size()));
}

// -------- SLOTS --------
void PreviewWidget::setPixmap(QPixmap pixmap)
{
	_pixmap = pixmap;
}
