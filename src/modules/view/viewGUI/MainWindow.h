#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QTabWidget>
#include <QLabel>
#include <QMenuBar>
#include <QMenu>
#include <QAction>

#include "src/modules/view/viewGUI/tab/HomepageWidget.h"
#include "src/modules/view/viewGUI/tab/InsertionWidget.h"
#include "src/modules/view/viewGUI/tab/GenerationWidget.h"
#include "src/modules/view/viewGUI/tab/ManagementWidget.h"
#include "src/modules/view/viewGUI/tab/ConfigFileWidget.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT

private:
	QWidget *_central_widget;
	QVBoxLayout *_central_widget_layout;
	QTabWidget *_tabs_widget;
	QMenuBar *_menu_bar;
	QMenu *_file_menu,
		*_output_menu;
	QAction *_file_create_database_action,
		*_file_open_database_action,
		*_file_import_option_file_action,
		*_output_generate_graph_action,
		*_help_action;

	HomepageWidget *_homepage_widget;
	InsertionWidget *_insertion_widget;
	GenerationWidget *_generation_widget;
	ManagementWidget *_management_widget;
	ConfigFileWidget *_config_file_widget;

	int _homepageWdgetPos,
		_insertionWidgetPos,
		_generationWidgetPos,
		_managementWidgetPos,
		_configFileWidgetPos;

	/**
	 * @brief buildInterface creates all interface components and assembles them
	 */
	void buildInterface();
	/**
	 * @brief relaySignals link each used signals from QWidgets to InsertionWidget signals in order to simplify signal connections
	 */
	void relaySignals();

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

	/**
	 * @brief getHomepageWidget
	 * Get the homepage widget
	 * @return a const reference to the HomepageWidget
	 */
	HomepageWidget *getHomepageWidget() const;

	/**
	 * @brief getInsertionWidget
	 * Get the insertion widget
	 * @return a const reference to the InsertionWidget
	 */
	InsertionWidget *getInsertionWidget() const;

	/**
	 * @brief getGenerationWidget
	 * Get the generation widget
	 * @return a const reference to the GenerationWidget
	 */
	GenerationWidget *getGenerationWidget() const;

	/**
	 * @brief getManagementWidget
	 * Get the management widget
	 * @return a const reference to the ManagementWidget
	 */
	ManagementWidget *getManagementWidget() const;

	ConfigFileWidget *getConfigFileWidget() const;

signals:
	void databaseCreated();
	void databaseOpened();
	/**
	 * @brief insertionTabViewed
	 * Event called when user go on InsertionWidget tab
	 */
	void insertionTabViewed();

	/**
	 * @brief generationTabViewed
	 * Event called when user go on GenerationWidget tab
	 */
	void generationTabViewed();

	/**
	 * @brief managementTabViewed
	 * Event called when user go on ManagementWidget tab
	 */
	void managementTabViewed();

private slots:
	/**
	 * @brief tabSelected
	 * Slot connected to QTabWidget::currentChanged()
	 */
	void onTabSelect(int);

	void onSwitchToImport();
	void onSwitchToGraph();
	void onSwitchToGestion();
	void onSwitchToConfigFile();
};

#endif // MAINWINDOW_H
