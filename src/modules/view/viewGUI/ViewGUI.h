#ifndef VIEWGUI_H
#define VIEWGUI_H

#include "src/modules/view/View.h"
#include "src/modules/view/viewGUI/MainWindow.h"
#include "src/modules/model/parsing/ParsingExperimentCampaign.h"

#include <QStringList>
#include <QFileDialog>

class ViewGUI : public View {
	Q_OBJECT

private:
	MainWindow *_main_window;
	QString _databaseFilesPattern,
		_graphicGenerationFilesPattern;

public:
	explicit ViewGUI();

	void bindEvents();
	void show();

private slots:
	void createDatabase();
	void openDatabase();
	void openFilesToParse();
    void openDirToParse();
	void clearExperimentCampaign();
	void configFileOpenFile();

public slots:
	void setPreviewImage(QPixmap *);
    void setExperiments(ParsingExperimentCampaign *);
	void setFiles(QList<ParsingExperiment *>*, ParsingConfig *);
	void configCheckResult(QVariant *, QVariant *);
	void generationGraphFileGenerationRequired();

signals:
	void databaseCreated(QString*);
	void databaseOpened(QString*);
	void filesParsed(QStringList*);
    void dirParsed(QStringList*);
    void configParsed(QStringList*);
	void configChecked(QVariant *);
	void experimentCampaignCleared();
	void experimentCampaignInserted(QString*, QString*);
	void experimentsSelected(QList<QListWidgetItem*>*);
	void metricValueChanged(QString*, QString*, QString*);
	/**
	 * @brief previewImageResized
	 * throws when preview widget is resized
	 * @param size the new size of the preview widget after resizing
	 */
	void previewImageResized(QSize *);

	/**
	 * @brief findSelectorValuesSetted
	 * Connected to GenerationWidget::setFindSelectorValues
	 */
	void findSelectorValuesSetted(MetricTable *);

	/**
	 * @brief ordinateSelectionValuesSetted
	 * Connected to GenerationWidget::setOrdinateSelectorValues
	 */
	void ordinateSelectionValuesSetted(MetricTable *);

	/**
	 * @brief generationTabViewed
	 * Connected to MainWindow::generationTabViewed
	 */
	void insertionTabViewed();
	void generationTabViewed();
	void managementTabViewed();

	void setInsertionWidgetParserOptions(std::map<unsigned long int, QString>);
	void insertionWidgetParserOptionSelected(unsigned long int);

	void managementExperimentCampaignSelected(unsigned long int);
	void managementExperimentsSelected(std::vector<unsigned long int>);
	void managementUpdateCurrentValues();

	void onManagementExperimentCampaignsLoaded(std::map<unsigned long int, QString>);
	void onManagementExperimentCampaignLoaded(std::map<unsigned long int, QString>, QString, QString);
	void onManagementMetricsLoaded(MetricTable *);
	void managementModifyExperimentName(bool);

	void generationGraphTypeChanged(GraphModule::GraphType);
	void generationGraphFileGenerated(QString);
	void generationSelectAbscissaMetric(unsigned long int);
	void setGenerationAbscissaSelectorValues(QStringList);

	void configFileParsedFile(QString);
	void configFileSave(QString);
	void setConfigFilePatterns(QStringList);
};

#endif // VIEWGUI_H
