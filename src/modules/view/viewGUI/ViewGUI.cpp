#include "src/modules/view/viewGUI/ViewGUI.h"
#include <QMessageBox>
#include <QImageWriter>
#include <iostream>

ViewGUI::ViewGUI():
	View(),
	_databaseFilesPattern("Database files (*.db *.sqlite);;All files (*)"),
	_graphicGenerationFilesPattern()
{
	_main_window = new MainWindow;

	QList<QByteArray> formats = QImageWriter::supportedImageFormats();
	for (QList<QByteArray>::iterator
		it = formats.begin(),
		end = formats.end();
		it != end; ++it)
		*it = " .*" + *it;
	_graphicGenerationFilesPattern =
		"R (*.R);;Image (" + formats.join(' ') + ")";

}

void ViewGUI::show()
{
	_main_window->show();
}
void ViewGUI::bindEvents()
{
	// connect all MainWindow events
	connect(_main_window, &MainWindow::databaseCreated, this, &ViewGUI::createDatabase);
	connect(_main_window, &MainWindow::databaseOpened, this, &ViewGUI::openDatabase);
	connect(_main_window, &MainWindow::insertionTabViewed, this, &ViewGUI::insertionTabViewed);
	connect(_main_window, &MainWindow::generationTabViewed, this, &ViewGUI::generationTabViewed);
	connect(_main_window, &MainWindow::managementTabViewed, this, &ViewGUI::managementTabViewed);

	// getting inserion widget for connecting all events
	InsertionWidget *insertWidget = _main_window->getInsertionWidget();
	connect(insertWidget, &InsertionWidget::filesToParseOpened, this, &ViewGUI::openFilesToParse);
    connect(insertWidget, &InsertionWidget::dirToParseOpened, this, &ViewGUI::openDirToParse);
	connect(insertWidget, &InsertionWidget::experimentsSelected, this, &ViewGUI::experimentsSelected);
	connect(insertWidget, &InsertionWidget::experimentCampaignCleared, this, &ViewGUI::experimentCampaignCleared); connect(insertWidget, &InsertionWidget::experimentCampaignInserted, this, &ViewGUI::experimentCampaignInserted);
	connect(insertWidget, &InsertionWidget::metricValueChanged, this, &ViewGUI::metricValueChanged);
	connect(insertWidget, &InsertionWidget::parserOptionsSelected, this, &ViewGUI::insertionWidgetParserOptionSelected);
	connect(this, &ViewGUI::setInsertionWidgetParserOptions, insertWidget, &InsertionWidget::setParserOptions);

	// getting generation widget for connecting all events
	GenerationWidget *generationWidget = _main_window->getGenerationWidget();
	connect(generationWidget, &GenerationWidget::previewImageResized, this, &ViewGUI::previewImageResized);
	connect(this, &ViewGUI::findSelectorValuesSetted, generationWidget, &GenerationWidget::setFindSelectorValues);
	connect(this, &ViewGUI::ordinateSelectionValuesSetted, generationWidget, &GenerationWidget::setOrdinateSelectorValues);
	connect(generationWidget, &GenerationWidget::graphTypeChanged, this, &ViewGUI::generationGraphTypeChanged);
	connect(generationWidget, &GenerationWidget::graphFileGenerationRequired, this, &ViewGUI::generationGraphFileGenerationRequired);
	connect(generationWidget, &GenerationWidget::selectesAbscissaMetric, this, &ViewGUI::generationSelectAbscissaMetric);
	connect(this, &ViewGUI::setGenerationAbscissaSelectorValues, generationWidget, &GenerationWidget::setAbscissaSelectorValues);

	// getting management widget for connecting all events
	ManagementWidget *managementWidget = _main_window->getManagementWidget();
	connect(managementWidget, &ManagementWidget::experimentCampaignSelected, this, &ViewGUI::managementExperimentCampaignSelected);
	connect(managementWidget, &ManagementWidget::experimentsSelected, this, &ViewGUI::managementExperimentsSelected);
	connect(managementWidget, &ManagementWidget::updateCurrentValues, this, &ViewGUI::managementUpdateCurrentValues);
	connect(this, &ViewGUI::onManagementExperimentCampaignLoaded, managementWidget, &ManagementWidget::onExperimentCampaignLoaded);
	connect(this, &ViewGUI::onManagementExperimentCampaignsLoaded, managementWidget, &ManagementWidget::onExperimentCampaignsLoaded);
	connect(this, &ViewGUI::onManagementMetricsLoaded, managementWidget, &ManagementWidget::onMetricsLoaded);
	connect(this, &ViewGUI::managementModifyExperimentName, managementWidget, &ManagementWidget::modifyExperimentName);

	// getting config file widget for connecting all events
	ConfigFileWidget *configFileWidget = _main_window->getConfigFileWidget();
	connect(configFileWidget, &ConfigFileWidget::parseConfigFile, this, &ViewGUI::configFileOpenFile);
	connect(configFileWidget, &ConfigFileWidget::saveMetrics, this, &ViewGUI::configFileSave);
	connect(this, &ViewGUI::setConfigFilePatterns, configFileWidget, &ConfigFileWidget::setConfigFilePatterns);
}

// -------- SLOTS --------
void ViewGUI::createDatabase()
{
	// to complete
	QString *file = new QString(QFileDialog::getSaveFileName(
		_main_window,
		"Create a new database file",
		".",
		_databaseFilesPattern
	));
	if (!file->isEmpty())
		emit databaseCreated(file);
}

void ViewGUI::openDatabase()
{
	QString *file = new QString(QFileDialog::getOpenFileName(
		_main_window,
		"Select the database file to work with",
		".",
		_databaseFilesPattern
	));
	if (!file->isEmpty())
		emit databaseOpened(file);
}
void ViewGUI::configCheckResult(QVariant * isFolder, QVariant * result)
{
	if (result->toBool())
	{

		if (isFolder->toBool())
		{
			QStringList *files = new QStringList(QFileDialog::getExistingDirectory(
				_main_window,
				"Select one or more files to parse",
				"../exemple/"
			));
			if (files->size() > 0) // if have files to parse
				emit dirParsed(files); // call parse event
		}
		else
		{
			QStringList *files = new QStringList(QFileDialog::getOpenFileNames(
				_main_window,
				"Select one or more files to parse",
				"../exemple/"
			));
			if (files->size() > 0) // if have files to parse
				emit filesParsed(files); // call parse event
		}
	}
	else
	{
		QMessageBox configAlert;
		configAlert.setText("Please, select a config before Parsing.");
		configAlert.exec();
	}
}

void ViewGUI::openFilesToParse()
{
	QVariant * newRes = new QVariant(false);
	emit configChecked(newRes);
}

void ViewGUI::openDirToParse()
{
	QVariant * newRes = new QVariant(true);
	emit configChecked(newRes);
}

void ViewGUI::clearExperimentCampaign()
{
	emit experimentCampaignCleared();
}

void ViewGUI::setPreviewImage(QPixmap *image)
{
	_main_window->getGenerationWidget()->setPreviewImage(image);
}

void ViewGUI::setExperiments(ParsingExperimentCampaign* campaignExperiment)
{
    _main_window->getInsertionWidget()->updateExperiments(campaignExperiment);
}

void ViewGUI::setFiles(QList<ParsingExperiment*> * experiment, ParsingConfig* config )
{
    _main_window->getInsertionWidget()->updateFiles(experiment, config->getConfig());
}

void ViewGUI::generationGraphFileGenerationRequired()
{
	QFileDialog fileDialog(_main_window, "Generate and save graph");
	fileDialog.setNameFilter(_graphicGenerationFilesPattern);
	fileDialog.setAcceptMode(QFileDialog::AcceptSave);
	fileDialog.setDefaultSuffix("R");
	if (fileDialog.exec())
		emit generationGraphFileGenerated(
			fileDialog.selectedFiles()[0]);
}

void ViewGUI::configFileOpenFile()
{
	QString fileName(QFileDialog::getOpenFileName(
		_main_window,
		"Select configuration file to import"
		));
	if (!fileName.isNull())
		emit configFileParsedFile(fileName);
}
