#include "src/modules/view/viewGUI/MainWindow.h"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	buildInterface();
	relaySignals();
}

MainWindow::~MainWindow()
{}

void MainWindow::buildInterface()
{
	// --
	_central_widget = new QWidget(this);
	setCentralWidget(_central_widget);
	_central_widget_layout = new QVBoxLayout(_central_widget);
	_central_widget->setLayout(_central_widget_layout);

	// ----
	_tabs_widget = new QTabWidget(_central_widget);
	_central_widget_layout->addWidget(_tabs_widget);

	_homepage_widget = new HomepageWidget(_tabs_widget);
	_homepageWdgetPos = _tabs_widget->addTab(_homepage_widget, QString("Homepage"));

	_insertion_widget = new InsertionWidget(_tabs_widget);
	_insertionWidgetPos = _tabs_widget->addTab(_insertion_widget, QString("Insert"));

	_generation_widget = new GenerationWidget(_tabs_widget);
	_generationWidgetPos = _tabs_widget->addTab(_generation_widget, QString("Generate"));

	_management_widget = new ManagementWidget(_tabs_widget);
	_managementWidgetPos = _tabs_widget->addTab(_management_widget, QString("Management"));

	_config_file_widget = new ConfigFileWidget(_tabs_widget);
	_configFileWidgetPos = _tabs_widget->addTab(_config_file_widget, QString("Config files"));


	// -- menu bar
	_menu_bar = new QMenuBar(this);
	setMenuBar(_menu_bar);

	_file_menu = new QMenu("File", _menu_bar);
	_menu_bar->addMenu(_file_menu);
	_file_create_database_action = new QAction("Create database", _file_menu);
	_file_menu->addAction(_file_create_database_action);
	_file_open_database_action = new QAction("Open database", _file_menu);
	_file_menu->addAction(_file_open_database_action);
	_file_import_option_file_action = new QAction("Import config file", _file_menu);
	_file_menu->addAction(_file_import_option_file_action);

	_output_menu = new QMenu("Output", _menu_bar);
	_menu_bar->addMenu(_output_menu);
	_output_generate_graph_action = new QAction("Create graph", _output_menu);
	_output_menu->addAction(_output_generate_graph_action);
	_help_action = new QAction("Help ?", _menu_bar);
	_menu_bar->addAction(_help_action);
}

void MainWindow::relaySignals()
{
	connect(_file_create_database_action, &QAction::triggered, this, &MainWindow::databaseCreated);
	connect(_file_open_database_action, &QAction::triggered, this, &MainWindow::databaseOpened);
	connect(_tabs_widget, &QTabWidget::currentChanged, this, &MainWindow::onTabSelect);
	connect(_homepage_widget, &HomepageWidget::switchToImport, this, &MainWindow::onSwitchToImport);
	connect(_homepage_widget, &HomepageWidget::switchToGraph, this, &MainWindow::onSwitchToGraph);
	connect(_homepage_widget, &HomepageWidget::switchToGestion, this, &MainWindow::onSwitchToGestion);
	connect(_homepage_widget, &HomepageWidget::switchToConfigFile, this, &MainWindow::onSwitchToConfigFile);
}

HomepageWidget *MainWindow::getHomepageWidget() const
{
	return _homepage_widget;
}

InsertionWidget *MainWindow::getInsertionWidget() const
{
	return _insertion_widget;
}

GenerationWidget *MainWindow::getGenerationWidget() const
{
	return _generation_widget;
}

ManagementWidget *MainWindow::getManagementWidget() const
{
	return _management_widget;
}

ConfigFileWidget *MainWindow::getConfigFileWidget() const
{
	return _config_file_widget;
}

// -------- SLOTS --------
void MainWindow::onTabSelect(int index)
{
	// impossible to use switch case statement cause case only can be a constant
	// uses of multiple if with return statement on it do the same thing in this context
	if (index == _insertionWidgetPos) {
		emit insertionTabViewed();
		return;
	} else if (index == _generationWidgetPos) {
		emit generationTabViewed();
		return;
	} else if (index == _managementWidgetPos) {
		emit managementTabViewed();
		return;
	}
}

void MainWindow::onSwitchToImport()
{
	_tabs_widget->setCurrentIndex(_insertionWidgetPos);
}

void MainWindow::onSwitchToGraph()
{
	_tabs_widget->setCurrentIndex(_generationWidgetPos);
}

void MainWindow::onSwitchToGestion()
{
	_tabs_widget->setCurrentIndex(_managementWidgetPos);
}

void MainWindow::onSwitchToConfigFile()
{
	_tabs_widget->setCurrentIndex(_configFileWidgetPos);
}
