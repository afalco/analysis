#ifndef PREVIEWWIDGET_H
#define PREVIEWWIDGET_H

#include <QWidget>
#include <QPixmap>
#include <QImage>
#include <QPaintEvent>
#include <QPainter>
#include <QPalette>
#include <QBrush>

class PreviewWidget : public QWidget
{
	Q_OBJECT

private:
	QPixmap _pixmap,
		_alphaBackground;
	QPalette _palette;
	QBrush _backgroundBrush;

	void buildInterface();
	void fillInterface();

protected:
	void paintEvent(QPaintEvent *);
	void resizeEvent(QResizeEvent *);

public:
	explicit PreviewWidget(QWidget *parent = 0);

signals:
	/**
	 * @brief resized
	 * throws when widget is resized
	 * @param size the new size of the widget after resizing
	 */
	void resized(QSize*);

public slots:
	/**
	 * @brief setPixmap
	 * change pixmap displayed by the widget
	 * @param pixmap the new pixmap
	 */
	void setPixmap(QPixmap);
};

#endif // PREVIEWWIDGET_H
