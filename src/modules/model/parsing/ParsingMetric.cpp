#include "src/modules/model/parsing/ParsingMetric.h"

ParsingMetric::ParsingMetric(std::string value, ParsingMetricPattern * pattern)
{
    _value = value;
	_pattern = pattern;
}

std::string ParsingMetric::getValue()
{
    return _value;
}

ParsingMetricPattern * ParsingMetric::getPattern()
{
	return _pattern;
}

void ParsingMetric::setValue(std::string value)
{
	_value = value;
}
