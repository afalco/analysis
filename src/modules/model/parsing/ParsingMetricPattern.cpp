#include "src/modules/model/parsing/ParsingMetricPattern.h"

ParsingMetricPattern::ParsingMetricPattern(std::string id, std::string name, std::string pattern, std::string type, bool isFraction, std::string table)
{
	_id = id;
    _name = name;
    _pattern = pattern;
    _type = type;
    _isFraction = isFraction;
	_table = table;
}

std::string ParsingMetricPattern::getId()
{
	return _id;
}

std::string ParsingMetricPattern::getName()
{
    return _name;
}

std::string ParsingMetricPattern::getPattern()
{
    return _pattern;
}

std::string ParsingMetricPattern::getType()
{
    return _type;
}

std::string ParsingMetricPattern::getDefault()
{
    return _default;
}

std::string ParsingMetricPattern::getPreOperation()
{
    return _preOperation;
}

std::string ParsingMetricPattern::getPostOperation()
{
    return _postOperation;
}

std::string ParsingMetricPattern::getTable()
{
    return _table;
}

bool ParsingMetricPattern::isFraction()
{
return _isFraction;
}

bool ParsingMetricPattern::isParameter()
{
    return _isParameter;
}

void ParsingMetricPattern::setPatternType(bool isParameter)
{
    _isParameter = isParameter;
}

void ParsingMetricPattern::setDefault(std::string defaultValue)
{
    _default = defaultValue;
}

void ParsingMetricPattern::setPreOperation(std::string preOperation)
{
    _preOperation = preOperation;
}

void ParsingMetricPattern::setPostOperation(std::string postOperation)
{
    _postOperation = postOperation;
}

void ParsingMetricPattern::setTable(std::string table)
{
    _table = table;
}
