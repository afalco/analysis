#include "src/modules/model/parsing/ParsingExperiment.h"
#include <iostream>

ParsingExperiment::ParsingExperiment()
{

}

ParsingExperiment::~ParsingExperiment()
{
	for (std::map<std::string, std::vector<ParsingMetric *> >::iterator itMap = _outputFiles.begin(); itMap != _outputFiles.end(); ++itMap)
	{
		for (std::vector<ParsingMetric *>::iterator it = itMap->second.begin(); it != itMap->second.end(); ++it)
		{
			delete (*it);
		}
	}
}

void ParsingExperiment::setName(std::string name)
{
    _name = name;
}

std::string ParsingExperiment::getName()
{
    return _name;
}

void ParsingExperiment::setOutputFiles(std::map<std::string, std::vector<ParsingMetric *> > outputFiles)
{
    _outputFiles = outputFiles;
}

std::map<std::string, std::vector<ParsingMetric *>> ParsingExperiment::getOutputFiles()
{
    return _outputFiles;
}

void ParsingExperiment::addOutputFile(std::string fileName, std::vector<ParsingMetric *> fileRow)
{
    _outputFiles.insert(std::pair<std::string, std::vector<ParsingMetric *>>(fileName, fileRow));
}

void ParsingExperiment::addMetric(std::string fileName, ParsingMetric * newMetric)
{
	_outputFiles.find(fileName)->second.push_back(newMetric);
}

void ParsingExperiment::setMetric(std::string id, std::string value, ParsingConfig * config, std::string fileName)
{
	bool found = false;
	for(std::map<std::string, std::vector<ParsingMetric *>>::iterator itOutputFiles = _outputFiles.begin(); itOutputFiles != _outputFiles.end(); ++itOutputFiles)
	{
		std::vector<ParsingMetric *> currentMetrics = itOutputFiles->second;
		for (std::vector<ParsingMetric *>::iterator itCurrentMetrics = currentMetrics.begin(); itCurrentMetrics != currentMetrics.end(); ++itCurrentMetrics)
		{
			if ((*itCurrentMetrics)->getPattern()->getId() == id)
			{
				(*itCurrentMetrics)->setValue(value);
				found = true;
			}
		}
	}
	if (!found)
	{
		std::vector<ParsingMetricPattern *> configVector = config->getConfig();
		for (std::vector<ParsingMetricPattern *>::iterator itPattern = configVector.begin(); itPattern != configVector.end(); ++itPattern)
		{
			if ((*itPattern)->getId() == id)
			{
				ParsingMetric * newMetric = new ParsingMetric(value, (*itPattern));
				//fileName = _outputFiles.begin()->first;
				if (fileName == "")
					fileName = _outputFiles.begin()->first;
				this->addMetric(fileName, newMetric);
			}
		}
	}
}
