#ifndef XMLCONTENTHANDLER_H
#define XMLCONTENTHANDLER_H
#include <QtXml>
#include <vector>
#include "src/modules/model/parsing/ParsingMetricPattern.h"

class XmlContentHandler : public QXmlDefaultHandler
{
private:
    std::vector<std::string> _parameters;
    std::vector<std::string> _results;
    std::vector<ParsingMetricPattern *> _patterns;
public:
    explicit XmlContentHandler();
	/**
	 * @brief startElement
	 * a function called each time the Xml parser find a tag while this class is the handler.
	 * @param namespaceURI
	 * not used
	 * @param localName
	 * the name of the tag
	 * @param QName
	 * not used
	 * @param atts
	 * the attributes of the tag
	 * @return the state of the execution on this tag
	 */
    bool startElement(const QString & namespaceURI, const QString & localName,
                                    const QString & QName, const QXmlAttributes & atts );

    std::vector<std::string> getParameters();
    std::vector<std::string> getResults();
    std::vector<ParsingMetricPattern *> getPatterns();
};

#endif // XMLCONTENTHANDLER_H
