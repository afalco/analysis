#ifndef PARSINGCONFIG_H
#define PARSINGCONFIG_H
#include <vector>
#include "src/modules/model/parsing/ParsingMetricPattern.h"
#include <QMetaType>

class ParsingConfig
{
private:
    std::vector<ParsingMetricPattern *> _currentConfig;
public:
    ParsingConfig();
	~ParsingConfig();
    std::vector<ParsingMetricPattern *> getConfig();
    void setConfig(std::vector<ParsingMetricPattern *>);
};

//This allows this class to be send through signals.
Q_DECLARE_METATYPE(ParsingConfig)

#endif // PARSINGCONFIG_H
