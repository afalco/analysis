#include "src/modules/model/parsing/XmlContentHandler.h"
#include <iostream>
#include <regex>
XmlContentHandler::XmlContentHandler() : QXmlDefaultHandler()
{

}

bool XmlContentHandler::startElement(const QString & namespaceURI, const QString & localName,
                                const QString & QName, const QXmlAttributes & atts )
{
	Q_UNUSED(namespaceURI);
	Q_UNUSED(QName);
    if (localName.toStdString() == "variables")
    {
		const std::regex wordRegex("[^ ]+");
        std::smatch sm;
		std::string paramsVariables = atts.value(0).toStdString();
        if (std::regex_search(
					paramsVariables,
                    sm,
					wordRegex))
        {
            auto words_begin =
					std::sregex_iterator(paramsVariables.begin(), paramsVariables.end(), wordRegex);
            auto words_end = std::sregex_iterator();
            for (std::sregex_iterator i = words_begin; i != words_end; ++i) {
                std::smatch match = *i;
                std::string match_str = match.str();
                _parameters.push_back(match_str);
            }
        }
		std::string resultsVariables = atts.value(1).toStdString();
        if (std::regex_search(
					resultsVariables,
                    sm,
					wordRegex))
        {
            auto words_begin =
					std::sregex_iterator(resultsVariables.begin(), resultsVariables.end(), wordRegex);
            auto words_end = std::sregex_iterator();
            for (std::sregex_iterator i = words_begin; i != words_end; ++i) {
                std::smatch match = *i;
                std::string match_str = match.str();
                _results.push_back(match_str);
            }
        }
    }
	else if (localName.toStdString() == "method")
    {
        //For the moment, do nothing
    }
	else if (localName.toStdString() == "pattern")
    {
		std::string id = "";
        std::string name = "";
		std::string type = "";
        std::string pattern = "";
        std::string defaultValue = "";
        std::string preOperation = "";
        std::string postOperation = "";
        std::string table = "";
        bool isFraction = false;
		bool isParameter;
		for(int index = 0; index < atts.length(); index++)
        {
            if (atts.qName(index).toStdString() == "id")
            {
                std::vector<std::string>::iterator findResult = std::find(_parameters.begin(), _parameters.end(), atts.value(index).toStdString());
                if (findResult != _parameters.end())
                    isParameter = true;
                else
                    isParameter = false;
				id= atts.value(index).toStdString();
            }
            else if (atts.qName(index).toStdString() == "print")
            {
                name = atts.value(index).toStdString();
            }
            else if (atts.qName(index).toStdString() == "text")
            {
                pattern = atts.value(index).toStdString();
            }
            else if (atts.qName(index).toStdString() == "elements")
            {
                pattern = atts.value(index).toStdString();
            }
            else if (atts.qName(index).toStdString() == "default")
            {
                defaultValue = atts.value(index).toStdString();
            }
            else if (atts.qName(index).toStdString() == "pre")
            {
                preOperation = atts.value(index).toStdString();
            }
            else if (atts.qName(index).toStdString() == "post")
            {
                postOperation = atts.value(index).toStdString();
            }
            else if (atts.qName(index).toStdString() == "isfraction")
            {
                isFraction=true;
            }
            else if (atts.qName(index).toStdString() == "table")
            {
                table = atts.value(index).toStdString();
            }
			else if (atts.qName(index).toStdString() == "type")
			{
				type = atts.value(index).toStdString();
			}
        }
        if (name != "" && pattern != "")
        {
			pattern = "(" + pattern + ")\\s*" + "(.*)";
			ParsingMetricPattern * newMetricPattern = new ParsingMetricPattern(id, name, pattern, type, isFraction, table);
			if (defaultValue != "")
				newMetricPattern->setDefault(defaultValue);
			if (preOperation != "")
				newMetricPattern->setPreOperation(preOperation);
			if (postOperation != "")
				newMetricPattern->setPostOperation(postOperation);
			newMetricPattern->setPatternType(isParameter);
			_patterns.push_back(newMetricPattern);
        }
    }
    return true;
}

std::vector<std::string> XmlContentHandler::getParameters()
{
    return _parameters;
}

std::vector<std::string> XmlContentHandler::getResults()
{
    return _results;
}

std::vector<ParsingMetricPattern *> XmlContentHandler::getPatterns()
{
    return _patterns;
}
