#ifndef PARSINGEXPERIMENT_H
#define PARSINGEXPERIMENT_H
#include <string>
#include <map>
#include <vector>
#include "src/modules/model/parsing/ParsingMetric.h"
#include <QMetaType>
#include "src/modules/model/parsing/ParsingConfig.h"

class ParsingExperiment
{
private:
    std::string _name;
    std::map<std::string, std::vector<ParsingMetric *>> _outputFiles;
public:
    ParsingExperiment();
	~ParsingExperiment();
    void setName(std::string name);
    std::string getName();
    void setOutputFiles(std::map<std::string, std::vector<ParsingMetric *>> outputFiles);
    std::map<std::string, std::vector<ParsingMetric *>> getOutputFiles();
    void addOutputFile(std::string fileName, std::vector<ParsingMetric *> fileRow);
	void addMetric(std::string fileName, ParsingMetric * newMetric);
	void setMetric(std::string id, std::string value, ParsingConfig * config, std::string fileName);
};

//This allows this class to be send through signals.
Q_DECLARE_METATYPE(ParsingExperiment);
#endif // PARSINGEXPERIMENT_H
