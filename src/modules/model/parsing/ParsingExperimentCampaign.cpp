#include "src/modules/model/parsing/ParsingExperimentCampaign.h"
#include <iostream>

ParsingExperimentCampaign::ParsingExperimentCampaign()
{
	_currentConfig = NULL;
}

ParsingExperimentCampaign::~ParsingExperimentCampaign()
{
	for (std::vector<ParsingExperiment *>::iterator it = _parsingExperiments.begin() ; it != _parsingExperiments.end(); ++it)
	{
		delete (*it);
	}
	delete _currentConfig;
}

void ParsingExperimentCampaign::setName(std::string name)
{
    _name = name;
}

void ParsingExperimentCampaign::setParsingConfig(ParsingConfig* config)
{
    _currentConfig = config;
}

ParsingConfig* ParsingExperimentCampaign::getParsingConfig()
{
    return _currentConfig;
}

void ParsingExperimentCampaign::addExperiment(ParsingExperiment * experiment)
{
    _parsingExperiments.push_back(experiment);
}

std::vector<ParsingExperiment*> ParsingExperimentCampaign::getExperiments()
{
    return _parsingExperiments;
}

std::vector<ParsingExperiment*> ParsingExperimentCampaign::getSelectedExperiments()
{
	return _selectedParsingExperiments;
}

void ParsingExperimentCampaign::addSelectedExperiment(ParsingExperiment* selectedExperiment)
{
	_selectedParsingExperiments.push_back(selectedExperiment);
}

void ParsingExperimentCampaign::clearSelectedExperiments()
{
	_selectedParsingExperiments.clear();
}
