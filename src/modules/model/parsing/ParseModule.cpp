#include <fstream>
#include <regex>
#include <QtGlobal>
#include <QString>
#include <QXmlSimpleReader>
#include <iostream>
#include "dirent.h"

#include "src/modules/model/parsing/XmlContentHandler.h"
#include "src/modules/model/parsing/ParseModule.h"
#include "src/toolbox.h"

ParseModule::ParseModule()
{

}


std::map<std::string, std::vector<ParsingMetric *>> ParseModule::parseFolder(const std::vector<ParsingMetricPattern *> config, const std::string foldername)
{
    std::vector<std::string> filenames;
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (foldername.c_str())) != NULL)
    {
        while ((ent = readdir (dir)) != NULL)
        {
            //Cast for working purposes
            std::string stringDirectoryname = ent->d_name;
            if (stringDirectoryname != "." && stringDirectoryname != "..")
            {
                filenames.push_back(foldername + ent->d_name);
            }
        }
        closedir (dir);
    }
    else {
        //Could not open directory, maybe throw an error
    }
    return parseFiles(config, filenames);
}


std::map<std::string, std::vector<ParsingMetric *>> ParseModule::parseFiles(std::vector<ParsingMetricPattern *> config, std::vector<std::string> filenames)
{
    std::map<std::string, std::vector<ParsingMetric *>> results;
    for (std::vector<std::string>::iterator it = filenames.begin(); it != filenames.end(); ++it)
    {
        std::vector<ParsingMetric *> fileResult = parseFile(config, (*it));
        results.insert(std::pair<std::string, std::vector<ParsingMetric *>>((*it), fileResult));
    }
    return results;
}

std::vector<ParsingMetric *> ParseModule::parseFile(std::vector<ParsingMetricPattern *> config, const std::string filename)
{
    std::string line;
    std::ifstream filetest (filename);
    std::vector<ParsingMetric *> fileRows;
	std::string file;

    if (filetest.is_open())
	{
		while ( getline(filetest, line))
		{
			file += line + " \n";
		}
		filetest.close();
	}
	else
	{
		// Maybe return an error or an exception ?
	}
    // Iterates for each line in the file and check for every pattern in the config if we have a metric to get.

	for (std::vector<ParsingMetricPattern *>::iterator it = config.begin(); it != config.end(); ++it)
	{
        std::string fileRestricted = file;
		const std::regex currentRegex((*it)->getPattern());
		std::smatch sm;
        if ((*it)->getPreOperation() != "")
        {
            const std::regex regexPreOperation((*it)->getPreOperation());
            if (std::regex_search(file, sm, regexPreOperation))
            {
                fileRestricted = sm[0];
            }
        }
        if (std::regex_search(fileRestricted, sm, currentRegex))
		{
            std::string result = sm[2];
            std::smatch smPost;
            if ((*it)->getPostOperation() != "")
            {
                const std::regex regexPostOperation((*it)->getPostOperation());
                if (std::regex_search(result, smPost, regexPostOperation))
                {
                    result = smPost[0];
                }
            }
            if ((*it)->isFraction())
                result = parseFraction(result);
			trim(result);
            fileRows.push_back(new ParsingMetric(result, (*it)));
		}
	}
    return fileRows;
}

std::vector<ParsingMetricPattern *> ParseModule::parseConfig(const std::string filename)
{
    QXmlSimpleReader xmlReader;
    QFile *file = new QFile(filename.c_str());
    QXmlInputSource *source = new QXmlInputSource(file);
    // Each tag of the xml file will be handle in the XmlContentHandler.
    XmlContentHandler* handler 	= new XmlContentHandler();
    xmlReader.setContentHandler(handler);
    bool ok = xmlReader.parse(source);

	//Maybe Throw an error better than print it in the console.
    if (!ok)
        std::cout << "Parsing failed." << std::endl;

	//std::vector<std::string> parameters = handler->getParameters();
    std::vector<ParsingMetricPattern *> patterns = handler->getPatterns();

    return patterns;
}

std::string ParseModule::parseFraction(std::string fraction)
{
	char *locale = std::setlocale(LC_ALL, NULL); // get currect locale in order to restore them later
	std::setlocale(LC_ALL, "C"); // set locale to ensure decimal separator is the point "."
	const std::regex fractionRegex("([0-9]+\\.?[0-9]*).*\\/[^[0-9]]*([0-9]+\\.?[0-9]*)");
	std::smatch sm;
	std::string result = fraction;
	if (std::regex_search(fraction, sm, fractionRegex))
	{
		std::string dividend = sm[1];
		std::string divisor = sm[2];
		result = std::to_string(atof(dividend.c_str()) / atof(divisor.c_str()) * 100.0);
	}
	std::setlocale(LC_ALL, locale); // restore locale
	return result;
}
