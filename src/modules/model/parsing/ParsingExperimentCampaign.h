#ifndef PARSINGEXPERIMENTCAMPAIGN_H
#define PARSINGEXPERIMENTCAMPAIGN_H
#include "src/modules/model/parsing/ParsingExperiment.h"
#include "src/modules/model/parsing/ParsingConfig.h"
#include "src/modules/model/parsing/ParsingMetricPattern.h"
#include <QMetaType>

class ParsingExperimentCampaign
{
private:
    std::vector<ParsingExperiment*> _parsingExperiments;
    //std::vector<Tuple *> _currentConfig;
    ParsingConfig * _currentConfig;
    std::string _name;
	std::vector<ParsingExperiment*> _selectedParsingExperiments;
public:
    ParsingExperimentCampaign();
	~ParsingExperimentCampaign();
    void setName(std::string name);
    void setParsingConfig(ParsingConfig * config);
    ParsingConfig * getParsingConfig();
    void addExperiment(ParsingExperiment * experiment);
	std::vector<ParsingExperiment*> getSelectedExperiments();
    std::vector<ParsingExperiment*> getExperiments();
	void addSelectedExperiment(ParsingExperiment *experiment);
	void clearSelectedExperiments();
};

//This allows this class to be send through signals.
Q_DECLARE_METATYPE(ParsingExperimentCampaign);

#endif // PARSINGEXPERIMENTCAMPAIGN_H
