#include "src/modules/model/parsing/ParsingConfig.h"
#include <iostream>
ParsingConfig::ParsingConfig():
	_currentConfig()
{}

ParsingConfig::~ParsingConfig()
{
	for (std::vector<ParsingMetricPattern *>::iterator it = _currentConfig.begin() ; it != _currentConfig.end(); ++it)
	{
		delete (*it);
	}
}

std::vector<ParsingMetricPattern *> ParsingConfig::getConfig()
{
    return _currentConfig;
}

void ParsingConfig::setConfig(std::vector<ParsingMetricPattern *> config)
{
    _currentConfig = config;
}
