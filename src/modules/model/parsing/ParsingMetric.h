#ifndef PARSINGMETRIC_H
#define PARSINGMETRIC_H
#include "src/modules/model/parsing/ParsingMetricPattern.h"
#include <string>

class ParsingMetric {
private :
    std::string _value;
    ParsingMetricPattern * _pattern;
public :
	explicit ParsingMetric(std::string value, ParsingMetricPattern * pattern);

    std::string getValue();
	void setValue(std::string value);
	ParsingMetricPattern * getPattern();
};

#endif // PARSINGMETRIC_H
