#ifndef PARSINGMETRICPATTERN_H
#define PARSINGMETRICPATTERN_H
#include <string>

class ParsingMetricPattern {
private :
	std::string _id;
    std::string _name;
    std::string _pattern;
    std::string _type;
    std::string _default;
    std::string _preOperation;
    std::string _postOperation;
    std::string _table;
    bool _isFraction;
    bool _isParameter;
public :
    explicit ParsingMetricPattern(std::string id, std::string name, std::string pattern, std::string type, bool isFraction, std::string table);

	std::string getId();
    std::string getName();
    std::string getPattern();
    std::string getType();
    std::string getDefault();
    std::string getPreOperation();
    std::string getPostOperation();
    std::string getTable();
    bool isParameter();
    bool isFraction();
    void setPatternType(bool isParameter);
    void setDefault(std::string defaultValue);
    void setPreOperation(std::string preOperation);
    void setPostOperation(std::string postOperation);
    void setTable(std::string table);
};

#endif // PARSINGMETRICPATTERN_H
