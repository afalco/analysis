#ifndef PARSEMODULE_H
#define PARSEMODULE_H

#include "src/modules/model/Module.h"
#include "src/modules/model/parsing/ParsingMetric.h"
#include <vector>
#include <map>

class ParseModule : public Module {
public:
	explicit ParseModule();
    /**
     * @brief parseFolder
     * applicate the Parsing algorithm for all the files in a folder. Will call parseFiles for all the files in the folder.
     * @param config
	 * a vector of ParsingMetricPattern, technically all the pattern we will have to look for in the files in the folder.
     * @param foldername
     * the name of the folder where we will parse the files.
	 * @return std::map<std::string, std::vector<ParsingMetric *>>, a map where the string is the file name and the vector of ParsingMetric the Metrics linked to this file.
     */
    static std::map<std::string, std::vector<ParsingMetric *>> parseFolder(const std::vector<ParsingMetricPattern *> config, const std::string foldername);
    /**
     * @brief parseFiles
     * Return the metrics found for each files in the parameter. Will call parseFile for each file in the vector.
     * @param config
	 * a vector of ParsingMetricPattern, technically all the pattern we will have to look for in the files in the folder.
	 * @param filenames
     * a vector of string, the name of each file to parse.
	 * @return std::map<std::string, std::vector<ParsingMetric *>>, a map where the string is the file name and the vector of ParsingMetric the Metrics linked to this file.
     */
    static std::map<std::string, std::vector<ParsingMetric *>> parseFiles(const std::vector<ParsingMetricPattern *> config, const std::vector<std::string> filenames);
	/**
	 * @brief parseFile
	 * Return the metrics found for the file in the parameter.
	 * @param config
	 * a vector of ParsingMetricPattern, technically all the pattern we will have to look for in the files in the folder.
	 * @param filename
	 * the name of the file to parse.
	 * @return std::vector<ParsingMetric *>, the metrics linked to the file.
	 */
	static std::vector<ParsingMetric *> parseFile(const std::vector<ParsingMetricPattern *> config, const std::string filename);
	/**
     * @brief parseConfig
     * Return the config from the file in the parameter.
     * @param filename
     * The name of the config file to parse to get the Metric patterns from.
	 * @return std::vector<ParsingMetricPattern *>, the config we get from the file.
     */
    static std::vector<ParsingMetricPattern *> parseConfig(const std::string filename);
	/**
	 * @brief parseFraction
	 * Return the result of the fraction in the string in percentage
	 * '10 / 50' -> 20
	 * @param fraction
	 * the fraction to parse, a number , a slash and an other number
	 * @return std::string, the result of the fraction
	 */
    static std::string parseFraction(std::string fraction);
};

#endif // PARSEMODULE_H
