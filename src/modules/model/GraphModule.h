#ifndef GRAPHMODULE_H
#define GRAPHMODULE_H

#include "src/modules/model/Module.h"
#include "src/modules/model/metricTable/MetricPlotStyle.h"
#include <QString>
#include <QPixmap>
#include <QTemporaryDir>
#include <QProcess>
#include <QVariant>
#include <string>
#include <locale>
#include <vector>
#include <map>

class GraphModule : public Module
{
	Q_GADGET
private:
	explicit GraphModule();

public:
	enum GraphType {
		SCATTER_PLOT,
		LINE_GRAPH,
		HISTOGRAM
	};
	Q_ENUM(GraphType)

	/**
	 * @brief generateRScript
	 * Generate valid R script to generate graph.
	 * Graph will be plotted in file with png format
	 * @param imageName the name of the generated image (without .png extension)
	 * @param width the width in pixel of the generated image
	 * @param height the height in pixel of the generated image
	 * @param abscisse the vector containing the abscisse pf the generated image
	 * @param ordinate the map of vectors containing the ordinate(s) of the generated image
	 * @param graphType the type of the generated graph
	 * @return QString representing the R script
	 */
	static QString generateRScript(
		QString,
		unsigned int,
		unsigned int,
		std::vector<QVariant>,
		std::map<MetricPlotStyle*,std::vector<QVariant> >,
		GraphType);

	/**
	 * @brief generateRImage
	 * Generate image file of the graph
	 * @param width the width in pixel of the generated image
	 * @param height the height in pixel of the generated image
	 * @param abscisse the vector containing the abscisse pf the generated image
	 * @param ordinate the map of vectors containing the ordinate(s) of the generated image
	 * @param graphType the type of the generated graph
	 * @return QPixmap containing the image or null QPixmap if image generating fails
	 */
	static QPixmap generateRImage(
		unsigned int,
		unsigned int,
		std::vector<QVariant>,
		std::map<MetricPlotStyle*,std::vector<QVariant> >,
		GraphType);

	static std::string getGraphType(GraphType);

private:
	static std::string getRPlot(std::string, GraphType);
};

#endif // GRAPHMODULE_H
