#include "src/modules/model/GraphModule.h"

GraphModule::GraphModule() {}

QString GraphModule::generateRScript(
	QString imageName,
	unsigned int width,
	unsigned int height,
	std::vector<QVariant> abscisse,
	std::map<MetricPlotStyle*,std::vector<QVariant> > ordinate,
	GraphType graphType)
	{
	char *locale = std::setlocale(LC_ALL, NULL); // get current locale in order to restore them later
	std::setlocale(LC_ALL, "C"); // set locale to ensure decimal separator is the point "."
	std::string script, splot;
	// prepare image file
	script += "png(filename=\"" + imageName.toStdString() +
		".png\", width=" + std::to_string(width) +
		", height=" + std::to_string(height) + ")\n";

	// generate R data

	script += "abscisse <- c(";
	for(std::vector<QVariant>::iterator it = abscisse.begin();
		it != abscisse.end();
		++it){
			double val = static_cast<QVariant>(*it).toDouble();
			script += std::to_string(val) + ",";
	}
	script.erase(script.size()-1, 1);
	script += ")\n";
	if (ordinate.empty()){
		if(graphType==2){
			splot += getRPlot(std::string(), graphType);
		}
	}
	else
	{
		int count=1;
		for(std::map<MetricPlotStyle*,std::vector<QVariant>>::iterator it = ordinate.begin();
		it != ordinate.end();
		++it){
			std::pair<MetricPlotStyle*, std::vector<QVariant> > val = static_cast<std::pair<MetricPlotStyle*, std::vector<QVariant> > >(*it);
			script += "ordinate_" + val.first->getName().toStdString() + " <- c(";
			splot += getRPlot(val.first->getName().toStdString(),graphType);
			count++;
			for(std::vector<QVariant>::iterator it2 = val.second.begin();
				it2 != val.second.end();
				++it2){
				double val = static_cast<QVariant>(*it2).toDouble();
				script+= std::to_string(val) + ",";
			}

			splot += "par(new=T)\n";
			script.erase(script.size()-1,1);
			script+=")\n";
		}
		splot.erase(splot.size()-12,11);
	}

	// plot the data
	script += splot;

	// flush data into file
	script += "dev.off()";

	std::setlocale(LC_ALL, locale); // restore locale
	return QString::fromStdString(script);
}

std::string GraphModule::getRPlot(std::string val, GraphType graphType){
	std::string plotReturn;
	switch(graphType)
	{
		case SCATTER_PLOT:
			plotReturn = "plot(ordinate_" + val + " ~ abscisse, col=\"blue\")\n";
			break;
		case LINE_GRAPH:
			plotReturn = "plot(abscisse, ordinate_" + val + ", col=\"red\",type=\"o\")\n par(new=T)\n plot(ordinate_" + val + " ~ abscisse, col=\"blue\")\n";
			break;
		case HISTOGRAM:
			plotReturn = "hist(abscisse,col=\"green\")\n";
			break;
		default:
			break;
	}
	return plotReturn;
}


QPixmap GraphModule::generateRImage(
	unsigned int width,
	unsigned int height,
	std::vector<QVariant> abscisse,
	std::map<MetricPlotStyle*,std::vector<QVariant> > ordinate,
	GraphType graphType)
{
	QPixmap image;
	QTemporaryDir tmpDir; // temporary directory
	if (tmpDir.isValid()) {
		// create temporary empty R file to contain script
		QFile scriptFile(tmpDir.path() + "/script.R");
		if (scriptFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
			// generate R script and copy them into temporary file
			QString rScript = generateRScript(tmpDir.path() + "/image", width, height, abscisse, ordinate, graphType);
			scriptFile.write(rScript.toStdString().data());
			scriptFile.close();
		}
		// execute R script command
		int exitCode = QProcess::execute(
			QString("Rscript"),
			QStringList() << "--vanilla" << tmpDir.path() + "/script.R");
		/*QProcess rProcess;
		rProcess.start(
			QString("Rscript"),
			QStringList() << "--vanilla" << tmpDir.path() + "/script.R");
		rProcess.waitForFinished();
		int exitCode = rProcess.exitCode();*/
		if (exitCode == 0) { // if the execution success then read image file into a pixmap
			image = QPixmap(tmpDir.path() + "/image.png");
		}
	}
	tmpDir.remove(); // remove temporary dir and its content
	return image;
}
std::string GraphModule::getGraphType(GraphType graphType){
	std::string graphTypeReturn;
	switch(graphType)
	{
		case SCATTER_PLOT:
			graphTypeReturn="Scatter Plot";
			break;
		case LINE_GRAPH:
			graphTypeReturn="Line Graph";
			break;
		case HISTOGRAM:
			graphTypeReturn="Histogram";
			break;
		default:
			break;
	}
	return graphTypeReturn;
}
