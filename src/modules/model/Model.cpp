#include "src/modules/model/Model.h"

#include <QDir>

#include "src/modules/model/parsing/ParseModule.h"
#include "src/modules/model/database/DatabaseModule.h"
#include "src/modules/model/database/ExperimentCampaign.h"
#include "src/modules/model/database/Hardware.h"
#include "src/modules/model/database/Software.h"
#include "src/modules/model/database/Solver.h"
#include "src/modules/model/database/Matrix.h"
#include "src/modules/model/database/Output.h"

//Totally tmp
#include <iostream>
#include <regex>

Model::Model():
	QObject(),
	_generationMetrics(),
	_generationAbscissa(),
	_generationOrdinate(),
	_generationGraphType(GraphModule::SCATTER_PLOT),
	_previewImageResizing(false),
	_currentPreviewImageSize(),
	_lastPreviewImageSize(),
	_generationImageSize(800, 600)
{
    //ParseModule::testParsing();
    _currentCampaign = new ParsingExperimentCampaign();
    //Change it with the slot linked to the TextField
    _currentCampaign->setName("tmp");

	_generationCurrentDatabaseContent = new MetricTable;
	_generationCurrentOrdinateSelected = new MetricTable;

	_managedExperimentCampaign = NULL;
	_managementMetricsTable = new MetricTable;

	_newParsingConfig = new ParsingConfig;
	_currentParsingConfig = new ParsingConfig;

	bindEvents();
}

Model::~Model()
{
	delete _generationCurrentDatabaseContent;
	delete _generationCurrentOrdinateSelected;
	delete _newParsingConfig;
	delete _currentParsingConfig;
}

void Model::bindEvents()
{
	connect(_generationCurrentDatabaseContent, &MetricTable::valueChanged, this, &Model::onCurrentDatabaseContentChanged);
}

// -------- SLOTS --------
void Model::parseFiles(QStringList *files)
{
    //Conversion QStringList -> Vector of string
    std::vector<std::string> filenames;
    foreach (const QString &str, *files)
    {
        filenames.push_back(str.toStdString());
    }

    std::vector<ParsingMetricPattern *> config;
	config = _currentCampaign->getParsingConfig()->getConfig();

    //parse the files
    std::map<std::string, std::vector<ParsingMetric *>> result = ParseModule::parseFiles(config, filenames);

    //Linked the result with the current campaign
    QStringList* experimentsName = new QStringList();
    for (std::map<std::string, std::vector<ParsingMetric *>>::iterator it = result.begin(); it != result.end(); ++it)
    {
        ParsingExperiment * newExperiment = new ParsingExperiment();
		std::string experimentName = it->first;
		const std::regex regex("([^\\/]+\\/)+");
		std::smatch sm;
		if (std::regex_search(experimentName, sm, regex))
		{
			std::string::size_type i = experimentName.find(sm[0]);
			if (i != std::string::npos)
				experimentName.erase(i, sm[0].length());
		}
		//experimentName.substr();
		newExperiment->setName(experimentName);
        experimentsName->push_back(QString(it->first.c_str()));
        std::map<std::string, std::vector<ParsingMetric *>> newResult;
        newResult.insert(std::pair<std::string, std::vector<ParsingMetric *>>(it->first, it->second));
        newExperiment->setOutputFiles(newResult);
        _currentCampaign->addExperiment(newExperiment);
    }
    emit setExperiments(this->_currentCampaign);
    delete files; // to be nice with valgrind
}

void Model::parseFolder(QStringList *files)
{
    //Conversion QStringList -> Vector of string
    std::vector<std::string> foldernames;
    //Remove the "." and the ".."
    foreach (const QString &str, *files)
    {
        foldernames.push_back(str.toStdString() + "/");
    }
    //Get the Config from DataBase depending on the solver
    //tmp config to test the pastix files because we don't have the exact config yet.
    std::vector<ParsingMetricPattern *> config;
	config = _currentCampaign->getParsingConfig()->getConfig();

    //parse the files
    std::map<std::string, std::vector<ParsingMetric *>> result = ParseModule::parseFolder(config, foldernames.front());

    //Linked the result with the current campaign
    QStringList* experimentsName = new QStringList();
    for (std::map<std::string, std::vector<ParsingMetric *>>::iterator it = result.begin(); it != result.end(); ++it)
    {
        ParsingExperiment * newExperiment = new ParsingExperiment();
        newExperiment->setName(it->first);
        experimentsName->push_back(QString(it->first.c_str()));
        std::map<std::string, std::vector<ParsingMetric *>> newResult;
        newResult.insert(std::pair<std::string, std::vector<ParsingMetric *>>(it->first, it->second));
        newExperiment->setOutputFiles(newResult);
        _currentCampaign->addExperiment(newExperiment);
    }

    emit setExperiments(this->_currentCampaign);
	delete files; // to be nice with valgrind
}

void Model::parseConfig(QStringList *files)
{
	ParsingConfig * newConfig = new ParsingConfig();
	newConfig->setConfig(
		ParseModule::parseConfig(
			files->front().toStdString()));

	_currentCampaign->setParsingConfig(newConfig);
    delete files; // to be nice with valgrind
}

void Model::parseConfigFile(QString file)
{
	std::vector<ParsingMetricPattern *> aaa =
		ParseModule::parseConfig(file.toStdString());
	_newParsingConfig->setConfig(aaa);

	std::vector<ParsingMetricPattern *> configVector
		= _newParsingConfig->getConfig();
	QStringList filePatterns;
	for (std::vector<ParsingMetricPattern *>::iterator
		it = configVector.begin(),
		end = configVector.end();
		it != end; ++it)
	{
		filePatterns.append(
			QString::fromStdString(
				static_cast<ParsingMetricPattern *>(*it)->getName()));
	}
	emit setConfigWidgetConfigFilePatterns(filePatterns);
}

void Model::saveConfigFile(QString name)
{
	std::vector<ParsingMetricPattern *> configVector
		= _newParsingConfig->getConfig();
	QStringList filePatterns;
	ConfigFile confFile;
	confFile.setName(name);
	//confFile.setPath();
	for (std::vector<ParsingMetricPattern *>::iterator
		it = configVector.begin(),
		end = configVector.end();
		it != end; ++it)
	{
		ParsingMetricPattern *pmp = static_cast<ParsingMetricPattern *>(*it);
		MetricPattern *mp = new MetricPattern;
		filePatterns.append(QString::fromStdString(pmp->getName()));
		confFile.addMetricPattern(mp);
		mp->setTable(DatabaseModule::tableNameToTable(
			QString::fromStdString(pmp->getTable())));
		mp->setName(QString::fromStdString(pmp->getId()));
		mp->setPattern(QString::fromStdString(pmp->getPattern()));
		mp->setPrint(QString::fromStdString(pmp->getName()));
		mp->setDefault(QString::fromStdString(pmp->getDefault()));
		mp->setPreOperation(QString::fromStdString(pmp->getPreOperation()));
		mp->setPostOperation(QString::fromStdString(pmp->getPostOperation()));
		mp->setType(QVariant::nameToType(pmp->getType().data()));
		mp->setType(typenameToQVariantType(QString::fromStdString(pmp->getType())));
		mp->isParameter(pmp->isParameter());
		mp->isFraction(pmp->isFraction());
	}
	confFile.save();
}

void Model::openDatabase(QString *name)
{
	DatabaseModule::connect(*name);
	delete name;
}

void Model::createDatabase(QString *name)
{
	std::remove(name->toStdString().data()); // delete old file
	DatabaseModule::connect(*name);
	DatabaseModule::createDatabaseScheme();
	DatabaseModule::populateWithTestData();
	delete name;
}

void Model::resizePreviewImage(QSize *size)
{
	if (!_previewImageResizing) {
		_previewImageResizing = true;

		_lastPreviewImageSize.setWidth(size->width());
		_lastPreviewImageSize.setHeight(size->height());
		_currentPreviewImageSize.setWidth(_lastPreviewImageSize.width());
		_currentPreviewImageSize.setHeight(_lastPreviewImageSize.height());

		// get data from user selection
		_generationAbscissa.clear();
		_generationOrdinate.clear();
		for (std::multimap<QString, MetricContainer*>::const_iterator
			it = _generationCurrentOrdinateSelected->firstItemIterator(),
			last = _generationCurrentOrdinateSelected->lastItemIterator();
			it != last; ++it)
		{
			std::pair<QString, MetricContainer*> item
				= static_cast<std::pair<QString, MetricContainer*>>(*it);
			MetricPlotStyle *style = static_cast<MetricPlotStyle *>(item.second);
			MetricSelector *selector = static_cast<MetricSelector *>(
				_generationCurrentDatabaseContent->getItem(item.first));
			std::vector<QVariant> values = DatabaseModule::getValuesFromMetricSelector(selector);
			for (unsigned int i = 0; i < values.size(); i++)
			_generationOrdinate[style] = values;
			_generationAbscissa = values;
		}

		QPixmap *image = new QPixmap(GraphModule::generateRImage(
			_currentPreviewImageSize.width(),
			_currentPreviewImageSize.height(),
			_generationAbscissa,
			_generationOrdinate,
			_generationGraphType)
		);

		emit setPreviewImage(image);

		_previewImageResizing = false;
	} else {
		_lastPreviewImageSize.setWidth(size->width());
		_lastPreviewImageSize.setHeight(size->height());
	}
	delete size;
}

void Model::getExperimentInformations(QList<QListWidgetItem*> *list)
{
	_currentCampaign->clearSelectedExperiments();
    std::vector<ParsingExperiment *> experiments = _currentCampaign->getExperiments();
    ParsingExperiment * experiment;
	QList<ParsingExperiment*>* newList = new QList<ParsingExperiment*>();

    for (std::vector<ParsingExperiment *>::iterator it = experiments.begin(); it != experiments.end(); ++it)
    {	
		QList<QListWidgetItem*>::iterator i;
		for (i = list->begin(); i != list->end(); ++i)
		{
			std::string iteratedName = (*i)->text().toStdString();
			if ((*it)->getName() == iteratedName)
			{
				experiment = (*it);
				_currentCampaign->addSelectedExperiment(experiment);
				newList->push_back(experiment);
			}
		}
	}
	emit selectedParsedExperiment(newList, _currentCampaign->getParsingConfig());
	delete list;
}

void Model::clearExperimentCampaign()
{
	delete _currentCampaign;
	_currentCampaign = new ParsingExperimentCampaign();
}

void Model::onFindSelectorValuesSet()
{
	emit setFindSelectorValues(_generationCurrentDatabaseContent);
}

void Model::onOrdinateSelectionValuesSet()
{
	emit setOrdinateSelectionValues(_generationCurrentOrdinateSelected);
}

void Model::updateFindDatabaseWidgetContent()
{
	_generationCurrentDatabaseContent->deleteAllContent();
	_generationMetrics = DatabaseModule::selectAllMetrics();
	_generationCurrentDatabaseContent->blockSignals(true);
	for (std::map<std::string, MetricSelector*>::iterator
		it = _generationMetrics.begin(),
		end = _generationMetrics.end();
		it != end;
		++it)
	{
		std::pair<std::string, MetricSelector*> metric =
			static_cast<std::pair<std::string, MetricSelector*> >(*it);
		_generationCurrentDatabaseContent->insertItem(QString::fromStdString(metric.first), metric.second);
	}
	_generationCurrentDatabaseContent->blockSignals(false);
	_generationCurrentDatabaseContent->refresh();

	onCurrentDatabaseContentChanged();
}

void Model::insertMetric(Experiment * experiment, std::string table, std::string id, std::string value)
{
	Hardware * experimentHardware = experiment->getHardware();
	Software * experimentSoftware = experiment->getSoftware();
	Solver * experimentSolver = experiment->getSolver();
	Matrix * experimentMatrix = experiment->getMatrix();
	//Output * experimentOutput = experiment->getOuput();

	//std::vector<Output *> experimentOutputs = experiment->getOutputs();

	if (table == "matrix")
	{
		experimentMatrix->addMetric(id, QVariant::fromValue(QString(value.c_str())));
	}
	else if (table == "hardware")
	{
		experimentHardware->addMetric(id, QVariant::fromValue(QString(value.c_str())));
	}
	else if (table == "software")
	{
		experimentSoftware->addMetric(id, QVariant::fromValue(QString(value.c_str())));
	}
	else if (table == "solver")
	{
		experimentSolver->addMetric(id, QVariant::fromValue(QString(value.c_str())));
	}
	else if (table == "output")
	{
		//experimentOutput->addMetric(id, QVariant::fromValue(QString(value.c_str())));
	}
	else if (table == "experiment")
	{
		experiment->addMetric(id, QVariant::fromValue(QString(value.c_str())));
	}
	else
		qInfo("Table not found [%s]", table.data());
}

void Model::insertExperimentCampaign(QString* experimentCampaignName, QString* experimentCampaignDescription)
{
	//Transform Parsing Class into Database Class
	ExperimentCampaign * ec = new ExperimentCampaign;
	ec->setName(*experimentCampaignName);
	ec->setDescription(*experimentCampaignDescription);
	std::vector<ParsingExperiment *> parsingExperiments = _currentCampaign->getExperiments();
	ParsingConfig * config = _currentCampaign->getParsingConfig();

	std::vector<ParsingMetricPattern *> configVector = config->getConfig();

	/*ConfigFile confFile;
	MetricPattern *mp;
	for (std::vector<ParsingMetricPattern *>::iterator itPattern = configVector.begin(); itPattern != configVector.end(); ++itPattern)
	{
		mp = new MetricPattern;
		ParsingMetricPattern *pmp = static_cast<ParsingMetricPattern *>(*itPattern);
		confFile.addMetricPattern(mp);
		mp->setTable(DatabaseModule::tableNameToTable(
			QString::fromStdString(pmp->getTable())));
		mp->setName(QString::fromStdString(pmp->getId()));
		mp->setPattern(QString::fromStdString(pmp->getPattern()));
		mp->setPrint(QString::fromStdString(pmp->getName()));
		mp->setDefault(QString::fromStdString(pmp->getDefault()));
		mp->setPreOperation(QString::fromStdString(pmp->getPreOperation()));
		mp->setPostOperation(QString::fromStdString(pmp->getPostOperation()));
		mp->setType(QVariant::nameToType(pmp->getType().data()));
		mp->setType(typenameToQVariantType(QString::fromStdString(pmp->getType())));
		mp->isParameter(pmp->isParameter());
		mp->isFraction(pmp->isFraction());
	}
	confFile.save();*/

	for (std::vector<ParsingExperiment *>::iterator itExperiments = parsingExperiments.begin(); itExperiments!= parsingExperiments.end(); ++itExperiments)
	{
		Experiment * newExperiment = new Experiment;
		newExperiment->setName((*itExperiments)->getName());


		std::map<std::string, std::vector<ParsingMetric *>> outputFiles = (*itExperiments)->getOutputFiles();

		//Todo : Maybe store the default Metrics somewhere, or find a better algorithm, or at least factorize it somewhere.
		for (std::vector<ParsingMetricPattern *>::iterator itPattern = configVector.begin(); itPattern != configVector.end(); ++itPattern)
		{
			ParsingMetricPattern * currentTestingPattern = (*itPattern);
			currentTestingPattern->getId();
			if (currentTestingPattern->getDefault() != "")
			{
				bool found = false;
				for (std::map<std::string, std::vector<ParsingMetric *>>::iterator itFiles = outputFiles.begin(); itFiles!= outputFiles.end(); ++itFiles)
				{
					//output
					std::vector<ParsingMetric *> fileRows = itFiles->second;
					for (std::vector<ParsingMetric *>::iterator itMetrics = fileRows.begin(); itMetrics!= fileRows.end(); ++itMetrics)
					{
						ParsingMetricPattern * currentMetricPattern = (*itMetrics)->getPattern();
						if (currentTestingPattern == currentMetricPattern)
						{
							found = true;
						}
					}
				}
				if (!found)
				{
					std::string table = currentTestingPattern->getTable();
					std::string id = currentTestingPattern->getId();
					std::string value = currentTestingPattern->getDefault();
					insertMetric(newExperiment, table, id, value);

				}
			}
		}

		for (std::map<std::string, std::vector<ParsingMetric *>>::iterator itFiles = outputFiles.begin(); itFiles!= outputFiles.end(); ++itFiles)
		{
			// Filename : it2->first
			std::vector<ParsingMetric *> fileRows = itFiles->second;
			for (std::vector<ParsingMetric *>::iterator itMetrics = fileRows.begin(); itMetrics!= fileRows.end(); ++itMetrics)
			{
				std::string table = (*itMetrics)->getPattern()->getTable();
				std::string id = (*itMetrics)->getPattern()->getId();
				std::string value = (*itMetrics)->getValue();
				insertMetric(newExperiment, table, id, value);
			}
		}
		//Date
		//File name
		ec->addExperiment(newExperiment);

	}
	//date
	delete experimentCampaignName;
	delete experimentCampaignDescription;
	ec->save();
	delete ec;
}

ParsingExperimentCampaign * Model::getCurrentCampaign()
{
    return _currentCampaign;
}

void Model::changeMetricValue(QString* id, QString* value, QString* fileName)
{
	std::vector<ParsingExperiment*> selectedExperiments = _currentCampaign->getSelectedExperiments();
	for (std::vector<ParsingExperiment*>::iterator it = selectedExperiments.begin(); it != selectedExperiments.end(); ++it)
	{
		(*it)->setMetric(id->toStdString(), value->toStdString(), _currentCampaign->getParsingConfig(), fileName->toStdString());
	}
	delete id;
	delete value;
	delete fileName;
}

void Model::onCurrentDatabaseContentChanged()
{
	std::map<QString, MetricPlotStyle*> abscissaMetrics, ordinatesMetrics;
        DatabaseModule::selectAbscissaOrdinateMetricRange(
		_generationCurrentDatabaseContent,
		abscissaMetrics,
		ordinatesMetrics);
	_generationCurrentOrdinateSelected->deleteAllContent();
	_generationCurrentOrdinateSelected->blockSignals(true);
	for (std::map<QString, MetricPlotStyle*>::iterator
			it = ordinatesMetrics.begin(),
			end = ordinatesMetrics.end();
		it != end; ++it)
	{
		std::pair<QString, MetricPlotStyle*> metric =
			static_cast<std::pair<QString, MetricPlotStyle*> >(*it);
		metric.second->setName(metric.first);
		_generationCurrentOrdinateSelected->insertItem(metric.first, metric.second);
	}
	_generationCurrentOrdinateSelected->blockSignals(false);
	_generationCurrentOrdinateSelected->refresh();

	QStringList abscissas;
	for (std::map<QString, MetricPlotStyle*>::iterator
			it = abscissaMetrics.begin(),
			end = abscissaMetrics.end();
		it != end; ++it)
	{
		std::pair<QString, MetricPlotStyle*> metric =
			static_cast<std::pair<QString, MetricPlotStyle*> >(*it);
		abscissas.append(metric.first);
	}
	emit setGenerationAbscissaSelectorValues(abscissas);
}

void Model::checkConfig(QVariant * isFolder)
{
	QVariant * result = new QVariant(_currentCampaign->getParsingConfig() != NULL);
	emit configCheckResult(isFolder, result);
}

// related to management
void Model::onManagementMetricTableSetted()
{
	emit managementMetricsLoaded(_managementMetricsTable);
}

void Model::onManagementTabViewed()
{
	std::map<unsigned long int, QString> experimentCampaigns =
		DatabaseModule::getAllExperimentCampaigns();
	emit managementExperimentCampaignsLoaded(experimentCampaigns);
}

void Model::onManagementExperimentCampaignSelected(unsigned long int experimentCampaign)
{
	std::map<unsigned long int, QString> experimentList;
	QString experimentCampaignName, experimentCampaignDescription;
	ExperimentCampaign *ec = NULL;
	try {
		ec = new ExperimentCampaign(experimentCampaign);
	} catch (DatabaseTableException &dte) {
		qInfo("%s", dte.what());
	}
	if (ec != NULL)
	{
		_managedExperimentCampaign = ec;
		experimentCampaignName = ec->getName();
		experimentCampaignDescription = ec->getDescription();
		std::vector<Experiment *> experiments = ec->getExperiments();
		for (std::vector<Experiment *>::iterator
			it = experiments.begin(),
			end = experiments.end();
			it != end; ++it)
		{
			Experiment *exp = static_cast<Experiment *>(*it);
			experimentList.insert(
				std::pair<unsigned long int, QString>(
					exp->getId(),
					QString::fromStdString(exp->getName())));
		}
	}
	emit managementExperimentCampaignLoaded(
		experimentList,
		experimentCampaignName,
		experimentCampaignDescription);
}

void insertMetricsInTable(MetricTable *table, std::map<std::string, QVariant> metrics, DatabaseModule::Tables databaseTable)
{
	for (std::map<std::string, QVariant>::iterator
		it = metrics.begin(),
		end = metrics.end();
		it != end; ++it)
	{
		std::pair<std::string, QVariant> metric =
			static_cast<std::pair<std::string, QVariant> >(*it);
		MetricModifier *mm =
			new MetricModifier(metric.second.type(), databaseTable, QString::fromStdString(metric.first) /* TO CHECK */);
		mm->setValue(metric.second);
		table->insertItem(
			QString::fromStdString(metric.first), mm);
	}
}

void Model::onManagementExperimentsSelected(std::vector<unsigned long int> experiments)
{
	emit managementModifyExperimentName(experiments.size() == 1);
	_managementMetricsTable->deleteAllContent();
	_managementMetricsTable->blockSignals(true);
	for (std::vector<unsigned long int>::iterator
		it = experiments.begin(),
		end = experiments.end();
		it != end; ++it)
	{
		Experiment *exp = NULL;
		try {
			exp = new Experiment(static_cast<unsigned long int>(*it));
		} catch (DatabaseTableException &dte) {
			qInfo("%s", dte.what());
		}
		if (exp != NULL)
		{
			insertMetricsInTable(_managementMetricsTable, exp->getMetrics(), DatabaseModule::EXPERIMENT);
			try {
				Matrix *matrix = exp->getMatrix();
				insertMetricsInTable(_managementMetricsTable, matrix->getMetrics(), DatabaseModule::MATRIX);
			} catch (DatabaseTableException &dte) { qInfo("%s", dte.what()); }
			try {
				insertMetricsInTable(_managementMetricsTable, exp->getHardware()->getMetrics(), DatabaseModule::HARDWARE);
			} catch (DatabaseTableException &dte) { qInfo("%s", dte.what()); }
			try {
				insertMetricsInTable(_managementMetricsTable, exp->getSoftware()->getMetrics(), DatabaseModule::SOFTWARE);
			} catch (DatabaseTableException &dte) { qInfo("%s", dte.what()); }
			try {
				insertMetricsInTable(_managementMetricsTable, exp->getSolver()->getMetrics(), DatabaseModule::SOLVER);
			} catch (DatabaseTableException &dte) { qInfo("%s", dte.what()); }
		}
	}
	_managementMetricsTable->blockSignals(false);
	_managementMetricsTable->refresh();

}

void Model::onManagementUpdateCurrentValues()
{
	if (_managedExperimentCampaign != NULL)
		_managedExperimentCampaign->save();
}

void Model::onGenerationGraphTypeChange(GraphModule::GraphType type)
{
	// TODO refresh graph
	_generationGraphType = type;
}

void Model::onGenerationGraphGenerateFile(QString fileName)
{
	if (fileName.split('.', QString::SkipEmptyParts).last() == "R")
	{
		QString rScript = GraphModule::generateRScript(
			QString::fromStdString(GraphModule::getGraphType(_generationGraphType)),
			_generationImageSize.width(),
			_generationImageSize.height(),
			_generationAbscissa,
			_generationOrdinate,
			_generationGraphType);
		QFile file(fileName);
		file.open(QFile::WriteOnly);
		file.write(rScript.toStdString().data());
		file.close();
	}
	else
	{
		QPixmap graph = GraphModule::generateRImage(
			_generationImageSize.width(),
			_generationImageSize.height(),
			_generationAbscissa,
			_generationOrdinate,
			_generationGraphType);
	}
}

void Model::onInsertionTabViewed()
{
	emit setInsertionWidgetParserOptions(
		DatabaseModule::getAllConfigFile());
}

void Model::onInsertionWidgetParserOptionSelected(unsigned long int id)
{
	try {
		ConfigFile *cf = new ConfigFile(id);
		std::vector<MetricPattern *> metricPatterns =
			cf->getMetricPattern();
		std::vector<ParsingMetricPattern *> parsingMetrics;

		for (std::vector<MetricPattern *>::iterator
			it = metricPatterns.begin(),
			end = metricPatterns.end();
			it !=end; ++it)
		{
			MetricPattern *mp = static_cast<MetricPattern *>(*it);
			ParsingMetricPattern *pmp = new ParsingMetricPattern(
				mp->getName().toStdString(),
				mp->getPrint().toStdString(),
				mp->getPattern().toStdString(),
				qVariantTypeToTypename(mp->getType()).toStdString(),
				mp->isFraction(),
				DatabaseModule::tableToTableName(mp->getTable()).toStdString()
				);
			parsingMetrics.push_back(pmp);
			pmp->setDefault(mp->getDefault().toStdString());
			pmp->setPreOperation(mp->getPreOperation().toStdString());
			pmp->setPostOperation(mp->getPostOperation().toStdString());
			pmp->setPatternType(mp->isParameter());
		}
		_currentParsingConfig->setConfig(parsingMetrics);
		_currentCampaign->setParsingConfig(_currentParsingConfig);
	} catch (DatabaseTableException &dte)
	{
		qInfo("%s", dte.what());
	}
}

void Model::onGenerationSelectAbscissaMetric(unsigned long int abscissaId)
{
	// TODO
}
