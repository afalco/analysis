#include "src/modules/model/database/DatabaseTableException.h"

DatabaseTableException::DatabaseTableException(const std::string &table, const std::string &info)
{
	_info = new std::string("Error: in table ");
	*_info += table + " : " + info;
}

DatabaseTableException::~DatabaseTableException()
{
	//delete _info;
}

const char* DatabaseTableException::what() const throw()
{
	return _info->data();
}
