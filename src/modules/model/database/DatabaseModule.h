#ifndef DATABASEMODULE_H
#define DATABASEMODULE_H

#include "src/toolbox.h"
#include "src/modules/model/Module.h"
#include "src/modules/model/database/Matrix.h"
#include "src/modules/model/database/Hardware.h"
#include "src/modules/model/database/Solver.h"
#include "src/modules/model/database/Software.h"
#include "src/modules/model/database/Output.h"
#include "src/modules/model/database/ExperimentCampaign.h"
#include "src/modules/model/database/Experiment.h"
class ConfigFile;//#include "model/database/ConfigFile.h"
class MetricPattern;//#include "model/database/MetricPattern.h"
class MetricTable;//#include "model/SelectTable.h"
class MetricSelector; //#include "model/MetricSelector.h"
#include "src/modules/model/metricTable/PlotStyleChooser.h"
class MetricPlotStyle; //#include "model/MetricPlotStyle.h"

#include <string>
#include <vector>
#include <fstream>
#include <sstream>

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>

#include <QtSql/QSqlRecord>
#include <QVariant>

/*
	<-TODO->
Get sql request from sql file
configure parser to load file depending on database module loaded
explode file string using ; semicolon
create some select functions
*/
class DatabaseModule : public Module
{
private:
	explicit DatabaseModule();

	static bool _isConnected;						// store if a sqldatabase have been connected
	static QSqlDatabase _database;					// store instance of database driver

	static const QString _EXPERIMENT_CAMPAIGN,	// strings to convert Tables enum to table name or reversely
		_EXPERIMENT,
		_MATRIX,
		_SOFTWARE,
		_HARDWARE,
		_OUTPUT,
		_SOLVER,
		_CONFIG_FILE,
		_METRIC_PATTERN,
		_INVALID;

	static std::vector<std::string> _selectExperimentCampaign,		// related to experiment campaign
		_createTables,					// queries to create tables
		_populateDatabase;				// queries to populate database with test values

	static std::string _createExperimentCampaign,// related to experiment campaign
		_updateExperimentCampaign,
		_selectExperiment,				// related to experiment
		_createExperiment,
		_updateExperiment,
		_deleteExperiment,
		_selectExperiments,

		_selectMatrix,					// related to matrix
		_createMatrix,
		_updateMatrix,
		_deleteMatrix,
		_selectSoftware,				// related to software
		_createSoftware,
		_updateSoftware,
		_deleteSoftware,
		_selectHardware,				// related to hardware
		_createHardware,
		_updateHardware,
		_deleteHardware,
		_selectSolver,					// related to solver
		_createSolver,
		_updateSolver,
		_deleteSolver,
		_selectOutput,					// related to output
		_createOutput,
		_updateOutput,
		_deleteOutput,
		_selectMetrics,					// related to metric pattern
		_selectMetricInterval,
		_selectAllFromTable,			// related to all table
		_selectTableByExperimentId;

	/**
	 * @brief SQLFileToString read an SQL file and return it's content
	 * @param filename is the file to read
	 * @return a std::string representing the file content
	 */
	static std::string SQLFileToString(const std::string);

	/**
	 * @brief SQLFileToVector read an SQL file and return a list of it's content. Each list element represents a query present in input file.
	 * @param filename is the file to read
	 * @return a std::list od std::string representing each queries in input file
	 */
	static std::vector<std::string> SQLFileToVector(const std::string);

	// Test method : print query result if query is a select query
	static void printQuery(QSqlQuery &);
	// Test method : print query error if query is in error state
	static void printQueryError(const QSqlQuery &);

	// ---------------- Methods related to MetricPattern ----------------
	static void bindQueryValues(QSqlQuery &, const MetricPattern*);

public:
	enum Driver {
		SQLITE,
		MYSQL
	};

	// setup database informations
	/**
	 * @brief connect connects class to a database and load required data like sql queries from files in app directory
	 * @param name represents the name of the database to open
	 * @return true on connection success and false if an error occurs
	 */
	static bool connect(const QString, Driver driver = DatabaseModule::SQLITE);
	/**
	 * @brief disconnect disconnects actual driver from the database
	 */
	static void disconnect();

	/**
	 * @brief isConnected is used to know if the database is connected
	 * @return true if a database is connected and false otherwise
	 */
	static bool isConnected();

	// ---------------- Methods related to table names ----------------
	enum Tables {
		EXPERIMENT_CAMPAIGN,
		EXPERIMENT,
		MATRIX,
		SOFTWARE,
		HARDWARE,
		OUTPUT,
		SOLVER,
		CONFIG_FILE,
		METRIC_PATTERN,
		INVALID
	};
	/**
	 * @brief tableNameToTable
	 * Convert table name in string to corresponding value in
	 * Tables enum. Returns INVALID in case of bad name
	 * @param tableName the name of the table to convert
	 * @return a value in Table enum
	 */
	static Tables tableNameToTable(const QString &);

	/**
	 * @brief tableToTableName
	 * Convert table value in Tables enum to string name of it.
	 * Returns a empty string in case of INVALID Tables value
	 * @param table The Tables enum to convert
	 * @return the name of corresponding table
	 */
	static QString tableToTableName(Tables);

	/// execute queries
	/**
	 * @brief createDatabaseScheme create the database structure inserting all required tables
	 * @return true on success and false if an error occurs
	 */
	static bool createDatabaseScheme();

	// ---------------- Methods related to Experiment Campaign ----------------
	static bool selectExperimentCampaign(const unsigned long int, ExperimentCampaign *);
	static unsigned long int createExperimentCampaign(const ExperimentCampaign *);
	static bool updateExperimentCampaign(const ExperimentCampaign *);

	static std::map<unsigned long int, QString> getAllExperimentCampaigns();

	// ---------------- Methods related to Experiment ----------------
	/**
	 * @brief selectExperimentFromId
	 * Get the data of the experiment from database and update experiment objet consequently.
	 * Returns true if id exists and false otherwise.
	 * @param id
	 * The unique identifier of the experiment
	 * @param experimentCampaign
	 * A pointer on the experiment campaign to update
	 * @return True if id exists and false otherwise
	 */
	static bool selectExperiment(const unsigned long int, Experiment *);

	static unsigned long int createExperiment(const Experiment *);
	static bool updateExperiment(const Experiment *);

	/**
	 * @brief selectExperimentsFromExperimentCampaignId
	 * Return a list of experiments associated to experiment campaign id. Can return empty list in case of no matches
	 * @param id
	 * The unique identifier of the experiment campaign
	 * @return std::vector<Experiment*>
	 */
	static std::vector<Experiment*> selectExperimentsFromExperimentCampaignId(unsigned long int);

	/**
	 * @brief getExperimentsOfExperimentCampaign
	 * Get all experiments in an experiment campaign
	 * @param experimentCampaign
	 * the id of the experiment campaign to get
	 * @return a list of experiment ids
	 */
	static std::vector<unsigned long int> getExperimentsOfExperimentCampaign(unsigned long int);

	// ---------------- Methods related to Matrix ----------------
	/**
	 * @brief selectMatrixByCampaignId
	 * Return the Matrix associated to an experimentCampaign id.
	 * @param id
	 * The unique identifier of the experiment campaign
	 * @return Matrix* object, can be NULL
	 */

	static bool selectMatrix(unsigned long int, Matrix *);
	static Matrix *selectMatrix(const Experiment *);
	static unsigned long int createMatrix(const Matrix *);
	static bool updateMatrix(const Matrix *);

	// ---------------- Methods related to Software ----------------
	/**
	 * @brief selectSoftwareByCampaignId
	 * Return the Software associated to an experimentCampaign id.
	 * @param id
	 * The unique identifier of the experiment campaign
	 * @return Software* object, can be NULL
	 */
	static bool selectSoftware(unsigned long int, Software *);
	static Software *selectSoftware(const Experiment *);

	static unsigned long int createSoftware(const Software *);
	static bool updateSoftware(const Software *);

	// ---------------- Methods related to Hardware ----------------
	/**
	 * @brief selectHardwareByCampaignId
	 * Return the Hardware associated to an experimentCampaign id.
	 * @param id
	 * The unique identifier of the experiment campaign
	 * @return Hardware* object, can be NULL
	 */
	static bool selectHardware(unsigned long int, Hardware *);
	static Hardware *selectHardware(const Experiment *);

	static unsigned long int createHardware(const Hardware *);
	static bool updateHardware(const Hardware *);

	// ---------------- Methods related to Solver ----------------
	/**
	 * @brief selectSolverByCampaignId
	 * Return the Solver associated to an experimentCampaign id.
	 * @param id
	 * The unique identifier of the experiment campaign
	 * @return Solver* object, can be NULL
	 */
	static bool selectSolver(unsigned long int, Solver *);
	static Solver *selectSolver(const Experiment *);

	static unsigned long int createSolver(const Solver *);
	static bool updateSolver(const Solver *);

	// ---------------- Methods related to Output ----------------
	static bool selectOutput(const Experiment *, Output *);
	static unsigned long int createOutput(const Output *);
	static bool updateOutput(const Output *);

	// ---------------- Methods related to MetricPattern ----------------
	static bool selectMetricPattern(const unsigned long int, MetricPattern *);
	static std::vector<MetricPattern*> selectMetricPatternsOfConfigFile(const ConfigFile *);
	static unsigned long int createMetricPattern(const MetricPattern *);
	static bool updateMetricPattern(const MetricPattern *);

	// ---------------- Methods related to Config File ----------------
	static bool selectConfigFile(const unsigned long int, ConfigFile *);
	static unsigned long int createConfigFile(const ConfigFile *);
	static bool updateConfigFile(const ConfigFile *);
	static std::map<unsigned long int, QString> getAllConfigFile();

	// ---------------- Methods related to all table ----------------
	// Test method : fill database with data, AWESOME !!!
	static bool populateWithTestData();
	static std::map<std::string, MetricSelector*> selectAllMetrics();
	static std::vector<QVariant> getValuesFromMetricSelector(MetricSelector*);
	static void selectAbscissaOrdinateMetricRange(const MetricTable *, std::map<QString, MetricPlotStyle*>&, std::map<QString, MetricPlotStyle*>&);
};

#endif // DATABASEMODULE_H
