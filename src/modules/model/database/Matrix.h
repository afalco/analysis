#ifndef MATRIX_H
#define MATRIX_H

#include "src/modules/model/database/DatabaseTableMetrics.h"
class Experiment;//#include "model/database/Experiment.h"

#include <QVariant>

#include <string>

class Matrix : public DatabaseTableMetrics
{
private:
	Experiment *_associatedExperiment;	// foreign key associated
	std::string _name;

public:
	explicit Matrix();
	explicit Matrix(unsigned long int);
	//explicit Matrix(Experiment *);

	void save();

	/* COMMON SETTERS */
	void setName(std::string);
	void setExperiment(Experiment *);

	/* COMMON GETTERS */
	const std::string getName()const;
	Experiment *getExperiment();
};

#endif // MATRIX_H
