#include "src/modules/model/database/ConfigFile.h"
#include "src/modules/model/database/DatabaseModule.h"

ConfigFile::ConfigFile():
	DatabaseTable(0),
	_name(), _path(), _metricPatterns()
{
	_new = true;
	_changed = true;
}

ConfigFile::ConfigFile(unsigned long int id):
	DatabaseTable(id),
	_name(), _path(), _metricPatterns()
{
	_new = false;
	_changed = false;

	if (!DatabaseModule::selectConfigFile(id, this))
		throw DatabaseTableException("ConfigFile", "id " + std::to_string(id) + " not found, unable to create config file");
}

ConfigFile::~ConfigFile()
{}

void ConfigFile::save()
{
	if (_changed) // if the structure has changed
	{
		unsigned long int newId = DatabaseModule::createConfigFile(this);
		if (newId != 0ul)
		{
			_id = newId;
			_new = false;
			_changed = false;

			for (std::vector<MetricPattern *>::iterator
				it = _metricPatterns.begin(),
				end = _metricPatterns.end();
				it != end; ++it)
			{
				static_cast<MetricPattern *>(*it)->save();
			}
		}
	}
	else // if instance of class already exists in database
	{
		DatabaseModule::updateConfigFile(this);
		_changed = false;
	}
}

void ConfigFile::addMetricPattern(MetricPattern *metricPattern)
{
	metricPattern->setAssociatedConfigFile(this);
	_metricPatterns.push_back(metricPattern);
	_changed = true;
}

void ConfigFile::addMetricPatterns(std::vector<MetricPattern *> metricPatterns)
{
	//_metricPatterns.insert(_metricPatterns.end(), metricPatterns.begin(), metricPatterns.end());
	for (std::vector<MetricPattern *>::iterator
		it = metricPatterns.begin(),
		end = metricPatterns.end();
		it != end; ++it)
	{
		MetricPattern *metricPattern
			= static_cast<MetricPattern *>(*it);
		metricPattern->setAssociatedConfigFile(this);
		_metricPatterns.push_back(metricPattern);
	}
	_changed = true;
}

/* COMMON SETTERS */
void ConfigFile::setName(QString name)
{
	_name = name;
	_changed = true;
}

void ConfigFile::setPath(QString path)
{
	_path = path;
	_changed = true;
}

/* COMMON GETTERS */
QString ConfigFile::getName() const
{
	return _name;
}

QString ConfigFile::getPath() const
{
	return _path;
}

std::vector<MetricPattern*> ConfigFile::getMetricPattern()
{
	return _metricPatterns;
}
