#ifndef DATABASETABLEEXCEPTION_H
#define DATABASETABLEEXCEPTION_H

#include <exception>
#include <string>

class DatabaseTableException : public std::exception
{
private:
	std::string *_info;

public:
	/**
	 * @brief DatabaseException
	 * Exception class for DatabaseTable
	 * @param table
	 * The table name of the emitter
	 * @param info
	 * Information about the error
	 */
	DatabaseTableException(const std::string&, const std::string&);
	~DatabaseTableException();
	virtual const char* what() const throw();
};


#endif // DATABASETABLEEXCEPTION_H
