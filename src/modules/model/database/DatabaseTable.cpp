#include "src/modules/model/database/DatabaseTable.h"

DatabaseTable::DatabaseTable(unsigned long int id):
	_id(id), _new(true), _changed(false)
{}

DatabaseTable::~DatabaseTable()
{}

bool DatabaseTable::hasChanged()
{
	return _changed;
}

/* COMMON GETTERS */
unsigned long int DatabaseTable::getId() const
{
	return _id;
}
