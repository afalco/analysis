#ifndef OUTPUT_H
#define OUTPUT_H

#include "src/modules/model/database/DatabaseTable.h"

#include <string>

#include <QTime>

class Output : public DatabaseTable
{
private:
	std::string _name;
	QTime _time;

public:
	explicit Output(unsigned long int);

	void save();

	/* COMMON SETTERS */
	void setName(std::string);
	void setTime(QTime);

	/* COMMON GETTERS */
	const std::string getName()const;
	QTime getTime();
};

#endif // OUTPUT_H
