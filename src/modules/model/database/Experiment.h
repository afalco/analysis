#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include "src/modules/model/database/DatabaseTableMetrics.h"
#include "src/modules/model/database/DatabaseTableException.h"
#include "src/modules/model/database/ExperimentCampaign.h"
class Matrix; //#include "model/database/Matrix.h"
class Software; //#include "model/database/Software.h"
class Hardware; //#include "model/database/Hardware.h"
class Solver; //#include "model/database/Solver.h"
#include "src/modules/model/database/Output.h"

#include <QVariant>

#include <string>
#include <map>

class Experiment : public DatabaseTableMetrics
{
private:
	ExperimentCampaign *_foreignKey;
	std::string _name;
	Matrix *_matrix;				// corresponding to Matrix foreign key
	Software *_software;			// corresponding to Software foreign key
	Hardware *_hardware;			// corresponding to hardware foreign key
	Solver *_solver;				// corresponding to Solver foreign key
	std::vector<Output *> _outputs;	// corresponding to Experiment foreign key in Output

public:
	/**
	 * @brief Experiment
	 * Constructor for a new instance of class without database content.
	 * Class constructed by this method is not yet in the database, must call
	 * save() method to insert the class in database
	 */
	explicit Experiment();

	/**
	 * @brief Experiment
	 * Constructor for an existing instance of class from database.
	 * Class constructed from existing database instance.
	 */
	explicit Experiment(unsigned long int);

	void save();

	bool haveMatrix() const;
	bool haveSoftware() const;
	bool haveHardware() const;
	bool haveSolver() const;

	/* COMMON SETTERS */
	void setName(std::string);
	void setExperimentCampaign(ExperimentCampaign *);
	void setMatrix(Matrix *);
	void setSoftware(Software *);
	void setHardware(Hardware *);
	void setSolver(Solver *);

	/* COMMON GETTERS */
	const std::string getName() const;
	ExperimentCampaign *getExperimentCampaign() const;
	Matrix *getMatrix();
	Software *getSoftware();
	Hardware *getHardware();
	Solver *getSolver();
	std::vector<Output *> getOutputs();
	const Matrix *getMatrix() const;
	const Software *getSoftware() const;
	const Hardware *getHardware() const;
	const Solver *getSolver() const;
};

#endif // EXPERIMENT_H
