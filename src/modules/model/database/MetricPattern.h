#ifndef METRICPATTERN_H
#define METRICPATTERN_H

#include "src/modules/model/database/DatabaseModule.h"
#include "src/modules/model/database/DatabaseTableMetrics.h"

class MetricPattern : public DatabaseTableMetrics
{
private:
	ConfigFile *_associatedConfigFile;	// equivalent of foreign key
	DatabaseModule::Tables _destinationTable;
	QString _name, // id
		_pattern,
		_print, // name
		_default,
		_preOperation,
		_postOperation;
	QVariant::Type _type;
	bool _isParameter,
		_isFraction;

public:
	MetricPattern();
	explicit MetricPattern(unsigned long int);

	void save();

	/* COMMON SETTERS */
	void setAssociatedConfigFile(ConfigFile *);
	void setTable(DatabaseModule::Tables);
	void setName(QString);
	void setPattern(QString);
	void setPrint(QString);
	void setDefault(QString);
	void setPreOperation(QString);
	void setPostOperation(QString);
	void setType(QVariant::Type);
	void isParameter(bool);
	void isFraction(bool);

	/* COMMON GETTERS */
	ConfigFile *getAssociatedConfigFile() const;
	DatabaseModule::Tables getTable() const;
	QString getName() const;
	QString getPattern() const;
	QString getPrint() const;
	QString getDefault() const;
	QString getPreOperation() const;
	QString getPostOperation() const;
	QVariant::Type getType() const;
	bool isParameter() const;
	bool isFraction() const;
};

#endif // METRICPATTERN_H
