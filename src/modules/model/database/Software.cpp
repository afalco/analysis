#include "src/modules/model/database/Software.h"
#include "src/modules/model/database/DatabaseModule.h"

Software::Software():
	DatabaseTableMetrics(0),
	_compiler(), _libraries(), _associatedExperiment(NULL)
{
	_new = true;
	_changed = true;
}

Software::Software(unsigned long int id):
	DatabaseTableMetrics(id),
	_compiler(), _libraries(), _associatedExperiment(NULL)
{
	_new = false;
	_changed = false;

	if (!DatabaseModule::selectSoftware(id, this))
		throw DatabaseTableException("Software", "id " + std::to_string(id) + " not found, unable to create software");
}

void Software::save()
{
	if (_changed) // if the structure has changed
	{
		if (_new) // if the class have never been saved
		{
			unsigned long int newId = DatabaseModule::createSoftware(this);
			if (newId != 0ul)
			{
				_id = newId;
				_new = false;
				_changed = false;
			}
		}
		else // if instance of class already exists in database
		{
			DatabaseModule::updateSoftware(this);
			_changed = false;
		}
	}
}

/* COMMON SETTERS */
void Software::setCompiler(std::string compiler)
{
	_changed = true;
	_compiler = compiler;
}

void Software::setLibraries(std::string libraries)
{
	_changed = true;
	_libraries = libraries;
}

void Software::setExperiment(Experiment *experiment)
{
	_changed = true;
	_associatedExperiment = experiment;
}

/* COMMON GETTERS */
const std::string Software::getCompiler()const
{
	return _compiler;
}

const std::string Software::getLibraries()const
{
	return _libraries;
}


Experiment *Software::getExperiment()
{
	return _associatedExperiment;
}
