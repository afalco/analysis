#ifndef DATABASETABLEMETRICS_H
#define DATABASETABLEMETRICS_H

#include "src/modules/model/database/DatabaseTable.h"

#include <QVariant>

#include <string>
#include <map>

class DatabaseTableMetrics : public DatabaseTable
{
private:
	std::map<std::string, QVariant> _metrics;

public:
	explicit DatabaseTableMetrics(unsigned long int);

	const std::map<std::string, QVariant> getMetrics() const;

	/**
	* @brief addMetric
	* Add a metric in actual experiment object.
	* @param name the name of the metric
	* @param value the value of the metric
	* @return true if the metric has been inserted or false if already exists
	*/
	bool addMetric(std::string, QVariant);

	QVariant getMetric(const std::string);
};

#endif // DATABASETABLEMETRICS_H
