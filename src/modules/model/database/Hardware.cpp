#include "src/modules/model/database/Hardware.h"
#include "src/modules/model/database/DatabaseModule.h"

Hardware::Hardware():
	DatabaseTableMetrics(0),
	_associatedExperiment(NULL)
{
	_new = true;
	_changed = true;
}

Hardware::Hardware(unsigned long int id):
	DatabaseTableMetrics(id),
	_associatedExperiment(NULL)
{
	_new = false;
	_changed = false;

	if (!DatabaseModule::selectHardware(id, this))
		throw DatabaseTableException("hardware", "id " + std::to_string(id) + " not found, unable to create hardware");
}

void Hardware::save()
{
	if (_changed) // if the structure has changed
	{
		if (_new) // if the class have never been saved
		{
			unsigned long int newId = DatabaseModule::createHardware(this);
			if (newId != 0ul)
			{
				_id = newId;
				_new = false;
				_changed = false;
			}
		}
		else // if instance of class already exists in database
		{
			DatabaseModule::updateHardware(this);
			_changed = false;
		}
	}
}

/* COMMON SETTERS */
void Hardware::setExperiment(Experiment *experiment)
{
	_changed = true;
	_associatedExperiment = experiment;
}

/* COMMON GETTERS */
Experiment *Hardware::getExperiment()
{
	return _associatedExperiment;
}
