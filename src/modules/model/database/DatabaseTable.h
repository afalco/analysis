#ifndef DATABASETABLE_H
#define DATABASETABLE_H

class DatabaseTable
{
protected:
	unsigned long int _id;
	bool _new;			// true if tuple is a new tuple (not already in database)
	bool _changed;		// true if tuple value(s) has changed (since last save)

public:
	explicit DatabaseTable(unsigned long int);
	virtual ~DatabaseTable();

	/**
	 * @brief save
	 * Save table content in database.
	 * If is a new tuple, an insert is done and update it's _id attribute.
	 * If is an existing modified tuple, an update is done.
	 * If no values has changed, nothing is done.
	 */
	virtual void save() = 0;

	/**
	 * @brief clone
	 * Clone current table content.
	 * Set it's _new property to true and _changed to false.
	 * @return DatabaseTable* the new cloned content
	 */
	//virtual DatabaseTable *clone() = 0;

	bool hasChanged();

	/* COMMON GETTERS */
	unsigned long int getId() const;
};

#endif // DATABASETABLE_H
