#include "src/modules/model/database/Output.h"
#include "src/modules/model/database/DatabaseModule.h"

Output::Output(unsigned long int id):
	DatabaseTable(id),
	_name(), _time()
{}

void Output::save()
{
	// TODO
}

/* COMMON SETTERS */
void Output::setName(std::string name)
{
	_name = name;
}

void Output::setTime(QTime time)
{
	_time = time;

/* COMMON GETTERS */
}
const std::string Output::getName()const
{
	return _name;
}

QTime Output::getTime()
{
	return _time;
}
