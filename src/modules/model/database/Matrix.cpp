#include "src/modules/model/database/Matrix.h"
#include "src/modules/model/database/DatabaseModule.h"

Matrix::Matrix():
	DatabaseTableMetrics(0),
	_associatedExperiment(NULL),
	_name()
{
	_new = true;
	_changed = true;
}

Matrix::Matrix(unsigned long int id):
	DatabaseTableMetrics(id),
	_associatedExperiment(NULL),
	_name()
{
	_new = false;
	_changed = false;

	if (!DatabaseModule::selectMatrix(id, this))
		throw DatabaseTableException("Matrix", "id " + std::to_string(id) + " not found, unable to create matrix");
}

void Matrix::save()
{
	if (_changed) // if the structure has changed
	{
		if (_new) // if the class have never been saved
		{
			unsigned long int newId = DatabaseModule::createMatrix(this);
			if (newId != 0ul)
			{
				_id = newId;
				_new = false;
				_changed = false;
			}
		}
		else // if instance of class already exists in database
		{
			DatabaseModule::updateMatrix(this);
			_changed = false;
		}
	}
}

/* COMMON SETTERS */
void Matrix::setName(std::string name)
{
	_changed = true;
	_name = name;
}

void Matrix::setExperiment(Experiment *experiment)
{
	_changed = true;
	_associatedExperiment = experiment;
}

/* COMMON GETTERS */
const std::string Matrix::getName()const
{
	return _name;
}

Experiment *Matrix::getExperiment()
{
	return _associatedExperiment;
}

