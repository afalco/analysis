#include "src/modules/model/database/Solver.h"
#include "src/modules/model/database/DatabaseModule.h"

Solver::Solver():
	DatabaseTableMetrics(0),
	_associatedExperiment(NULL),
	_name(), _version(), _options()
{
	_new = true;
	_changed = true;
}

Solver::Solver(unsigned long int id):
	DatabaseTableMetrics(id),
	_associatedExperiment(NULL),
	_name(), _version(), _options()
{
	_new = false;
	_changed = false;

	if (!DatabaseModule::selectSolver(id, this))
		throw DatabaseTableException("Solver", "id " + std::to_string(id) + " not found, unable to create solver");
}

void Solver::save()
{
	if (_changed) // if the structure has changed
	{
		if (_new) // if the class have never been saved
		{
			unsigned long int newId = DatabaseModule::createSolver(this);
			if (newId != 0ul)
			{
				_id = newId;
				_new = false;
				_changed = false;
			}
		}
		else // if instance of class already exists in database
		{
			DatabaseModule::updateSolver(this);
			_changed = false;
		}
	}
}

/* COMMON SETTERS */
void Solver::setExperiment(Experiment *experiment)
{
	_changed = true;
	_associatedExperiment = experiment;
}
void Solver::setName(std::string name)
{
	_changed = true;
	_name = name;
}
void Solver::setVersion(std::string version)
{
	_changed = true;
	_version = version;
}
void Solver::setOptions(std::string options)
{
	_changed = true;
	_options = options;
}

/* COMMON GETTERS */
Experiment *Solver::getExperiment()
{
	return _associatedExperiment;
}

const std::string Solver::getName()const
{
	return _name;
}

const std::string Solver::getVersion()const
{
	return _version;
}

const std::string Solver::getOptions()const
{
	return _options;
}
