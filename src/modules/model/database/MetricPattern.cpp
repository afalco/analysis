#include "src/modules/model/database/MetricPattern.h"

MetricPattern::MetricPattern():
	DatabaseTableMetrics(0),
	_associatedConfigFile(NULL),
	_destinationTable(),
	_name(),
	_pattern(),
	_print(),
	_default(),
	_preOperation(),
	_postOperation(),
	_type(),
	_isParameter(false),
	_isFraction(false)
{
	_new = true;
	_changed = true;
}

MetricPattern::MetricPattern(unsigned long int id):
	DatabaseTableMetrics(id),
	_associatedConfigFile(NULL),
	_destinationTable(),
	_name(),
	_pattern(),
	_print(),
	_default(),
	_preOperation(),
	_postOperation(),
	_type(),
	_isParameter(false),
	_isFraction(false)
{
	_new = false;
	_changed = false;

	// if experiment campaign can't be selected
	if (!DatabaseModule::selectMetricPattern(id, this))
		throw DatabaseTableException("MetricPattern", "id " + std::to_string(id) + " not found, unable to create metric pattern");
}

void MetricPattern::save()
{
	if (_changed) // if the structure has changed
	{
		if (_new) // if the class have never been saved
		{
			unsigned long int newId = DatabaseModule::createMetricPattern(this);
			if (newId != 0ul)
			{
				_id = newId;
				_new = false;
				_changed = false;
			}
		}
		else // if instance of class already exists in database
		{
			DatabaseModule::updateMetricPattern(this);
			_changed = false;
		}
	}
}

/* COMMON SETTERS */
void MetricPattern::setAssociatedConfigFile(ConfigFile *condifFile)
{
	_associatedConfigFile = condifFile;
	_changed = true;
}

void MetricPattern::setTable(DatabaseModule::Tables table)
{
	_destinationTable = table;
	_changed = true;
}

void MetricPattern::setName(QString name)
{
	_name = name;
	_changed = true;
}

void MetricPattern::setPattern(QString pattern)
{
	_pattern = pattern;
	_changed = true;
}

void MetricPattern::setPrint(QString print)
{
	_print = print;
	_changed = true;
}

void MetricPattern::setDefault(QString defaultText)
{
	_default = defaultText;
	_changed = true;
}

void MetricPattern::setPreOperation(QString preOperation)
{
	_preOperation = preOperation;
	_changed = true;
}

void MetricPattern::setPostOperation(QString postOperation)
{
	_postOperation = postOperation;
	_changed = true;
}

void MetricPattern::setType(QVariant::Type type)
{
	_type = type;
	_changed = true;
}

void MetricPattern::isParameter(bool isParameter)
{
	_isParameter = isParameter;
	_changed = true;
}

void MetricPattern::isFraction(bool isFraction)
{
	_isFraction = isFraction;
	_changed = true;
}

/* COMMON GETTERS */
ConfigFile *MetricPattern::getAssociatedConfigFile() const
{
	return _associatedConfigFile;
}

DatabaseModule::Tables MetricPattern::getTable() const
{
	return _destinationTable;
}

QString MetricPattern::getName() const
{
	return _name;
}

QString MetricPattern::getPattern() const
{
	return _pattern;
}

QString MetricPattern::getPrint() const
{
	return _print;
}

QString MetricPattern::getDefault() const
{
	return _default;
}

QString MetricPattern::getPreOperation() const
{
	return _preOperation;
}

QString MetricPattern::getPostOperation() const
{
	return _postOperation;
}

QVariant::Type MetricPattern::getType() const
{
	return _type;
}

bool MetricPattern::isParameter() const
{
	return _isParameter;
}

bool MetricPattern::isFraction() const
{
	return _isFraction;
}
