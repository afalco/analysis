#ifndef HARDWARE_H
#define HARDWARE_H

#include "src/modules/model/database/DatabaseTableMetrics.h"
class Experiment;//#include "model/database/Experiment.h"

#include <QVariant>

#include <string>

class Hardware : public DatabaseTableMetrics
{
private:
	Experiment *_associatedExperiment; // foreign key associated
public:
	explicit Hardware();
	explicit Hardware(unsigned long int);

	void save();

	/* COMMON SETTERS */
	void setExperiment(Experiment *);

	/* COMMON GETTERS */
	Experiment *getExperiment();
};

#endif // HARDWARE_H
