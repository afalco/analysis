#ifndef SOLVER_H
#define SOLVER_H

#include "src/modules/model/database/DatabaseTableMetrics.h"
class Experiment;//#include "model/database/Experiment.h"

#include <QVariant>

#include <string>

class Solver : public DatabaseTableMetrics
{
private:
	Experiment *_associatedExperiment;	// foreign key associated

	std::string _name,
		_version,
		_options;

public:
	explicit Solver();
	explicit Solver(unsigned long);

	void save();

	/* COMMON SETTERS */
	void setExperiment(Experiment *);
	void setName(std::string);
	void setVersion(std::string);
	void setOptions(std::string);

	/* COMMON GETTERS */
	Experiment *getExperiment();
	const std::string getName()const;
	const std::string getVersion()const;
	const std::string getOptions()const;
};

#endif // SOLVER_H
