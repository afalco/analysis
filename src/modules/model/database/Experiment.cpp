#include "src/modules/model/database/Experiment.h"
#include "src/modules/model/database/DatabaseModule.h"

Experiment::Experiment():
	DatabaseTableMetrics(0),
	_foreignKey(NULL),
	_name(), _outputs()
{
	_new = true;
	_changed = true;

	_matrix = NULL;
	_software = NULL;
	_hardware = NULL;
	_solver = NULL;
}

Experiment::Experiment(unsigned long int id):
	DatabaseTableMetrics(id),
	_foreignKey(NULL),
	_name(), _outputs()
{
	_new = false;
	_changed = false;
	_matrix = NULL;
	_software = NULL;
	_hardware = NULL;
	_solver = NULL;

	// if experiment can't be selected
	if (!DatabaseModule::selectExperiment(id, this))
		throw DatabaseTableException("Experiment", "id " + std::to_string(id) + " not found, unable to create experiment");
}

void Experiment::save()
{
	if (_changed) // if the structure has changed
	{
		// first save these tables to get they primary key in next query
		if (_matrix != NULL)
			_matrix->save();
		if (_software != NULL)
			_software->save();
		if (_hardware != NULL)
			_hardware->save();
		if (_solver != NULL)
			_solver->save();
		if (_new) // if the class have never been saved
		{
			unsigned long int newId = DatabaseModule::createExperiment(this);
			if (newId != 0ul)
			{
				_id = newId;
				_new = false;
				_changed = false;
			}
		}
		else // if instance of class already exists in database
		{
			DatabaseModule::updateExperiment(this);
			_changed = false;
		}
	}
}

bool Experiment::haveMatrix() const
{
	return _matrix != NULL;
}

bool Experiment::haveSoftware() const
{
	return _software != NULL;
}

bool Experiment::haveHardware() const
{
	return _hardware != NULL;
}

bool Experiment::haveSolver() const
{
	return _solver != NULL;
}

/* COMMON SETTERS */
void Experiment::setName(std::string name)
{
	_name = name;
	_changed = true;
}

void Experiment::setExperimentCampaign(ExperimentCampaign *experimentCampaign)
{
	_foreignKey = experimentCampaign;
}

void Experiment::setMatrix(Matrix *matrix)
{
	if (_matrix != NULL) _matrix->setExperiment(NULL);
	_matrix = matrix;
	_matrix->setExperiment(this);
}

void Experiment::setSoftware(Software *software)
{
	if (_software != NULL) _software->setExperiment(NULL);
	_software = software;
	_software->setExperiment(this);
}

void Experiment::setHardware(Hardware *hardware)
{
	if (_hardware != NULL) _hardware->setExperiment(NULL);
	_hardware = hardware;
	_hardware->setExperiment(this);
}

void Experiment::setSolver(Solver *solver)
{
	if (_solver != NULL) _solver->setExperiment(NULL);
	_solver = solver;
	_solver->setExperiment(this);
}

/* COMMON GETTERS */

const std::string Experiment::getName() const
{
	return _name;
}

ExperimentCampaign *Experiment::getExperimentCampaign() const
{
	return _foreignKey;
}

Matrix *Experiment::getMatrix()
{
	if (_matrix == NULL)
	{
		if (_new)
			_matrix = new Matrix;
		else
			_matrix = DatabaseModule::selectMatrix(this);
		_matrix->setExperiment(this);
	}
	return _matrix;
}

Software *Experiment::getSoftware()
{
	if (_software == NULL)
	{
		if (_new)
			_software = new Software;
		else
			_software = DatabaseModule::selectSoftware(this);
		_software->setExperiment(this);
	}
	return _software;
}

Hardware *Experiment::getHardware()
{
	if (_hardware == NULL)
	{
		if (_new)
			_hardware = new Hardware;
		else
			_hardware = DatabaseModule::selectHardware(this);
		_hardware->setExperiment(this);
	}
	return _hardware;
}

Solver *Experiment::getSolver()
{
	if (_solver == NULL)
	{
		if (_new)
			_solver = new Solver;
		else
			_solver = DatabaseModule::selectSolver(this);
		_solver->setExperiment(this);
	}
	return _solver;
}

std::vector<Output *> Experiment::getOutputs()
{
	return _outputs;
}

const Matrix *Experiment::getMatrix() const
{
	return _matrix;
}

const Software *Experiment::getSoftware() const
{
	return _software;
}

const Hardware *Experiment::getHardware() const
{
	return _hardware;
}

const Solver *Experiment::getSolver() const
{
	return _solver;
}
