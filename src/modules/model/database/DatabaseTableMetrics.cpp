#include "src/modules/model/database/DatabaseTableMetrics.h"

DatabaseTableMetrics::DatabaseTableMetrics(unsigned long int id):
	DatabaseTable(id),
	_metrics()
{}

const std::map<std::string, QVariant> DatabaseTableMetrics::getMetrics() const
{
	return _metrics;
}

bool DatabaseTableMetrics::addMetric(std::string name, QVariant value)
{

	bool newInserted = _metrics.insert(std::pair<std::string, QVariant>(name, value)).second;
	if (newInserted)
		_changed = true;
	return newInserted;
}

QVariant DatabaseTableMetrics::getMetric(const std::string name)
{
	std::map<std::string, QVariant>::iterator it = _metrics.find(name);
	if (it != _metrics.end())
		return static_cast<std::pair<std::string, QVariant> >(*it).second;
	else
		return QVariant(); // invalid variant
}
