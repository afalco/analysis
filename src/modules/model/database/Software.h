#ifndef SOFTWARE_H
#define SOFTWARE_H

#include "src/modules/model/database/DatabaseTableMetrics.h"
class Experiment;//#include "model/database/Experiment.h"

#include <QVariant>

#include <string>

class Software : public DatabaseTableMetrics
{
private:
	std::string _compiler,
		_libraries;
	Experiment *_associatedExperiment;

public:
	explicit Software();
	explicit Software(unsigned long int);

	void save();

	/* COMMON SETTERS */
	void setCompiler(std::string);
	void setLibraries(std::string);
	void setExperiment(Experiment *);

	/* COMMON GETTERS */
	const std::string getCompiler()const;
	const std::string getLibraries()const;
	Experiment *getExperiment();
};

#endif // SOFTWARE_H
