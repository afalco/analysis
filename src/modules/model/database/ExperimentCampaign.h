#ifndef EXPERIMENTCAMPAIGN_H
#define EXPERIMENTCAMPAIGN_H

#include "src/modules/model/database/DatabaseTable.h"
#include "src/modules/model/database/DatabaseTableException.h"

class Experiment; // in cause of loop inclusion with #include "model/database/Experiment.h"

#include <QString>
#include <QDateTime>

#include <vector>

class ExperimentCampaign : public DatabaseTable
{
private:
	QString _name;
	QDateTime _date;
	QString _description;
	std::vector<Experiment*> _experiments;

public:
	ExperimentCampaign();
	explicit ExperimentCampaign(unsigned long int);

	~ExperimentCampaign();

	void save();

	/* COMMON SETTERS */
	void setName(QString);
	void setDate(QDateTime);
	void setDescription(QString);

	/* COMMON GETTERS */
	QString getName() const;
	const QDateTime getDate() const;
	QString getDescription() const;
	const std::vector<Experiment*> getExperiments() const;


	void addExperiment(Experiment*);
};

#endif // EXPERIMENTCAMPAIGN_H
