#include "src/modules/model/database/ExperimentCampaign.h"
#include "src/modules/model/database/DatabaseModule.h"

ExperimentCampaign::ExperimentCampaign():
	DatabaseTable(0),
	_name(), _date(), _experiments()
{
	_new = true;
	_changed = true;
}

ExperimentCampaign::ExperimentCampaign(unsigned long int id):
	DatabaseTable(id),
	_name(), _date(), _experiments()
{
	_new = false;
	_changed = false;

	// if experiment campaign can't be selected
	if (!DatabaseModule::selectExperimentCampaign(id, this))
		throw DatabaseTableException("ExperimentCampaign", "id " + std::to_string(id) + " not found, unable to create experiment campaign");
}

ExperimentCampaign::~ExperimentCampaign()
{}

void ExperimentCampaign::save()
{
	if (_changed) // if the structure has changed
	{
		if (_new) // if the class have never been saved
		{
			unsigned long int newId = DatabaseModule::createExperimentCampaign(this);
			if (newId != 0ul)
			{
				_id = newId;
				_new = false;
				_changed = false;
				for (std::vector<Experiment*>::iterator
					it = _experiments.begin(),
					end = _experiments.end();
					it != end; ++it)
				{
					static_cast<Experiment*>(*it)->save();
				}
			}
		}
		else // if instance of class already exists in database
		{
			DatabaseModule::updateExperimentCampaign(this);
			_changed = false;
		}
	}
}

void ExperimentCampaign::addExperiment(Experiment* experiment)
{
	_experiments.push_back(experiment);
	experiment->setExperimentCampaign(this);
	_changed = true;
}

/* COMMON SETTERS */
void ExperimentCampaign::setName(QString name)
{
	_name = name;
	_changed = true;
}

void ExperimentCampaign::setDate(QDateTime date)
{
	_date = date;
	_changed = true;
}

void ExperimentCampaign::setDescription(QString description)
{
	_description = description;
	_changed = true;
}

/* COMMON GETTERS */
QString ExperimentCampaign::getName() const
{
	return _name;
}

const QDateTime ExperimentCampaign::getDate() const
{
	return _date;
}

QString ExperimentCampaign::getDescription() const
{
	return _description;
}

const std::vector<Experiment*> ExperimentCampaign::getExperiments() const
{
	return _experiments;
}
