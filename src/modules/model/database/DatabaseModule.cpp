#include "src/modules/model/database/DatabaseModule.h"
#include "src/modules/model/metricTable/MetricTable.h"
#include "src/modules/model/metricTable/MetricPlotStyle.h"
#include "src/modules/model/database/MetricPattern.h"
#include "src/modules/model/database/ConfigFile.h"

#include <QDebug>

bool DatabaseModule::_isConnected = false;
QSqlDatabase DatabaseModule::_database;

const QString DatabaseModule::_EXPERIMENT_CAMPAIGN = QString("experimentCampaign");
const QString DatabaseModule::_EXPERIMENT = QString("experiment");
const QString DatabaseModule::_MATRIX = QString("matrix");
const QString DatabaseModule::_SOFTWARE = QString("software");
const QString DatabaseModule::_HARDWARE = QString("hardware");
const QString DatabaseModule::_OUTPUT = QString("output");
const QString DatabaseModule::_SOLVER = QString("solver");
const QString DatabaseModule::_CONFIG_FILE = QString("configFile");
const QString DatabaseModule::_METRIC_PATTERN = QString("metricPattern");
const QString DatabaseModule::_INVALID = QString("");

// initialize data structures for queries, useful because of full static class implementation
std::vector<std::string> DatabaseModule::_createTables = std::vector<std::string>();
std::vector<std::string> DatabaseModule::_populateDatabase = std::vector<std::string>();

// related to experiment campaign
std::vector<std::string> DatabaseModule::_selectExperimentCampaign = std::vector<std::string>();
std::string DatabaseModule::_createExperimentCampaign = std::string();
std::string DatabaseModule::_updateExperimentCampaign = std::string();

// related to experiment
std::string DatabaseModule::_selectExperiment = std::string();
std::string DatabaseModule::_createExperiment = std::string();
std::string DatabaseModule::_updateExperiment = std::string();
std::string DatabaseModule::_deleteExperiment = std::string();
std::string DatabaseModule::_selectExperiments = std::string();

// related to matrix
std::string DatabaseModule::_selectMatrix = std::string();
std::string DatabaseModule::_createMatrix = std::string();
std::string DatabaseModule::_updateMatrix = std::string();

// related to software
std::string DatabaseModule::_selectSoftware = std::string();
std::string DatabaseModule::_createSoftware = std::string();
std::string DatabaseModule::_updateSoftware = std::string();

// related to hardware
std::string DatabaseModule::_selectHardware = std::string();
std::string DatabaseModule::_createHardware = std::string();
std::string DatabaseModule::_updateHardware = std::string();

// related to solver
std::string DatabaseModule::_selectSolver = std::string();
std::string DatabaseModule::_createSolver = std::string();
std::string DatabaseModule::_updateSolver = std::string();

// related to Output
std::string DatabaseModule::_selectOutput = std::string();
std::string DatabaseModule::_createOutput = std::string();
std::string DatabaseModule::_updateOutput = std::string();

// related to metric pattern
std::string DatabaseModule::_selectMetrics = std::string();
std::string DatabaseModule::_selectMetricInterval = std::string();

// related to all table
std::string DatabaseModule::_selectAllFromTable = std::string();
std::string DatabaseModule::_selectTableByExperimentId = std::string();

// NON STATIC METHODS
DatabaseModule::DatabaseModule()
{}

// STATIC METHODS
std::string DatabaseModule::SQLFileToString(const std::string filename)
{
	std::string content;
	std::ifstream file;

	try {
		file.open(filename);
	} catch(const std::ifstream::failure &e) {
		qInfo("%s", e.what());
	}

	if (file.is_open()) {
		content = std::string(
			std::istreambuf_iterator<char>(file),
			std::istreambuf_iterator<char>()
			);
		trim(content);
	}

	file.close();
	return content;
}

std::vector<std::string> DatabaseModule::SQLFileToVector(const std::string filename)
{
	std::vector<std::string> content;
	std::ifstream file;

	try {
		file.open(filename);
	} catch(const std::ifstream::failure &e) {
		//string message e.what();
	}

	if (file.is_open()) {
		for (std::string query; std::getline(file, query, ';');)
		{
			trim(query);
			if (query.size() != 0)
				content.push_back(query);
		}
	}
	file.close();
	return content;
}

void DatabaseModule::printQuery(QSqlQuery &query)
{
	if (true) // add test to check if query is a correct select query
	{
		unsigned int nbRecords = query.record().count();
		unsigned int i;
		while (query.next()) {
			for (i = 0; i < nbRecords; ++i) {
				qDebug() << query.value(i).toString();
			}
			qDebug() << "\n";
		}
	}
}

void DatabaseModule::printQueryError(const QSqlQuery &query)
{
	qInfo("Error in DatabaseModule:");
	qInfo("\tdatabase error: %s", query.lastError().databaseText().toStdString().data());
	qInfo("\tdriver error: %s", query.lastError().driverText().toStdString().data());
	qInfo("\tnative error: %s", query.lastError().nativeErrorCode().toStdString().data());
	qInfo("\ttext error: %s", query.lastError().text().toStdString().data());
}

bool DatabaseModule::connect(const QString name, DatabaseModule::Driver driver)
{
	switch (driver)
	{
	case SQLITE:
	{
		// make the connection with named database using sqlite driver
		_database = QSqlDatabase::addDatabase("QSQLITE");
		_database.setDatabaseName(name);

		// get sql code from files
		_createTables = SQLFileToVector("src/sql/createTables.sql");
		// temporary, for the test
		std::vector<std::string> modifiers = SQLFileToVector("src/sql/addOptionnalAttributes.sql");
		_createTables.insert(_createTables.end(), modifiers.begin(), modifiers.end());
		_populateDatabase = SQLFileToVector("src/sql/populate.sql");

		// related to experiment campaign
		_selectExperimentCampaign = SQLFileToVector("src/sql/selectExperimentCampaign.sql");
		_createExperimentCampaign = SQLFileToString("src/sql/createExperimentCampaign.sql");
		_updateExperimentCampaign = SQLFileToString("src/sql/updateExperimentCampaign.sql");

		// related to experiment
		_selectExperiment = SQLFileToString("src/sql/selectExperiment.sql");
		_createExperiment = SQLFileToString("src/sql/createExperiment.sql");
		_updateExperiment = SQLFileToString("src/sql/updateExperiment.sql");
		_deleteExperiment = SQLFileToString("src/sql/deleteExperiment.sql");
		_selectExperiments = SQLFileToString("src/sql/selectExperiments.sql");

		// related to matrix
		_selectMatrix = SQLFileToString("src/sql/selectMatrix.sql");
		_createMatrix = SQLFileToString("src/sql/createMatrix.sql");
		_updateMatrix = SQLFileToString("src/sql/updateMatrix.sql");

		// related to software
		_selectSoftware = SQLFileToString("src/sql/selectSoftware.sql");
		_createSoftware = SQLFileToString("src/sql/createSoftware.sql");
		_updateSoftware = SQLFileToString("src/sql/updateSoftware.sql");

		// related to hardware
		_selectHardware = SQLFileToString("src/sql/selectHardware.sql");
		_createHardware = SQLFileToString("src/sql/createHardware.sql");
		_updateHardware = SQLFileToString("src/sql/updateHardware.sql");

		// related to solver
		_selectSolver = SQLFileToString("src/sql/selectSolver.sql");
		_createSolver = SQLFileToString("src/sql/createSolver.sql");
		_updateSolver = SQLFileToString("src/sql/updateSolver.sql");

		// related to output
		_selectOutput = SQLFileToString("src/sql/selectOutput.sql");
		_createOutput = SQLFileToString("src/sql/createOutput.sql");
		_updateOutput = SQLFileToString("src/sql/updateOutput.sql");

		// related to metrics
		_selectMetrics = SQLFileToString("src/sql/selectMetrics.sql");
		_selectMetricInterval = SQLFileToString("src/sql/selectMetricInterval.sql");


		// related to all table
		_selectAllFromTable = SQLFileToString("src/sql/selectAllFromTable.sql");
		_selectTableByExperimentId = SQLFileToString("src/sql/selectTableByExperimentId.sql");
		break;
	}
	case MYSQL:
	{
		break;
	}
	}

	return _isConnected = _database.open();
}

void DatabaseModule::disconnect()
{
	_isConnected = false;
	_database.close();
}

bool DatabaseModule::isConnected()
{
	return _isConnected;
}

// ---------------- Methods related to table names ----------------
DatabaseModule::Tables DatabaseModule::tableNameToTable(const QString &tableName)
{
	Tables table = INVALID;
	if (tableName.compare(_EXPERIMENT_CAMPAIGN, Qt::CaseInsensitive) == 0)
		table = EXPERIMENT_CAMPAIGN;
	else if (tableName.compare(_EXPERIMENT, Qt::CaseInsensitive) == 0)
		table = EXPERIMENT;
	else if (tableName.compare(_MATRIX, Qt::CaseInsensitive) == 0)
		table = MATRIX;
	else if (tableName.compare(_SOFTWARE, Qt::CaseInsensitive) == 0)
		table = SOFTWARE;
	else if (tableName.compare(_HARDWARE, Qt::CaseInsensitive) == 0)
		table = HARDWARE;
	else if (tableName.compare(_OUTPUT, Qt::CaseInsensitive) == 0)
		table = OUTPUT;
	else if (tableName.compare(_SOLVER, Qt::CaseInsensitive) == 0)
		table = SOLVER;
	else if (tableName.compare(_CONFIG_FILE, Qt::CaseInsensitive) == 0)
		table = CONFIG_FILE;
	else if (tableName.compare(_METRIC_PATTERN, Qt::CaseInsensitive) == 0)
		table = METRIC_PATTERN;
	return table;
}

QString DatabaseModule::tableToTableName(Tables table)
{
	QString tableName;
	switch (table)
	{
	case EXPERIMENT_CAMPAIGN:
		tableName = _EXPERIMENT_CAMPAIGN;
		break;
	case EXPERIMENT:
		tableName = _EXPERIMENT;
		break;
	case MATRIX:
		tableName = _MATRIX;
		break;
	case SOFTWARE:
		tableName = _SOFTWARE;
		break;
	case HARDWARE:
		tableName = _HARDWARE;
		break;
	case OUTPUT:
		tableName = _SOFTWARE;
		break;
	case SOLVER:
		tableName = _SOLVER;
		break;
	case CONFIG_FILE:
		tableName = _CONFIG_FILE;
		break;
	case METRIC_PATTERN:
		tableName = _METRIC_PATTERN;
		break;
	case INVALID:
		tableName = _INVALID;
		break;
	default:
		tableName = "";
	}
	return tableName;
}

bool DatabaseModule::createDatabaseScheme()
{
	QSqlQuery createTables(_database);
	bool ok = true;
	for (std::vector<std::string>::iterator it = _createTables.begin();
		 it != _createTables.end();
		 ++it) {
		ok = createTables.exec(QString::fromStdString(static_cast<std::string>(*it)));
		if (!ok) {
			printQueryError(createTables);
		}
	}
	return ok;
}

// ---------------- Methods related to Experiment Campaign ----------------
bool DatabaseModule::selectExperimentCampaign(const unsigned long int id, ExperimentCampaign *experimentCampaign)
{
	QSqlQuery selectExperimentCampaign(_database);
	bool ok;

	selectExperimentCampaign.prepare(
		_selectExperimentCampaign.size() >= 1 ?
			QString::fromStdString(_selectExperimentCampaign.at(0)) : "");
	selectExperimentCampaign.bindValue(":id", QVariant::fromValue(id));
	ok = selectExperimentCampaign.exec();

	if (!ok) // an errors has occured
		printQueryError(selectExperimentCampaign);
	else
	{
		if (selectExperimentCampaign.next()) // if one experiment campaign has been selected
		{
			QSqlRecord expCampRec = selectExperimentCampaign.record();
			experimentCampaign->setName(expCampRec.value("name").toString());
			experimentCampaign->setDate(expCampRec.value("date").toDateTime());
			experimentCampaign->setDescription(expCampRec.value("description").toString());

			QSqlQuery selectAssociatedExperiments(_database);
			selectAssociatedExperiments.prepare(
				_selectExperimentCampaign.size() >= 2 ?
				QString::fromStdString(_selectExperimentCampaign.at(1)) : "");
			selectAssociatedExperiments.bindValue(":id", QVariant::fromValue(id));
			ok = selectAssociatedExperiments.exec();
			if (!ok)
				printQueryError(selectAssociatedExperiments);
			else
			{
				while (selectAssociatedExperiments.next())
				{
					Experiment *experiment = NULL;
					try {
						experiment = new Experiment(
							selectAssociatedExperiments.value("id").toULongLong());
					} catch(DatabaseTableException &dte) {
						// SOME ERROR REPORT
					}
					if (experiment != NULL)
						experimentCampaign->addExperiment(experiment);
				}
			}
		}
		else // no experiment selected
			ok = false;
	}
	return ok;
}

unsigned long int DatabaseModule::createExperimentCampaign(const ExperimentCampaign *experimentCampaign)
{
	QSqlQuery createExperimentCampaign(_database);
	bool ok;
	unsigned long int newId = 0;

	createExperimentCampaign.prepare(QString::fromStdString(_createExperimentCampaign));
	createExperimentCampaign.bindValue(":name", experimentCampaign->getName());
	createExperimentCampaign.bindValue(":date", experimentCampaign->getDate());
	createExperimentCampaign.bindValue(":description", experimentCampaign->getDescription());
	ok = createExperimentCampaign.exec();
	if (!ok)
		printQueryError(createExperimentCampaign);
	else
	{
		newId = createExperimentCampaign.lastInsertId().toULongLong();
	}
	return newId;
}

bool DatabaseModule::updateExperimentCampaign(const ExperimentCampaign *experimentCampaign)
{
	return false && experimentCampaign; // TODO
}

std::map<unsigned long int, QString> DatabaseModule::getAllExperimentCampaigns()
{
	std::map<unsigned long int, QString> experiments;
	QSqlQuery selectAllExperiments(_database);
	bool ok;

	selectAllExperiments.prepare("SELECT id, name FROM experimentCampaign");
	ok = selectAllExperiments.exec();
	if (!ok)
		printQueryError(selectAllExperiments);
	else
	{
		while (selectAllExperiments.next())
		{
			experiments.insert(std::pair<unsigned long int, QString>(
				selectAllExperiments.value("id").toULongLong(),
				selectAllExperiments.value("name").toString()));
		}

	}
	return experiments;
}

// ---------------- Methods related to Experiment ----------------
bool DatabaseModule::selectExperiment(const unsigned long int id, Experiment *experiment)
{
	QSqlQuery selectExperiment(_database);
	bool ok;

	std::string query = _selectAllFromTable;
	replace(query, ":table", "Experiment");
	selectExperiment.prepare(QString::fromStdString(query));
	selectExperiment.bindValue(":id", QVariant::fromValue(id));
	ok = selectExperiment.exec();
	if (!ok) // an error has occured
		printQueryError(selectExperiment);
	else
	{
		if (selectExperiment.next()) // if one experiment has been selected
		{
			QSqlRecord expRec = selectExperiment.record();

			for (int i = 0, nbExp = expRec.count(); i < nbExp; ++i)
			{
				QString fieldName = expRec.fieldName(i);
				QVariant value = expRec.value(i);
				if (value.isValid() && !value.isNull())
				{
					if (fieldName == "id")
					{
						// Do nothing
					}
					else if (fieldName == "name")
						experiment->setName(value.toString().toStdString());
					else if (fieldName == "idSolver")
						try {
							experiment->setSolver(new Solver(value.toULongLong()));
						} catch (DatabaseTableException &dte) {
							qInfo("%s", dte.what());
						}
					else if (fieldName == "idMatrix")
						try {
							experiment->setMatrix(new Matrix(value.toULongLong()));
						} catch (DatabaseTableException &dte) {
							qInfo("%s", dte.what());
						}
					else if (fieldName == "idHardware")
						try {
							experiment->setHardware(new Hardware(value.toULongLong()));
						} catch (DatabaseTableException &dte) {
							qInfo("%s", dte.what());
						}
					else if (fieldName == "idSoftware")
						try {
							experiment->setSoftware(new Software(value.toULongLong()));
						} catch (DatabaseTableException &dte) {
							qInfo("%s", dte.what());
						}
					else
						experiment->addMetric(fieldName.toStdString(), value);
				}
			}
		}
		else // no experiment selected
			ok = false;
	}
	return ok;
}

unsigned long int DatabaseModule::createExperiment(const Experiment *experiment)
{
	QSqlQuery createExperiment(_database);
	bool ok;
	unsigned long int id = 0;
	std::string metricNames, metricValues; // store the metrics names and values to insert after (sql format)

	metricNames = "idExperimentCampaign, name";
	metricValues = ":idExperimentCampaign, :name";
	if (experiment->haveMatrix())
	{
		metricNames += ", idMatrix";
		metricValues += ", :idMatrix";
	}
	if (experiment->haveSoftware())
	{
		metricNames += ", idSoftware";
		metricValues += ", :idSoftware";
	}
	if (experiment->haveHardware())
	{
		metricNames += ", idHardware";
		metricValues += ", :idHardware";
	}
	if (experiment->haveSolver())
	{
		metricNames += ", idSolver";
		metricValues += ", :idSolver";
	}

	const std::map<std::string, QVariant> metrics = experiment->getMetrics();
	for (std::map<std::string, QVariant>::const_iterator
		it = metrics.begin(), end = metrics.end();
		it != end; ++it) {
		std::pair<std::string, QVariant> metric =
			static_cast<std::pair<std::string, QVariant> >(*it);

		// build the list of optionnal metric names to add to the sql statement
		metricNames += ", " + metric.first;
		// build the list of optionnel metric value to add to the sql statement
		// ":" added at begin of each metric to replace them by it's value later (later function correctly format metrics)
		metricValues += ", :" + metric.first;
	}

	std::string createExperimentQuery = _createExperiment;
	replace(createExperimentQuery, ":metricNames", metricNames);
	replace(createExperimentQuery, ":metricValues", metricValues);
	createExperiment.prepare(QString::fromStdString(createExperimentQuery));
	createExperiment.bindValue(":idExperimentCampaign",
		QVariant::fromValue<unsigned long long>(experiment->getExperimentCampaign()->getId()));
	createExperiment.bindValue(":name",
		QString::fromStdString(experiment->getName()));
	if (experiment->haveMatrix())
		createExperiment.bindValue(":idMatrix",
			QVariant::fromValue<unsigned long long>(experiment->getMatrix()->getId()));
	if (experiment->haveSoftware())
		createExperiment.bindValue(":idSoftware",
			QVariant::fromValue<unsigned long long>(experiment->getSoftware()->getId()));
	if (experiment->haveHardware())
		createExperiment.bindValue(":idHardware",
			QVariant::fromValue<unsigned long long>(experiment->getHardware()->getId()));
	if (experiment->haveSolver())
		createExperiment.bindValue(":idSolver",
			QVariant::fromValue<unsigned long long>(experiment->getSolver()->getId()));

	for (std::map<std::string, QVariant>::const_iterator
		it = metrics.begin(), end = metrics.end();
		it != end; ++it) {
		std::pair<std::string, QVariant> metric =
			static_cast<std::pair<std::string, QVariant> >(*it);
		createExperiment.bindValue(
			QString::fromStdString(":" + metric.first),
			metric.second);
	}

	ok = createExperiment.exec();
	if (!ok) // an errors has occured
		printQueryError(createExperiment);
	else
		id = createExperiment.lastInsertId().toULongLong(); // get id of inserted item

	return id;
}

bool DatabaseModule::updateExperiment(const Experiment *experiment)
{
	QSqlQuery updateExperiment(_database);
	bool ok;

	updateExperiment.prepare(QString::fromStdString(_updateExperiment));
	updateExperiment.bindValue(":id", QVariant::fromValue(experiment->getId()));
	updateExperiment.bindValue(":name", QVariant::fromValue(QString::fromStdString(experiment->getName())));

	ok = updateExperiment.exec();
	if (!ok) // an errors has occured
		printQueryError(updateExperiment);

	return ok;
}

std::vector<Experiment*> DatabaseModule::selectExperimentsFromExperimentCampaignId(unsigned long int id)
{
	QSqlQuery getExperiments(_database);
	bool ok;
	std::vector<Experiment*> ve;
	getExperiments.prepare(QString::fromStdString(_selectExperiments));
	getExperiments.bindValue(":id", QVariant::fromValue(id));
	ok = getExperiments.exec();
	if (!ok)
		printQueryError(getExperiments);
	else {
		while (getExperiments.next()) {
			QSqlRecord expRecord = getExperiments.record();
			Experiment *e = new Experiment(expRecord.value("experiment.id").toLongLong());
			e->setName(expRecord.value("name").toString().toStdString());

			// i = 2 cause name and date records already got
			for (int i = 2; i < expRecord.count(); ++i) {
				QVariant value = expRecord.value(i);
				if (value.isValid() && !value.isNull())
					e->addMetric(expRecord.fieldName(i).toStdString(), value);
			}
			ve.push_back(e);
		}
	}
	return ve;
}

// ---------------- Methods related to Matrix ----------------
bool DatabaseModule::selectMatrix(unsigned long int id, Matrix *matrix)
{
	QSqlQuery selectMatrix(_database);
	bool ok;


	std::string query = _selectAllFromTable;
	replace(query, ":table", "Matrix");
	selectMatrix.prepare(QString::fromStdString(query));
	selectMatrix.bindValue(":id", QVariant::fromValue(id));
	ok = selectMatrix.exec();
	if (!ok) // an error has occured
		printQueryError(selectMatrix);
	else
	{
		if (selectMatrix.next())
		{
			QSqlRecord record = selectMatrix.record();
			for (int i = 0, nbRec = record.count(); i < nbRec; ++i)
			{
				QString fieldName = record.fieldName(i);
				QVariant value = record.value(i);
				if (value.isValid() && !value.isNull())
				{
					if (fieldName == "id")
					{
						// Do nothing
					}
					else if (fieldName == "name")
						matrix->setName(value.toString().toStdString());
					else
						matrix->addMetric(fieldName.toStdString(), value);
				}
			}
		}
		else
			ok = false;
	}
	return ok;
}

Matrix *DatabaseModule::selectMatrix(const Experiment *experiment)
{
	QSqlQuery selectMatrix(_database);
	bool ok;
	Matrix *matrix = NULL;

	std::string query = _selectTableByExperimentId;
	replace(query, ":table", "Matrix", true);
	selectMatrix.prepare(QString::fromStdString(query));
	selectMatrix.bindValue(":id", QVariant::fromValue(experiment->getId()));
	ok = selectMatrix.exec();
	if (!ok) // an error has occured
		printQueryError(selectMatrix);
	else
	{
		if (selectMatrix.next()) // not need of try catch cause exception have to be handled in method caller, not here
			matrix = new Matrix(selectMatrix.value("matrix.id").toULongLong());
		else
			throw DatabaseTableException("Matrix", "foreign key " + std::to_string(experiment->getId()) + " not found, unable to create matrix");
	}
	return matrix;
}

unsigned long int DatabaseModule::createMatrix(const Matrix *matrix)
{
	QSqlQuery createMatrix(_database);
	bool ok;
	unsigned long int id = 0;
	std::string metricNames, metricValues; // store the metrics names and values to insert after (sql format)

	metricNames = "name";
	metricValues = ":name";

	const std::map<std::string, QVariant> metrics = matrix->getMetrics();
	for (std::map<std::string, QVariant>::const_iterator
		it = metrics.begin(), end = metrics.end();
		it != end; ++it) {
		std::pair<std::string, QVariant> metric =
			static_cast<std::pair<std::string, QVariant> >(*it);
		metricNames += ", " + metric.first;
		metricValues += ", :" + metric.first;
	}

	std::string createMatrixQuery = _createMatrix;
	replace(createMatrixQuery, ":metricNames", metricNames);
	replace(createMatrixQuery, ":metricValues", metricValues);

	createMatrix.prepare(QString::fromStdString(createMatrixQuery));
	createMatrix.bindValue(":name", QString::fromStdString(matrix->getName()));

	for (std::map<std::string, QVariant>::const_iterator
		it = metrics.begin(), end = metrics.end();
		it != end; ++it) {
		std::pair<std::string, QVariant> metric =
			static_cast<std::pair<std::string, QVariant> >(*it);
		createMatrix.bindValue(
			QString::fromStdString(":" + metric.first),
			metric.second);
	}

	ok = createMatrix.exec();
	if (!ok) // an errors has occured
		printQueryError(createMatrix);
	else
		id = createMatrix.lastInsertId().toULongLong(); // get id of inserted item

	return id;
}

bool DatabaseModule::updateMatrix(const Matrix *matrix)
{
	return false && matrix;
}

// ---------------- Methods related to Software ----------------
bool DatabaseModule::selectSoftware(unsigned long int id, Software *software)
{
	QSqlQuery selectSoftware(_database);
	bool ok;

	std::string query = _selectAllFromTable;
	replace(query, ":table", "Software");
	selectSoftware.prepare(QString::fromStdString(query));
	selectSoftware.bindValue(":id", QVariant::fromValue(id));
	ok = selectSoftware.exec();
	if (!ok)
		printQueryError(selectSoftware);
	else
	{
		if (selectSoftware.next())
		{
			QSqlRecord record = selectSoftware.record();
			for (int i = 0, size = record.count(); i < size; ++i)
			{
				QString fieldName = record.fieldName(i);
				QVariant value = record.value(i);
				if (value.isValid() && !value.isNull())
				{
					if (fieldName == "id")
					{
						// Do nothing
					}
					else if (fieldName == "compiler")
						software->setCompiler(value.toString().toStdString());
					else if (fieldName == "libraries"
							 "")
						software->setLibraries(value.toString().toStdString());
					else
						software->addMetric(fieldName.toStdString(), value);
				}
			}
		}
		else // no row selected
			ok = false;
	}
	return ok;
}

Software *DatabaseModule::selectSoftware(const Experiment *experiment)
{
	QSqlQuery selectSoftware(_database);
	bool ok;
	Software *solver = NULL;

	std::string query = _selectTableByExperimentId;
	replace(query, ":table", "Software", true);
	selectSoftware.prepare(QString::fromStdString(query));
	selectSoftware.bindValue(":id", QVariant::fromValue(experiment->getId()));
	ok = selectSoftware.exec();
	if (!ok) // an error has occured
		printQueryError(selectSoftware);
	else
	{
		if (selectSoftware.next()) // not need of try catch cause exception have to be handled in method caller, not here
			solver = new Software(selectSoftware.value("Software.id").toULongLong());
		else
			throw DatabaseTableException("Software", "foreign key " + std::to_string(experiment->getId()) + " not found, unable to create software");
	}
	return solver;
}

unsigned long int DatabaseModule::createSoftware(const Software *software)
{
	QSqlQuery createSoftware(_database);
	bool ok;
	unsigned long int id = 0;
	std::string metricNames, metricValues; // store the metrics names and values to insert after (sql format)

	metricNames = "compiler, libraries";
	metricValues = ":compiler, :libraries";

	const std::map<std::string, QVariant> metrics = software->getMetrics();
	for (std::map<std::string, QVariant>::const_iterator
		it = metrics.begin(), end = metrics.end();
		it != end; ++it) {
		std::pair<std::string, QVariant> metric =
			static_cast<std::pair<std::string, QVariant> >(*it);
		metricNames += ", " + metric.first;
		metricValues += ", :" + metric.first;
	}

	std::string createSoftwareQuery = _createSoftware;
	replace(createSoftwareQuery, ":metricNames", metricNames);
	replace(createSoftwareQuery, ":metricValues", metricValues);

	createSoftware.prepare(QString::fromStdString(createSoftwareQuery));
	createSoftware.bindValue(":compiler", QString::fromStdString(software->getCompiler()));
	createSoftware.bindValue(":libraries", QString::fromStdString(software->getLibraries()));

	for (std::map<std::string, QVariant>::const_iterator
		it = metrics.begin(), end = metrics.end();
		it != end; ++it) {
		std::pair<std::string, QVariant> metric =
			static_cast<std::pair<std::string, QVariant> >(*it);
		createSoftware.bindValue(
			QString::fromStdString(":" + metric.first),
			metric.second);
	}

	ok = createSoftware.exec();
	if (!ok) // an errors has occured
		printQueryError(createSoftware);
	else
		id = createSoftware.lastInsertId().toULongLong(); // get id of inserted item

	return id;
}

bool DatabaseModule::updateSoftware(const Software *software)
{
	return false && software;
}

// ---------------- Methods related to Hardware ----------------
bool DatabaseModule::selectHardware(unsigned long int id, Hardware *hardware)
{
	QSqlQuery selectHardware(_database);
	bool ok;

	std::string query = _selectAllFromTable;
	replace(query, ":table", "Hardware");
	selectHardware.prepare(QString::fromStdString(query));
	selectHardware.bindValue(":id", QVariant::fromValue(id));
	ok = selectHardware.exec();
	if (!ok)
		printQueryError(selectHardware);
	else
	{
		if (selectHardware.next())
		{
			QSqlRecord record = selectHardware.record();
			for (int i = 0, size = record.count(); i < size; ++i)
			{
				QString fieldName = record.fieldName(i);
				QVariant value = record.value(i);
				if (value.isValid() && !value.isNull())
				{
					if (fieldName == "id")
					{
						// Do nothing
					}
					else
						hardware->addMetric(fieldName.toStdString(), value);
				}
			}
		}
		else // no row selected
			ok = false;
	}
	return ok;
}

Hardware *DatabaseModule::selectHardware(const Experiment *experiment)
{
	QSqlQuery selectHardware(_database);
	bool ok;
	Hardware *hardware = NULL;

	std::string query = _selectTableByExperimentId;
	replace(query, ":table", "Hardware", true);
	selectHardware.prepare(QString::fromStdString(query));
	selectHardware.bindValue(":id", QVariant::fromValue(experiment->getId()));
	ok = selectHardware.exec();
	if (!ok) // an error has occured
		printQueryError(selectHardware);
	else
	{
		if (selectHardware.next()) // not need of try catch cause exception have to be handled in method caller, not here
			hardware = new Hardware(selectHardware.value("hardware.id").toULongLong());
		else
			throw DatabaseTableException("Hardware", "foreign key " + std::to_string(experiment->getId()) + " not found, unable to create hardware");
	}
	return hardware;
}

unsigned long int DatabaseModule::createHardware(const Hardware *hardware)
{
	QSqlQuery createHardware(_database);
	bool ok;
	unsigned long int id = 0;
	std::string metricNames, metricValues; // store the metrics names and values to insert after (sql format)

	const std::map<std::string, QVariant> metrics = hardware->getMetrics();
	for (std::map<std::string, QVariant>::const_iterator
		it = metrics.begin(), end = metrics.end();
		it != end; ++it) {
		std::pair<std::string, QVariant> metric =
			static_cast<std::pair<std::string, QVariant> >(*it);
		metricNames += ", " + metric.first;
		metricValues += ", :" + metric.first;
	}

	// because no other values than metrics
	metricNames[0] = ' ';
	metricValues[0] = ' ';

	std::string createHardwareQuery = _createHardware;
	replace(createHardwareQuery, ":metricNames", metricNames);
	replace(createHardwareQuery, ":metricValues", metricValues);

	createHardware.prepare(QString::fromStdString(createHardwareQuery));

	for (std::map<std::string, QVariant>::const_iterator
		it = metrics.begin(), end = metrics.end();
		it != end; ++it) {
		std::pair<std::string, QVariant> metric =
			static_cast<std::pair<std::string, QVariant> >(*it);
		createHardware.bindValue(
			QString::fromStdString(":" + metric.first),
			metric.second);
	}

	ok = createHardware.exec();
	if (!ok) // an errors has occured
		printQueryError(createHardware);
	else
		id = createHardware.lastInsertId().toULongLong(); // get id of inserted item

	return id;
}

bool DatabaseModule::updateHardware(const Hardware *hardware)
{
	return false && hardware;
}

// ---------------- Methods related to Solver ----------------
bool DatabaseModule::selectSolver(unsigned long int id, Solver *solver)
{
	QSqlQuery selectSolver(_database);
	bool ok;

	std::string query = _selectAllFromTable;
	replace(query, ":table", "Solver");
	selectSolver.prepare(QString::fromStdString(query));
	selectSolver.bindValue(":id", QVariant::fromValue(id));
	ok = selectSolver.exec();
	if (!ok)
		printQueryError(selectSolver);
	else
	{
		if (selectSolver.next())
		{
			QSqlRecord record = selectSolver.record();
			for (int i = 0, size = record.count(); i < size; ++i)
			{
				QString fieldName = record.fieldName(i);
				QVariant value = record.value(i);
				if (value.isValid() && !value.isNull())
				{
					if (fieldName == "id")
					{
						// Do nothing
					}
					else if (fieldName == "name")
						solver->setName(value.toString().toStdString());
					else if (fieldName == "options")
						solver->setOptions(value.toString().toStdString());
					else if (fieldName == "version")
						solver->setVersion(value.toString().toStdString());
					else
						solver->addMetric(fieldName.toStdString(), value);
				}
			}
		}
		else // no row selected
			ok = false;
	}
	return ok;
}

Solver *DatabaseModule::selectSolver(const Experiment *experiment)
{
	QSqlQuery selectSolver(_database);
	bool ok;
	Solver *solver = NULL;

	std::string query = _selectTableByExperimentId;
	replace(query, ":table", "Solver", true);
	selectSolver.prepare(QString::fromStdString(query));
	selectSolver.bindValue(":id", QVariant::fromValue(experiment->getId()));
	ok = selectSolver.exec();
	if (!ok) // an error has occured
		printQueryError(selectSolver);
	else
	{
		if (selectSolver.next()) // not need of try catch cause exception have to be handled in method caller, not here
			solver = new Solver(selectSolver.value("Solver.id").toULongLong());
		else
			throw DatabaseTableException("Solver", "foreign key " + std::to_string(experiment->getId()) + " not found, unable to create solver");
	}
	return solver;
}

unsigned long int DatabaseModule::createSolver(const Solver *solver)
{
	QSqlQuery createSolver(_database);
	bool ok;
	unsigned long int id = 0;
	std::string metricNames, metricValues; // store the metrics names and values to insert after (sql format)

	metricNames = "name, options, version";
	metricValues = ":name, :options, :version";

	const std::map<std::string, QVariant> metrics = solver->getMetrics();
	for (std::map<std::string, QVariant>::const_iterator
		it = metrics.begin(), end = metrics.end();
		it != end; ++it) {
		std::pair<std::string, QVariant> metric =
			static_cast<std::pair<std::string, QVariant> >(*it);
		metricNames += ", " + metric.first;
		metricValues += ", :" + metric.first;
	}

	std::string createSolverQuery = _createSolver;
	replace(createSolverQuery, ":metricNames", metricNames);
	replace(createSolverQuery, ":metricValues", metricValues);

	createSolver.prepare(QString::fromStdString(createSolverQuery));
	createSolver.bindValue(":name", QString::fromStdString(solver->getName()));
	createSolver.bindValue(":options", QString::fromStdString(solver->getOptions()));
	createSolver.bindValue(":version", QString::fromStdString(solver->getVersion()));

	for (std::map<std::string, QVariant>::const_iterator
		it = metrics.begin(), end = metrics.end();
		it != end; ++it) {
		std::pair<std::string, QVariant> metric =
			static_cast<std::pair<std::string, QVariant> >(*it);
		createSolver.bindValue(
			QString::fromStdString(":" + metric.first),
			metric.second);
	}

	ok = createSolver.exec();
	if (!ok) // an errors has occured
		printQueryError(createSolver);
	else
		id = createSolver.lastInsertId().toULongLong(); // get id of inserted item

	return id;
}

bool DatabaseModule::updateSolver(const Solver *solver)
{
	return false && solver;
}

// ---------------- Methods related to Output ----------------
/*bool selectOutput(const Experiment *, Output *);
unsigned long int createOutput(const Output *);
bool updateOutput(const Output *);*/

// ---------------- Methods related to MetricPattern ----------------
void DatabaseModule::bindQueryValues(QSqlQuery &query, const MetricPattern *metricPattern)
{
	query.bindValue(":destinationTable",
		tableToTableName(metricPattern->getTable()));
	query.bindValue(":name",
		metricPattern->getName());
	query.bindValue(":pattern",
		metricPattern->getPattern());
	query.bindValue(":print",
		metricPattern->getPrint());
	query.bindValue(":defaultVal",
		metricPattern->getDefault());
	query.bindValue(":preOperation",
		metricPattern->getPreOperation());
	query.bindValue(":postOperation",
		metricPattern->getPostOperation());
	query.bindValue(":type",
		qVariantTypeToTypename(
			metricPattern->getType()));
	query.bindValue(":isParameter",
		metricPattern->isParameter());
	query.bindValue(":isFraction",
		metricPattern->isFraction());
	query.bindValue(":idConfigFile",
		QVariant::fromValue<unsigned long long>(
			metricPattern->getAssociatedConfigFile()->getId()));
}

bool DatabaseModule::selectMetricPattern(const unsigned long int id, MetricPattern *metricPattern)
{
	QSqlQuery selectMetricPattern(_database);
	bool ok;

	selectMetricPattern.prepare(QString::fromStdString(
		"SELECT destinationTable, name, pattern, print, defaultVal, preOperation, postOperation, type, isParameter, isFraction FROM metricPattern WHERE id = :id;"));
	selectMetricPattern.bindValue(":id", QVariant::fromValue<unsigned long long>(id));
	ok = selectMetricPattern.exec();
	if (!ok) // an errors has occured
		printQueryError(selectMetricPattern);
	else
	{
		if (selectMetricPattern.next())
		{
			metricPattern->setTable(tableNameToTable(
				selectMetricPattern.value("destinationTable").toString()));
			metricPattern->setName(
				selectMetricPattern.value("name").toString());
			metricPattern->setPattern(
				selectMetricPattern.value("pattern").toString());
			metricPattern->setPrint(
				selectMetricPattern.value("print").toString());
			metricPattern->setDefault(
				selectMetricPattern.value("defaultVal").toString());
			metricPattern->setPreOperation(
				selectMetricPattern.value("preOperation").toString());
			metricPattern->setPostOperation(
				selectMetricPattern.value("postOperation").toString());
			metricPattern->setType(typenameToQVariantType(
				selectMetricPattern.value("type").toString()));
			metricPattern->isParameter(
				selectMetricPattern.value("isParameter")
					.toBool());
			metricPattern->isFraction(
				selectMetricPattern.value("isFraction")
					.toBool());
		}
	}
	return ok;
}

std::vector<MetricPattern*> DatabaseModule::selectMetricPatternsOfConfigFile(const ConfigFile *configFile)
{
	QSqlQuery selectMetricPatterns(_database);
	bool ok;
	std::vector<MetricPattern*> metricPatterns;

	selectMetricPatterns.prepare(QString::fromStdString("SELECT id FROM metricPattern WHERE idConfigFile = :idConfigFile;"));
	selectMetricPatterns.bindValue(":idConfigFile",
		QVariant::fromValue<unsigned long long>(configFile->getId()));

	ok = selectMetricPatterns.exec();
	if (!ok)
		printQueryError(selectMetricPatterns);
	else
	{
		while (selectMetricPatterns.next())
		{
			unsigned long idMetricPatren = selectMetricPatterns.value("id").toULongLong();

			try {
				MetricPattern *metricPattern = new MetricPattern(idMetricPatren);
				metricPatterns.push_back(metricPattern);
			} catch (DatabaseTableException &dte) {
				qInfo("%s", dte.what());
			}
		}
	}

	return metricPatterns;
}

unsigned long int DatabaseModule::createMetricPattern(const MetricPattern *metricPattern)
{
	QSqlQuery foundMetricPattern(_database),
		createMetricPattern(_database),
		insertNewMetric(_database);	// inserts the new metric in corresponding table
	bool ok;
	unsigned long int newId = 0;
	std::string addMetricQuery = "ALTER TABLE :table ADD COLUMN :newMetric;";
	// search an existing metring pattern with same informations
	foundMetricPattern.prepare(QString::fromStdString("SELECT id FROM metricPattern WHERE"
		" destinationTable = :destinationTable AND name = :name AND pattern = :pattern AND print = :print AND defaultVal = :defaultVal AND preOperation = :preOperation AND postOperation = :postOperation AND type = :type AND isParameter = :isParameter AND isFraction = :isFraction AND idConfigFile = :idConfigFile;"));
	bindQueryValues(foundMetricPattern, metricPattern);
	ok = foundMetricPattern.exec();
	if (!ok)
		printQueryError(foundMetricPattern);
	else
	{
		if (!foundMetricPattern.next())
		{
			// if no same metric pattern exists
			// insert current metric pattern
			createMetricPattern.prepare(QString::fromStdString(
				"INSERT INTO metricPattern (destinationTable, name, pattern, print, defaultVal, preOperation, postOperation, type, isParameter, isFraction, idConfigFile)"
				" VALUES (:destinationTable, :name, :pattern, :print, :defaultVal, :preOperation, :postOperation, :type, :isParameter, :isFraction, :idConfigFile);"));
			bindQueryValues(createMetricPattern, metricPattern);

			ok = createMetricPattern.exec();
			if (!ok)
				printQueryError(createMetricPattern);
			else
			{
				newId = createMetricPattern.lastInsertId().toULongLong();

				// add metric in corresponding table
				replace(addMetricQuery, ":table", tableToTableName(metricPattern->getTable()).toStdString());
				replace(addMetricQuery, ":newMetric", metricPattern->getName().toStdString());
				ok = insertNewMetric.exec(QString::fromStdString(addMetricQuery));
				if (!ok)
					printQueryError(insertNewMetric);
			}
		}
	}
	return newId;
}

bool DatabaseModule::updateMetricPattern(const MetricPattern *metricPattern)
{
	return false && metricPattern; // TODO
}

// ---------------- Methods related to Config File ----------------
bool DatabaseModule::selectConfigFile(const unsigned long int id, ConfigFile *configFile)
{
	QSqlQuery selectConfigFile(_database);
	bool ok;

	selectConfigFile.prepare(QString::fromStdString("SELECT name, path FROM configFile WHERE id = :id;"));
	selectConfigFile.bindValue(":id", QVariant::fromValue<unsigned long long>(id));

	ok = selectConfigFile.exec();
	if (!ok)
		printQueryError(selectConfigFile);
	else
	{
		if (selectConfigFile.next())
		{
			configFile->setName(
				selectConfigFile.value("name").toString());
			configFile->setPath(
				selectConfigFile.value("path").toString());

			configFile->addMetricPatterns(
				DatabaseModule::selectMetricPatternsOfConfigFile(configFile));
		}
		else
			ok = false;
	}

	return ok;
}

unsigned long int DatabaseModule::createConfigFile(const ConfigFile *configFile)
{
	QSqlQuery createConfigFile(_database);
	bool ok;
	unsigned long int id = 0;

	createConfigFile.prepare(QString::fromStdString("INSERT INTO configFile (name, path) VALUES (:name, :path);"));
	createConfigFile.bindValue(":name", configFile->getName());
	createConfigFile.bindValue(":path", configFile->getPath());

	ok = createConfigFile.exec();
	if (!ok)
		printQueryError(createConfigFile);
	else
		id = createConfigFile.lastInsertId().toULongLong();

	return id;
}

bool DatabaseModule::updateConfigFile(const ConfigFile *configFile)
{
	return false && configFile;
}

std::map<unsigned long int, QString> DatabaseModule::getAllConfigFile()
{
	QSqlQuery selectConfigFiles(_database);
	bool ok;
	std::map<unsigned long int, QString> configFiles;

	selectConfigFiles.prepare(QString::fromStdString("SELECT id, name FROM configFile;"));
	ok = selectConfigFiles.exec();
	if (!ok)
		printQueryError(selectConfigFiles);
	else
	{
		while (selectConfigFiles.next())
		{
			configFiles.insert(
				std::pair<unsigned long int, QString>(
					selectConfigFiles.value("id").toULongLong(),
					selectConfigFiles.value("name").toString()));
		}
	}

	return configFiles;
}

// ---------------- Methods related to all tables ----------------
bool DatabaseModule::populateWithTestData()
{
	QSqlQuery insertFruts(_database);
	bool ok;

	for (std::vector<std::string>::iterator it = _populateDatabase.begin();
		 it != _populateDatabase.end();
		 ++it) {
		ok = insertFruts.exec(QString::fromStdString(static_cast<std::string>(*it)));
		if (!ok)
			printQueryError(insertFruts);
	}
	return ok;
}

std::map<std::string, MetricSelector*> DatabaseModule::selectAllMetrics()
{
	QSqlQuery selectMetrics(_database),
			selectMetricRange(_database);
	std::map<std::string, MetricSelector*> metrics;
	bool ok;

	selectMetrics.prepare(QString::fromStdString(_selectMetrics));
	ok = selectMetrics.exec();
	if (!ok)
		printQueryError(selectMetrics);
	else
	{
		while (selectMetrics.next())
		{
			std::string destinationTable = selectMetrics.value("destinationTable").toString().toStdString(),
				name = selectMetrics.value("name").toString().toStdString();
			MetricSelector *metricSelector = new MetricSelector(
				typenameToQVariantType(selectMetrics.value("type").toString()),
				tableNameToTable(selectMetrics.value("destinationTable").toString()),
				QString::fromStdString(name));
			metricSelector->isAbscissa(selectMetrics.value("isParameter").toBool());
			metrics.insert(std::pair<std::string, MetricSelector*>(
				destinationTable + "." + name,
				metricSelector
				));

			selectMetricRange.prepare(QString::fromStdString("SELECT MIN("+name+"), MAX("+name+") FROM "+destinationTable+";"));
			if (selectMetricRange.exec() && selectMetricRange.next())
			{
				metricSelector->setMin(selectMetricRange.value(0));
				metricSelector->setMax(selectMetricRange.value(1));
				metricSelector->setBegin(selectMetricRange.value(0));
				metricSelector->setEnd(selectMetricRange.value(1));
			}
		}
	}
	return metrics;
}

std::vector<QVariant> DatabaseModule::getValuesFromMetricSelector(MetricSelector *selector){

	QSqlQuery getMetricInterval(_database);
	bool ok;
	std::vector<QVariant> vector;
	std::string selectQuery = _selectMetricInterval;
	QString metric = selector->getMetricName();
	replace(selectQuery, ":metric", metric.toStdString(), true);
	replace(selectQuery, ":table",
		tableToTableName(selector->getDatabaseTableMetrics())
			.toStdString(), true);
	getMetricInterval.prepare(QString::fromStdString(selectQuery));
	getMetricInterval.bindValue(":begin", selector->getBegin());
	getMetricInterval.bindValue(":end", selector->getEnd());
	ok = getMetricInterval.exec();
	if (!ok)
		printQueryError(getMetricInterval);
	else
	{
		while (getMetricInterval.next())
			vector.push_back(getMetricInterval.value(metric));
	}
	return vector;
}
void DatabaseModule::selectAbscissaOrdinateMetricRange(
		const MetricTable *currentMetricSelection,
		std::map<QString, MetricPlotStyle*>& abscissaMetrics,
		std::map<QString, MetricPlotStyle*>& ordinateMetrics)
{
	QSqlQuery selectMetricRange(_database);
	bool ok;

	std::string selectMetricRangeQuery;
	for (std::multimap<QString, MetricContainer*>::const_iterator
			it = currentMetricSelection->firstItemIterator(),
			last = currentMetricSelection->lastItemIterator();
		it != last; ++it)
	{
		std::pair<QString, MetricContainer*> metric = static_cast<std::pair<QString, MetricContainer*> >(*it);
		MetricSelector *ms = static_cast<MetricSelector*>(metric.second);
		if (ms->getBegin() != ms->getEnd()) // begin and end are different
		{
			selectMetricRangeQuery = _selectMetricInterval;
			replace(selectMetricRangeQuery, ":table", metric.first.split('.')[0].toStdString());
			replace(selectMetricRangeQuery, ":metric", metric.first.split('.')[1].toStdString());
			selectMetricRange.prepare(QString::fromStdString(selectMetricRangeQuery));
			selectMetricRange.bindValue(":begin", ms->getBegin());
			selectMetricRange.bindValue(":end", ms->getEnd());
			ok = selectMetricRange.exec();
			if (!ok)
				printQueryError(selectMetricRange);
			else
			{
				PlotStyleChooser_p pscp(new PlotStyleChooser);
				QVariant var = QVariant::fromValue(pscp);
				MetricPlotStyle *mps = new MetricPlotStyle(var.type(), ms->getDatabaseTableMetrics(), ms->getMetricName());
				//ms->setBegin("<STYLE SELECTOR>");
				//ms->setBegin(var);
				std::pair<QString, MetricPlotStyle*> value(metric.first, mps);

				if (ms->isAbscissa())
					abscissaMetrics.insert(value);
				else
					ordinateMetrics.insert(value);
			}
		}
	}
}
