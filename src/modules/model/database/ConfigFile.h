#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include "src/modules/model/database/DatabaseTable.h"
#include "src/modules/model/database/MetricPattern.h"

#include <vector>

class ConfigFile : public DatabaseTable
{
private:
	QString _name,
		_path;
	std::vector<MetricPattern*> _metricPatterns;

public:
	ConfigFile();
	explicit ConfigFile(unsigned long int);
	~ConfigFile();

	void addMetricPattern(MetricPattern *);
	void addMetricPatterns(std::vector<MetricPattern *>);

	/* COMMON SETTERS */
	void setName(QString);
	void setPath(QString);

	/* COMMON GETTERS */
	QString getName() const;
	QString getPath() const;
	std::vector<MetricPattern*> getMetricPattern();

	void save();
};

#endif // CONFIGFILE_H
