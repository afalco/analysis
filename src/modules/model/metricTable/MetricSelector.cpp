#include "src/modules/model/metricTable/MetricSelector.h"

MetricSelector::MetricSelector(QVariant::Type type, DatabaseModule::Tables sourceTable, QString metricName, QObject *parent):
	MetricContainer(type, sourceTable, metricName, parent),
	_begin(type), _end(type),
	_min(type), _max(type),
	_isAbscissa(false)
{
	//_databaseTableMetrics = databaseTableMetrics;
}

MetricSelector::MetricSelector(const MetricSelector &metricSelector):
	MetricContainer(metricSelector),
	_begin(metricSelector._begin),
	_end(metricSelector._end),
	_min(metricSelector._min),
	_max(metricSelector._max),
	_isAbscissa(false)
{
	//_databaseTableMetrics = metricSelector->_databaseTableMetrics;
}

MetricSelector::~MetricSelector()
{
	//disconnect(this, 0, 0, 0); ?useful?
}

/*int MetricSelector::getUserType()
{
	return _begin.userType();
}*/

QVariant MetricSelector::getBegin() const
{
	return _begin;
}

QVariant MetricSelector::getEnd() const
{
	return _end;
}

QVariant MetricSelector::getMin() const
{
	return _min;
}

QVariant MetricSelector::getMax() const
{
	return _max;
}

bool MetricSelector::isAbscissa() const
{
	return _isAbscissa;
}

void MetricSelector::setBegin(QVariant begin)
{
	if (begin.type() == QVariant::UserType)
	{
		_begin = begin;
	}
	else
		if (begin.canConvert(_type))
		{
			_begin = begin;
			emit containerValueChanged();
		}
}

void MetricSelector::setEnd(QVariant end)
{
	if (end.canConvert(_type))
	{
		_end = end;
		emit containerValueChanged();
	}
}

void MetricSelector::setMin(QVariant min)
{
	if (min.canConvert(_type))
	{
		_min = min;
		emit containerValueChanged();
	}
}

void MetricSelector::setMax(QVariant max)
{
	if (max.canConvert(_type))
	{
		_max = max;
		emit containerValueChanged();
	}
}

void MetricSelector::isAbscissa(bool isAbscissa)
{
	_isAbscissa = isAbscissa;
}

/*void MetricSelector::attachMetricSelectorWidget(MetricSelectorWidget *msw)
{
	connect(this, &MetricSelector::containerValueChanged, msw, &MetricSelectorWidget::containerValueChanged);
	//connect(msw, &MetricSelectorWidget::valueChanged, this, &MetricSelector::onValueChange);
}

void MetricSelector::detachMetricSelectorWidget(MetricSelectorWidget *msw)
{
	disconnect(this, &MetricSelector::containerValueChanged, msw, &MetricSelectorWidget::containerValueChanged);
	disconnect(msw, &MetricSelectorWidget::valueChanged, this, &MetricSelector::onValueChange);
}*/

// -------- SLOTS --------
void MetricSelector::onValueChange()
{
	/*_begin = begin;//_begin->setValue(begin->value());
	_end = end; //_end->setValue(end->value());*/
	//emit valueChanged(new QVariant(begin), new QVariant(end));
	emit valueChanged();
}
