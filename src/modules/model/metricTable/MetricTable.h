#ifndef SELECTTABLE_H
#define SELECTTABLE_H

#include "src/modules/model/database/DatabaseTableMetrics.h"
#include "src/modules/model/metricTable/MetricSelector.h"
#include "src/modules/model/metricTable/MetricPlotStyle.h"
#include "src/modules/model/metricTable/MetricModifier.h"

#include <QObject>

#include <map>

class MetricTable : public QObject
{
	Q_OBJECT

protected:
	std::multimap<QString, MetricContainer*> *_items;

public:
	explicit MetricTable(QObject *parent = 0);
	~MetricTable();

	void insertItem(const QString, MetricSelector*);
	void insertItem(const QString, MetricPlotStyle*);
	void insertItem(const QString, MetricModifier*);

	/**
	 * @brief getItem
	 * Get the MetricSelector associated to the name or NULL pointer if name not in
	 * Return only the first found item
	 * @param name The name of the item
	 * @return first MetricSelector found or NULL
	 */
	MetricContainer *getItem(const QString);

	/**
	 * @brief removeItem
	 * Remove the MetricSelector from the list and return them or NULL pointer if name not in
	 * Return only the first found item
	 * @param name The name of the item
	 * @return first MetricSelector found or NULL
	 */
	MetricContainer *removeItem(const QString);

	/**
	 * @brief deleteAllContent
	 * Remove all items and call their destructors
	 */
	void deleteAllContent();

	std::multimap<QString, MetricContainer*>::const_iterator firstItemIterator() const;
	std::multimap<QString, MetricContainer*>::const_iterator lastItemIterator() const;

	/**
	 * @brief refresh
	 * Refresh container contents emitting all synchronization signals
	 */
	void refresh();

signals:
	void valueChanged();
	void elementChange();

public slots:
	void onMetricSelectorValueChange();
};

// To use this class in signals, slots and QVariants
//Q_DECLARE_METATYPE(SelectTable)
/*
private:
	Q_DISABLE_COPY(SelectTable)
*/

#endif // SELECTTABLE_H
