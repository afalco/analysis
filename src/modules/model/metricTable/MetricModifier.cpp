#include "MetricModifier.h"

MetricModifier::MetricModifier(QVariant::Type type, DatabaseModule::Tables sourceTable, QString tableField, QObject *parent):
	MetricContainer(type, sourceTable, tableField, parent),
	_value()
{}

MetricModifier::MetricModifier(const MetricModifier& metricModifier):
	MetricContainer(metricModifier),
	_value(metricModifier._value)
{}

QVariant MetricModifier::getValue() const
{
	return _value;
}

void MetricModifier::setValue(QVariant value)
{
	_value = value;
}

// -------- SLOTS --------
void MetricModifier::onValueChange()
{
	emit valueChanged();
}
