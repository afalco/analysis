#ifndef PLOTSTYLECHOOSER_H
#define PLOTSTYLECHOOSER_H

#include <QMetaType>
#include <QColor>
#include <QSharedPointer>

class PlotStyleChooser
{
private:
	QColor _color;
	double _width;

public:
	static const int METATYPE_ID;
	static const int METATYPE_POINTER_ID;

	PlotStyleChooser();
	PlotStyleChooser(const PlotStyleChooser&);
	~PlotStyleChooser();

	void setColor(const QColor&);
	void setWidth(const unsigned int&);

	QColor getColor() const;
	unsigned int getWidth() const;

	bool operator ==(const PlotStyleChooser&) const;
	bool operator !=(const PlotStyleChooser&) const;
	bool operator <(const PlotStyleChooser&) const;
	bool operator >(const PlotStyleChooser&) const;
};

typedef QSharedPointer<PlotStyleChooser> PlotStyleChooser_p;

Q_DECLARE_METATYPE(PlotStyleChooser)
Q_DECLARE_METATYPE(PlotStyleChooser_p)

#endif // PLOTSTYLECHOOSER_H
