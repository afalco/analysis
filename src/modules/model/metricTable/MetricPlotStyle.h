#ifndef METRICPLOTSYTLE_H
#define METRICPLOTSYTLE_H

#include "src/modules/model/metricTable/MetricContainer.h"

#include <QColor>

class MetricPlotStyle : public MetricContainer
{
	Q_OBJECT

public:
	enum LineType {
		NoLine = 1024,
		Continuous = 1,
		Dashed,
		Dotted,
		DashedDotted,
		DashedLarge,
		DashedDottedLarge
	};
	Q_ENUM(LineType)

	enum DotType {
		NoDot = 1024,
		EmptySquare = 0,
		EmptyCircle,
		EmptyTriangle,
		Plus,
		Cross,
		EmptyLosange,
		EmptyInvertedTriangle,
		SquareWithCross,
		Star,
		LosangeWithPlus,
		CircleWithPlus,
		TrianglesTopBottom,
		SquareWithPlus,
		CircleWithCross,
		SquareWithTriangle,
		Square,
		Circle,
		Triangle,
		Losange,
		BigCircle,
		SmallCircle,
		FilledCircle,
		FilledSquare,
		FilledLosange,
		FilledTriangle,
		FiledReversedTriangle
	};
	Q_ENUM(DotType)

private:
	QString _name;
	QColor _lineColor,
		_dotColor;
	LineType _lineType;
	DotType _dotType;
	double _lineWidth,
		_dotSize;

public:

	explicit MetricPlotStyle(QVariant::Type, DatabaseModule::Tables, QString tableField, QObject *parent = 0);
	explicit MetricPlotStyle(const MetricPlotStyle&);

	static const char *lineTypeToName(LineType);
	static const char *dotTypeToName(DotType);

	/* COMMON GETTERS */
	QString getName() const;
	QColor getLineColor() const;
	QColor getDotColor() const;
	LineType getLineType() const;
	DotType getDotType() const;
	double getLineWidth() const;
	double getDotSize() const;

	/* COMMON SETTERS */
	void setName(QString);
	void setLineColor(QColor);
	void setDotColor(QColor);
	void setLineType(LineType);
	void setDotType(DotType);
	void setLineWidth(double);
	void setDotSize(double);

signals:
	void valueChanged();

	void containerValueChanged();

public slots:
	void onValueChange();
};

//Q_DECLARE_METATYPE(MetricPlotStyle::LineType)
//Q_DECLARE_METATYPE(MetricPlotStyle::DotType)

#endif // METRICPLOTSYTLE_H
