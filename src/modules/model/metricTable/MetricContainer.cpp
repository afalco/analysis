#include "src/modules/model/metricTable/MetricContainer.h"
#include "src/modules/view/viewGUI/metricTable/MetricWidget.h"

MetricContainer::MetricContainer(QVariant::Type type, DatabaseModule::Tables sourceTable, QString tableField, QObject *parent):
	QObject(parent),
	_type(type), _sourceTable(sourceTable), _metricName(tableField)
{}

MetricContainer::MetricContainer(const MetricContainer& metricContainer):
	QObject(metricContainer.parent()),
	_type(metricContainer._type),
	_sourceTable(metricContainer._sourceTable),
	_metricName(metricContainer._metricName)
{}

QVariant::Type MetricContainer::getType() const
{
	return _type;
}

int MetricContainer::getUserType() const
{ return 0; }

DatabaseModule::Tables MetricContainer::getDatabaseTableMetrics()
{
	return _sourceTable;
}

QString MetricContainer::getMetricName() const
{
	return _metricName;
}

void MetricContainer::setDatabaseTableMetrics(DatabaseModule::Tables sourceTable)
{
	_sourceTable = sourceTable;
}

void MetricContainer::setMetricName(QString tableField)
{
	_metricName = tableField;
}
