#include "src/modules/model/metricTable/MetricTable.h"

MetricTable::MetricTable(QObject *parent):
	QObject(parent)
{
	_items = new std::multimap<QString, MetricContainer*>;
}

MetricTable::~MetricTable()
{
	delete _items;
}

void MetricTable::insertItem(const QString name, MetricSelector *table)
{
	connect(table, &MetricSelector::valueChanged, this, &MetricTable::onMetricSelectorValueChange);
	_items->insert(std::pair<QString, MetricSelector*>(name, table));
	emit elementChange();
}

void MetricTable::insertItem(const QString name, MetricPlotStyle *table)
{
	connect(table, &MetricPlotStyle::valueChanged, this, &MetricTable::onMetricSelectorValueChange);
	_items->insert(std::pair<QString, MetricPlotStyle*>(name, table));
	emit elementChange();
}

void MetricTable::insertItem(const QString name, MetricModifier *table)
{
	connect(table, &MetricModifier::valueChanged, this, &MetricTable::onMetricSelectorValueChange);
	_items->insert(std::pair<QString, MetricModifier*>(name, table));
	emit elementChange();
}

MetricContainer *MetricTable::getItem(const QString name)
{
	std::multimap<QString, MetricContainer*>::iterator it =
		_items->find(name);
	if (it != _items->end()) // item found
		return static_cast<MetricContainer*>(it->second);
	else // item not found
		return NULL;
}

MetricContainer *MetricTable::removeItem(const QString name)
{
	MetricContainer *metricSelector = NULL;
	std::multimap<QString, MetricContainer*>::iterator it =
		_items->find(name);
	if (it != _items->end()) { // item found
		_items->erase(it);
		metricSelector = static_cast<MetricContainer*>(it->second);
		disconnect(metricSelector, &MetricContainer::valueChanged, this, &MetricTable::onMetricSelectorValueChange);
	}
	emit elementChange();
	return metricSelector;
}

void MetricTable::deleteAllContent()
{
	for (std::multimap<QString, MetricContainer*>::iterator it = _items->begin(), end = _items->end();
		it != end; ++it) {
		delete it->second;
	}
	_items->clear();
}

std::multimap<QString, MetricContainer*>::const_iterator MetricTable::firstItemIterator() const
{
	return _items->cbegin();
}

std::multimap<QString, MetricContainer*>::const_iterator MetricTable::lastItemIterator() const
{
	return _items->cend();
}

void MetricTable::refresh()
{
	emit valueChanged();
	emit elementChange();
}

// -------- SLOTS --------
void MetricTable::onMetricSelectorValueChange()
{
	emit valueChanged();
}
