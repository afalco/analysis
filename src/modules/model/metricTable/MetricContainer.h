#ifndef METRICCONTAINER_H
#define METRICCONTAINER_H

#include "src/modules/model/database/DatabaseModule.h"
class MetricWidget;//#include "view/viewGUI/MetricWidget.h"

#include <QObject>
#include <QVariant>

class MetricContainer : public QObject
{
	Q_OBJECT

protected:
	QVariant::Type _type;	// type of data used
	DatabaseModule::Tables _sourceTable;
	QString _metricName;

public:
	explicit MetricContainer(QVariant::Type, DatabaseModule::Tables, QString, QObject *parent = 0);
	explicit MetricContainer(const MetricContainer&);

	QVariant::Type getType() const;
	int getUserType() const;
	DatabaseModule::Tables getDatabaseTableMetrics();
	QString getMetricName() const;

	void setDatabaseTableMetrics(DatabaseModule::Tables);
	void setMetricName(QString);

signals:
	/**
	 * @brief valueChanged
	 * Emitted when a value has been changed by associated view component
	 */
	void valueChanged();

	/**
	 * @brief containerValueChanged
	 * Emitted when user change container value and associated view component must be consequently updated
	 */
	void containerValueChanged();

public slots:
	/**
	 * @brief onValueChange
	 * Once connected to view component, is used by them to notify a value update
	 */
	virtual void onValueChange() = 0;
};

#endif // METRICCONTAINER_H
