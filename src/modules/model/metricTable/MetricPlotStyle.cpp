#include "MetricPlotStyle.h"

const char *MetricPlotStyle::lineTypeToName(LineType lineType)
{
	switch (lineType)
	{
	case NoLine:
		return "No Line";
		break;
	case Continuous:
		return "Continuous";
		break;
	case Dashed:
		return "Dashed";
		break;
	case Dotted:
		return "Dotted";
		break;
	case DashedDotted:
		return "DashedDotted";
		break;
	case DashedLarge:
		return "DashedLarge";
		break;
	case DashedDottedLarge:
		return "DashedDottedLarge";
		break;
	default:
		return "";
	}
}

const char *MetricPlotStyle::dotTypeToName(DotType dotType)
{
	switch (dotType)
	{
	case NoDot:
		return "No Dot";
		break;
	case EmptyCircle:
		return "EmptyCircle";
		break;
	case EmptyTriangle:
		return "EmptyTriangle";
		break;
	case Plus:
		return "Plus";
		break;
	case Cross:
		return "Cross";
		break;
	case EmptyLosange:
		return "EmptyLosange";
		break;
	case EmptyInvertedTriangle:
		return "EmptyInvertedTriangle";
		break;
	case SquareWithCross:
		return "SquareWithCross";
		break;
	case Star:
		return "Star";
		break;
	case LosangeWithPlus:
		return "LosangeWithPlus";
		break;
	case CircleWithPlus:
		return "CircleWithPlus";
		break;
	case TrianglesTopBottom:
		return "TrianglesTopBottom";
		break;
	case SquareWithPlus:
		return "SquareWithPlus";
		break;
	case CircleWithCross:
		return "CircleWithCross";
		break;
	case SquareWithTriangle:
		return "SquareWithTriangle";
		break;
	case Square:
		return "Square";
		break;
	case Circle:
		return "Circle";
		break;
	case Triangle:
		return "Triangle";
		break;
	case Losange:
		return "Losange";
		break;
	case BigCircle:
		return "BigCircle";
		break;
	case SmallCircle:
		return "SmallCircle";
		break;
	case FilledCircle:
		return "FilledCircle";
		break;
	case FilledSquare:
		return "FilledSquare";
		break;
	case FilledLosange:
		return "FilledLosange";
		break;
	case FilledTriangle:
		return "FilledTriangle";
		break;
	case FiledReversedTriangle:
		return "FiledReversedTriangle";
		break;
	default:
		return "";
	}
}

MetricPlotStyle::MetricPlotStyle(QVariant::Type type, DatabaseModule::Tables sourceTable, QString tableField, QObject *parent):
	MetricContainer(type, sourceTable, tableField, parent),
	_name(), _lineColor(), _dotColor(),
	_lineType(Continuous), _dotType(EmptySquare),
	_lineWidth(1.0), _dotSize(1.0)
{}

MetricPlotStyle::MetricPlotStyle(const MetricPlotStyle &metricPlotStyle):
	MetricContainer(metricPlotStyle),
	_name(metricPlotStyle._name),
	_lineColor(metricPlotStyle._lineColor),
	_dotColor(metricPlotStyle._dotColor),
	_lineType(metricPlotStyle._lineType),
	_dotType(metricPlotStyle._dotType),
	_lineWidth(metricPlotStyle._lineWidth),
	_dotSize(metricPlotStyle._dotSize)
{}

/* COMMON GETTERS */
QString MetricPlotStyle::getName() const
{
	return _name;
}

QColor MetricPlotStyle::getLineColor() const
{
	return _lineColor;
}

QColor MetricPlotStyle::getDotColor() const
{
	return _dotColor;
}

MetricPlotStyle::LineType MetricPlotStyle::getLineType() const
{
	return _lineType;
}

MetricPlotStyle::DotType MetricPlotStyle::getDotType() const
{
	return _dotType;
}

double MetricPlotStyle::getLineWidth() const
{
	return _lineWidth;
}

double MetricPlotStyle::getDotSize() const
{
	return _dotSize;
}


/* COMMON SETTERS */
void MetricPlotStyle::setName(QString name)
{
	_name = name;
}

void MetricPlotStyle::setLineColor(QColor color)
{
	_lineColor = color;
}

void MetricPlotStyle::setDotColor(QColor color)
{
	_dotColor = color;
}

void MetricPlotStyle::setLineType(LineType type)
{
	_lineType = type;
}

void MetricPlotStyle::setDotType(DotType type)
{
	_dotType = type;
}

void MetricPlotStyle::setLineWidth(double width)
{
	_lineWidth = width;
}

void MetricPlotStyle::setDotSize(double size)
{
	_dotSize = size;
}


// -------- SLOTS --------
void MetricPlotStyle::onValueChange()
{}
