#ifndef METRICMODIFIER_H
#define METRICMODIFIER_H

#include "src/modules/model/metricTable/MetricContainer.h"

#include <QVariant>

class MetricModifier : public MetricContainer
{
	Q_OBJECT

private:
	QVariant _value;

public:
	MetricModifier();
	explicit MetricModifier(QVariant::Type, DatabaseModule::Tables, QString, QObject *parent = 0);
	explicit MetricModifier(const MetricModifier&);

	QVariant getValue() const;

	void setValue(QVariant);

signals:
	void valueChanged();

	void containerValueChanged();

public slots:
	void onValueChange();
};

#endif // METRICMODIFIER_H
