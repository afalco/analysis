#ifndef METRICSELECTOR_H
#define METRICSELECTOR_H

#include "src/modules/model/metricTable/MetricContainer.h"
//#include "model/database/DatabaseTableMetrics.h"
class MetricSelectorWidget; // in cause of loop inclusion with #include "view/viewGUI/MetricSelectorWidget.h"

#include <QObject>
#include <QVariant>

class MetricSelector : public MetricContainer
{
	Q_OBJECT

protected:
	QVariant _begin,		// select range begin
		_end,				// select range end
		_min,				// the min selectable value
		_max;				// the max selectable value
	bool _isAbscissa;

public:
	explicit MetricSelector(QVariant::Type, DatabaseModule::Tables, QString tableField, QObject *parent = 0);
	explicit MetricSelector(const MetricSelector&);
	~MetricSelector();

	/**
	 * @brief attachMetricSelectorWidget
	 * Connect events with MetricSelectorWidget
	 * @param concerned widget
	 */
	void attachMetricSelectorWidget(MetricSelectorWidget *);

	/**
	 * @brief detachMetricSelectorWidget
	 * Disconnect events with MetricSelectorWidget
	 * @param concerned widget
	 */
	void detachMetricSelectorWidget(MetricSelectorWidget *);

	QVariant getBegin() const;
	QVariant getEnd() const;
	QVariant getMin() const;
	QVariant getMax() const;
	bool isAbscissa() const;

	void setBegin(QVariant);
	void setEnd(QVariant);
	void setMin(QVariant);
	void setMax(QVariant);
	void isAbscissa(bool);

signals:
	/**
	 * @brief valueChanged
	 * Emitted when a value has been changed by associated view component
	 */
	void valueChanged();

	/**
	 * @brief containerValueChanged
	 * Emitted when user change container value and associated view component must be consequently updated
	 */
	void containerValueChanged();

public slots:
	/**
	 * @brief onValueChange
	 * Once connected to view component, is used by them to notify a value update
	 * @param begin New begin value
	 * @param end New end value
	 */
	void onValueChange();
};

#endif // METRICSELECTOR_H
