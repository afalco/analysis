#include "src/modules/model/metricTable/PlotStyleChooser.h"

const int PlotStyleChooser::METATYPE_ID = qRegisterMetaType<PlotStyleChooser>();
const int PlotStyleChooser::METATYPE_POINTER_ID = qRegisterMetaType<PlotStyleChooser_p>();
//QMetaType::registerComparators<PlotStyleChooser>();

PlotStyleChooser::PlotStyleChooser():
	_color(0, 0, 0), _width(1.)
{}

PlotStyleChooser::PlotStyleChooser(const PlotStyleChooser &plotStyleChooser):
	_color(plotStyleChooser._color),
	_width(plotStyleChooser._width)
{}

PlotStyleChooser::~PlotStyleChooser()
{}

void PlotStyleChooser::setColor(const QColor& color)
{
	_color = color;
}

void PlotStyleChooser::setWidth(const unsigned int& width)
{
	if (_width > 0)
		_width = width;
}

QColor PlotStyleChooser::getColor() const
{
	return _color;
}

unsigned int PlotStyleChooser::getWidth() const
{
	return _width;
}


bool PlotStyleChooser::operator ==(const PlotStyleChooser& plotStyleChooser) const
{
	return _width == plotStyleChooser._width && _color == plotStyleChooser._color;
}

bool PlotStyleChooser::operator !=(const PlotStyleChooser& plotStyleChooser) const
{
	return _width != plotStyleChooser._width || _color != plotStyleChooser._color;
}
bool PlotStyleChooser::operator <(const PlotStyleChooser& plotStyleChooser) const
{
	return _width < plotStyleChooser._width;// && _color < plotStyleChooser._color;
}
bool PlotStyleChooser::operator >(const PlotStyleChooser& plotStyleChooser) const
{
	return _width > plotStyleChooser._width;// && _color > plotStyleChooser._color;
}
