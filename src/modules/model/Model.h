#ifndef MODEL_H
#define MODEL_H

#include "src/modules/model/parsing/ParsingMetric.h"
#include "src/modules/model/parsing/ParsingExperimentCampaign.h"
#include "src/modules/model/GraphModule.h"

#include "src/modules/model/database/DatabaseTable.h"
#include "src/modules/model/database/DatabaseTableMetrics.h"
#include "src/modules/model/database/MetricPattern.h"
#include "src/modules/model/database/ExperimentCampaign.h"
#include "src/modules/model/database/Experiment.h"
#include "src/modules/model/database/Matrix.h"
#include "src/modules/model/database/Hardware.h"
#include "src/modules/model/database/Software.h"
#include "src/modules/model/database/Solver.h"
#include "src/modules/model/database/ConfigFile.h"

#include "src/modules/model/metricTable/MetricTable.h"
#include "src/modules/model/metricTable/MetricPlotStyle.h"
#include "src/modules/model/metricTable/MetricModifier.h"

#include <QObject>
#include <QListWidgetItem>
#include <QDate>
#include <QTime>
#include <QDateTime>
#include <QFile>

#include <vector>
#include <map>
#include <cstdio>

class Model : public QObject
{
	Q_OBJECT

private:
    //std::vector<OutputFile *> _outputFiles;


    //ExperimentCampaign * _currentCampaign;
    ParsingExperimentCampaign * _currentCampaign;

	// generation module
	std::map<std::string, MetricSelector*> _generationMetrics;
	std::vector<QVariant> _generationAbscissa;
	std::map<MetricPlotStyle*, std::vector<QVariant> > _generationOrdinate;
	MetricTable *_generationCurrentDatabaseContent,
		*_generationCurrentOrdinateSelected;
	GraphModule::GraphType _generationGraphType;

	/*
	 * because this class must be threaded, these variables are used to avoid
	 * bogues recieving multiples calls to slots durig time too short.
	 */
	volatile bool _previewImageResizing;
	QSize _currentPreviewImageSize,
		_lastPreviewImageSize,
		_generationImageSize;

	// management
	ExperimentCampaign *_managedExperimentCampaign;
	MetricTable *_managementMetricsTable;

	void bindEvents();
	void insertMetric(Experiment * experiment, std::string table, std::string id, std::string value);

	// configuration module
	ParsingConfig *_newParsingConfig;

	// parsing module
	ParsingConfig *_currentParsingConfig;

public:
	explicit Model();
	~Model();
    ParsingExperimentCampaign * getCurrentCampaign();
    void setOutputFiles(std::map<std::string, std::vector<ParsingMetric *>> outputFiles);
    void addOutputFile(std::string fileName, std::vector<ParsingMetric *> fileRow);

public slots:
	/**
	 * @brief parseFiles parse files in parameter
	 * @param files is parsed files
	 */
	void parseFiles(QStringList *);
    /**
     * @brief parseFolder parse folders in parameter
     * @param folder is parsed folders
     */
    void parseFolder(QStringList *);
    /**
     * @brief parseConfig parse files in parameter
     * @param files is parsed files
     */
    void parseConfig(QStringList *);
	/**
	 * @brief openDatabase open an existing database
	 * @param name the name of database to open
	 */
	void openDatabase(QString*);
	/**
	 * @brief createDatabase create a new database and create required tables
	 * @param name the name of database to open
	 */
	void createDatabase(QString*);

	/**
	 * @brief resizePreviewImage
	 * generate new image to fit the size of the widget
	 * @param size the size of image to generate
	 */
	void resizePreviewImage(QSize*);

    /**
     * @brief
     * @param
     */
	void getExperimentInformations(QList<QListWidgetItem*>*);

    /**
     * @brief
     * @param
     */
	void clearExperimentCampaign();

	/**
	 * @brief onFindSelectorValuesSet
	 * Send to view the pointer on _currentDatabaseContent in order to
	 * connect view and model values. Only used once, on application init
	 */
	void onFindSelectorValuesSet();

	/**
	 * @brief onOrdinateSelectionValuesSet
	 * Send to view the pointer on _currentOrdinateSelected in order to
	 * connect view and model values. Only used once, on application init
	 */
	void onOrdinateSelectionValuesSet();

	/**
	 * @brief updateFindDatabaseWidgetContent
	 * Update the database widget contents
	 */
	void updateFindDatabaseWidgetContent();

	void insertExperimentCampaign(QString* experimentCampaignName, QString* experimentCampaignDescription);

	void changeMetricValue(QString* id, QString* value, QString* fileName);

	void checkConfig(QVariant * isFolder);

	// related to management
	void onManagementMetricTableSetted();
	void onManagementTabViewed();
	void onManagementExperimentCampaignSelected(unsigned long int);
	void onManagementExperimentsSelected(std::vector<unsigned long int>);
	void onManagementUpdateCurrentValues();

	void onGenerationGraphTypeChange(GraphModule::GraphType);
	void onGenerationGraphGenerateFile(QString);
	void onGenerationSelectAbscissaMetric(unsigned long int);
	void onInsertionTabViewed();

	// related to config file
	void parseConfigFile(QString);
	void saveConfigFile(QString);
	void onInsertionWidgetParserOptionSelected(unsigned long int);

private slots:
	void onCurrentDatabaseContentChanged();

signals:
	/**
	 * @brief setPreviewImage
	 * set the image showed in the preview widget
	 * @param image the image to show
	 */
	void setPreviewImage(QPixmap*);
    /**
     * @brief setExperiments
     * send the experiments to the view
     * @param experiments the experiments to show
     */
    void setExperiments(ParsingExperimentCampaign*);
    /**
     * @brief selectedParsedExperiment
     *
     * @param
     */
	void selectedParsedExperiment(QList<ParsingExperiment*>*, ParsingConfig*);

	/**
	 * @brief setFindSelectorValues
	 * Once connected to view, update findSelectorValues pointer to
	 * current selectTable object.
	 */
	void setFindSelectorValues(MetricTable *);

	/**
	 * @brief setOrdinateSelectionValues
	 * Onc connected to view, update ordinateSelectvalues pointer to
	 * current selectTable object.
	 */
	void setOrdinateSelectionValues(MetricTable *);

	void configCheckResult(QVariant*, QVariant*);
	void setInsertionWidgetParserOptions(std::map<unsigned long int, QString>);

	// related to management
	void managementExperimentCampaignsLoaded(std::map<unsigned long int, QString>);
	void managementExperimentCampaignLoaded(std::map<unsigned long int, QString>, QString, QString);
	void managementMetricsLoaded(MetricTable *);
	void managementModifyExperimentName(bool);
	void setGenerationAbscissaSelectorValues(QStringList);

	// related to config file
	void setConfigWidgetConfigFilePatterns(QStringList);
};

#endif // MODEL_H
