#include "src/toolbox.h"

void replace(std::string &subject, const std::string &oldText, const std::string &newText, bool all)
{
	std::size_t pos, lastPos = pos = subject.find(oldText);
	bool once = false;
	while (pos != std::string::npos && !once)
	{
		subject.replace(pos, oldText.size(), newText);
		lastPos = pos;
		pos = subject.find(oldText, lastPos);
		if (!all)
			once = true;
	}
}

std::map<QString, QVariant::Type, cmpQStringInsensitive> nameTypeSurjection(multimapStartValues, multimapStartValues + multimapStartValuesSize);

QString qVariantTypeToTypename(QVariant::Type type)
{
	QString name;
	for (std::map<QString, QVariant::Type>::const_iterator
		it = nameTypeSurjection.cbegin(),
		end = nameTypeSurjection.cend();
		it != end; ++it)
	{
		if (it->second == type)
		{
			name = it->first;
			it = end;
		}
	}
	return name;
}

QVariant::Type typenameToQVariantType(const QString &name)
{
	QVariant::Type type = QVariant::Invalid;
	std::map<QString, QVariant::Type>::const_iterator it =
		nameTypeSurjection.find(name);
	if (it != nameTypeSurjection.cend())
		type = static_cast<std::pair<QString, QVariant::Type> >(*it).second;
	return type;
}
